# gdt
[![pipeline status](https://gitlab.com/kavalchuk.d.v/gdt/badges/master/pipeline.svg)](https://gitlab.com/kavalchuk.d.v/gdt/-/commits/master)

Game dev studiyng repo.
Main goal of this project is to develop 2D game about space and gravity. 
Final version is contained in game_space folder. 
In order to download source of game you can download or clone only this folder.
You need CMake 3.16 to build game.
Linux, Android and Windows are supported.