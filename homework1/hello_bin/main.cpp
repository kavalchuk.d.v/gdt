#include <cstdlib>
#include <hello_lib.h>
#include <iostream>

int main()
{
    using namespace std;
    cout << "Hello World" << endl;
    hello_lib();
    return ((bool)cout) ? EXIT_SUCCESS : EXIT_FAILURE;
}
