cmake_minimum_required(VERSION 3.16)

project(hello_lib LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(hello-lib-dynamic SHARED hello_lib.h hello_lib.cpp)
target_include_directories (hello-lib-dynamic PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
