in vec2 a_position1;
in vec4 a_color;
in vec2 a_position2;

out vec4 v_position;
out vec4 v_color;

// koefficient to morph vertex
uniform float u_koef;

void main()
{
    v_position = vec4(a_position1 * u_koef + a_position2 * (1.0f - u_koef), 0.0, 1.0);
    v_color = a_color;
    gl_Position = v_position;
}
