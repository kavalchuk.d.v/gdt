#include "audio_wrapper.hpp"
#include "global.hpp"
#include <algorithm>

AudioWrapper::AudioWrapper(om::IEngine& engine)
    : m_engine{ engine }
{
    addAllTracks();
}

void AudioWrapper::addAllTracks()
{
    m_soundTrackUserRocket = m_engine.addSoundTrack(pathToSoundTrackUserRocket);
    m_soundBufferUserRocket = m_engine.addSoundBuffer(m_soundTrackUserRocket);
}

static auto isPlaying = false;

void AudioWrapper::play(const World& world)
{

    const auto& userShip = world.objects[world.userShipIndex];

    const auto isMainEngineEnabled  = userShip.getMainEngineState().getState();
    const auto isSideEnginesEnabled = userShip.getSideEngineState().getState();

    const auto mainEngineSound =
        (isMainEngineEnabled) ? userShip.mainEnginesPercentThrust / 100.f : 0.f;

    if (!isMainEngineEnabled && !isSideEnginesEnabled)
    {
        isPlaying = false;
        m_soundBufferUserRocket->stop();
    }

    const auto sideEnginesSound =
        (isSideEnginesEnabled) ? userShip.sideEnginesPercentThrust / 100.f
                               : 0.f;

    const auto engineSoundPower =
        static_cast<float>(mainEngineSound * 0.85f + sideEnginesSound * 0.15f);

    const auto distance2 =
        static_cast<float>(userShip.x * userShip.x + userShip.y * userShip.y);

    const auto distance = std::sqrt(distance2);

    const auto maxHearableDistance = 2000.f;

    auto engineDistanceKoef = std::pow(
        1.f - std::min(distance, maxHearableDistance) / maxHearableDistance, 2);

    const auto enginesSoundVolume = engineSoundPower * engineDistanceKoef;

    auto sinA{ 1.f };

    if (distance > std::numeric_limits<float>::epsilon() * 2)
    {
        sinA = std::abs(userShip.x / distance);
    }

    const auto sign = (userShip.x > 0.f) ? -1.0 : 1.0;

    const auto maxLeftRightRegulation{ 0.15f };

    auto leftRightValue = sign * sinA * maxLeftRightRegulation;

    auto stereoValue = sign * sinA;

    if (std::abs(userShip.y) < 300.f && std::abs(userShip.x) < 300.f)
    {
        leftRightValue *= std::abs(userShip.x) * std::abs(userShip.y) * 1.f /
                          300.f * 1.f / 300.f;

        stereoValue *= leftRightValue;
    }

    m_soundBufferUserRocket->setVolume(enginesSoundVolume);

    m_soundBufferUserRocket->setLeftRightBalance(leftRightValue);

    m_soundBufferUserRocket->setStereo(stereoValue);

    if (!isPlaying)
    {
        isPlaying = true;
        m_soundBufferUserRocket->play(om::ISoundBuffer::properties::looped);
    }
}
