#pragma once
#include "world.hpp"
#include <iengine.hpp>

class AudioWrapper
{
public:
    explicit AudioWrapper(om::IEngine& engine);
    void play(const World& world);

private:
    void addAllTracks();

    om::ISoundTrack* m_soundTrackUserRocket;

    om::ISoundBuffer* m_soundBufferUserRocket;

    static constexpr std::string_view pathToSoundTrackUserRocket{
        "res/sounds/user_rocket_sound.wav"
    };

    static constexpr std::string_view pathToSoundTrackEnemyRocket{
        "res/sounds/enemy_rocket_sound.wav"
    };

    om::IEngine& m_engine;
};
