﻿#include "render_wrapper.hpp"
#include "global.hpp"
#include "utilities.hpp"

#include <algorithm>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexAttributePositions{
        { om::Vertex::positionAttributeNumber, "a_position" },
        { om::Vertex::colorAttributeNumber, "a_color" }
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexMorphedAttributePositions{
        { om::VertexMorphed::positionAttributeNumber, "a_position1" },
        { om::VertexMorphed::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexTexturedAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexWideAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position1" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::string_view> RenderWrapper::textureAttributeNames{
    "s_texture"
};

using namespace renderObjects;

RenderWrapper::RenderWrapper(om::IEngine&                 engine,
                             std::array<om::myGlfloat, 3> color,
                             om::myGlfloat                gridStep)
    : m_engine{ engine }
{
    m_programIdShaderGrid = m_engine.addProgram(
        "res/shaders/vertex_shader_1.vert",
        "res/shaders/fragment_shader_1.frag", vertexAttributePositions);

    m_programIdShaderMorph = m_engine.addProgram(
        "res/shaders/vertex_shader_2.vert",
        "res/shaders/fragment_shader_2.frag", vertexMorphedAttributePositions);

    m_programIdTexturedMorphed = m_engine.addProgram(
        "res/shaders/vertex_shader_3.vert",
        "res/shaders/fragment_shader_3.frag", vertexWideAttributePositions);

    m_programIdTexturedMoved = m_engine.addProgram(
        "res/shaders/vertex_shader_5.vert",
        "res/shaders/fragment_shader_5.frag", vertexTexturedAttributePositions);

    m_programIdMorphedMoved = m_engine.addProgram(
        "res/shaders/vertex_shader_6.vert",
        "res/shaders/fragment_shader_6.frag", vertexMorphedAttributePositions);

    m_programIdMoved = m_engine.addProgram(
        "res/shaders/vertex_shader_moved.vert",
        "res/shaders/fragment_shader_moved.frag", vertexAttributePositions);

    m_textureIdRocketMainCorpus =
        m_engine.addTexture("res/textures/topdownfighter.png");
    m_textureIdFireMainEngine =
        m_engine.addTexture("res/textures/flame_fire.png");
    m_textureIdFireSideEngine =
        m_engine.addTexture("res/textures/flame_blueish_flame.png");

    rocket = renderObjects::Rocket(
        textureAttributeNames[0], moveMatrixUniformName,
        m_programIdTexturedMoved, m_textureIdRocketMainCorpus,
        m_textureIdFireMainEngine, m_textureIdFireSideEngine);

    m_engine.setCurrentDefaultProgram(m_programIdTexturedMoved);

    m_color    = color;
    m_gridStep = gridStep;
}

bool RenderWrapper::checkColor(std::array<om::myGlfloat, 3> color)
{
    return std::none_of(
        color.begin(), color.end(), [](om::myGlfloat colorComponent) {
            return (colorComponent > 1.f || colorComponent < 0.f);
        });
}

void RenderWrapper::renderRectangleTexturedTank(om::IEngine& engine)
{
    static std::vector<om::VertexTextured> vertexes{
        { { -1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 0.0, 0.0 } },
        { { -1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 0.0, 1.0 } },
        { { 1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 1.0, 0.0 } },
        { { 1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 1.0, 1.0 } }
    };

    static std::vector<om::Vertex> verticesTest{
        { { -1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { -1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { 1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { 1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f } }
    };

    std::vector<om::myUint> indexes{ 0, 1, 3, 0, 2, 3 };

    static Timer timer;
    float        elapsedSeconds = timer.elapsed().count();

    const om::myGlfloat rotatingPeriod{ 2 };
    auto                angle = elapsedSeconds / rotatingPeriod * M_PI - M_PI;

    const om::myGlfloat xShift = std::sin(angle);

    const om::myGlfloat yShift = -std::sin(angle) / 2;

    // mainEngineFire.setAngle(angle);

    // mainEngineFire.setPos({ xShift, yShift });

    //    mainEngineFire.draw(engine, MainEngineFire::Event::Run,
    //                        fireTimer.elapsed());

    //    tankSprite.setSpritePos({ xShift, yShift });

    //    tankSprite.setAngle(angle);

    //    // engine.render(verticesTest, { 0, 1, 3, 0, 2, 3 },
    //    m_programIdShaderGrid);

    //    tankSprite.draw(engine);
}

void RenderWrapper::render(const World& world)
{
    renderRectangleTexturedTank(m_engine);

    renderWorld(world);
}

void RenderWrapper::renderWorld(const World& world)
{
    for (const auto& currentObject : world.objects)
    {
        rocket.draw(m_engine, currentObject);
    }
}
