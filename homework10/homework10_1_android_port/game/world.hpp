#pragma once
#include "utilities.hpp"
#include "world_objects.hpp"
#include "world_physics.hpp"
#include <iosfwd>
#include <unordered_map>
#include <vector>
#include <array>
#define _USE_MATH_DEFINES
#include <math.h>


class World
{
public:
    enum class Events : size_t
    {
        userCommandShipUp,
        userCommandShipTeleporation,
        userCommandShipRotateLeft,
        userCommandShipRotateRight,
        maxType,
    };

    using WorldEvents =
        std::unordered_map<World::Events, std::array<double, 2>>;

    using clock_t        = Timer::clock_t;
    using seconds_t      = Timer::seconds_t;
    using milliseconds_t = Timer::milliseconds_t;
    using time_point_t   = Timer::time_point_t;

    World(time_point_t initialTime);

    int update(time_point_t nowTime, const WorldEvents* events);

    /// TODO change to other container
    std::vector<PhysicalObject> objects;

    size_t userShipIndex;

    time_point_t lastUpdateTime;
    seconds_t    dt{ milliseconds_t{ 4 } };

    friend std::ostream& operator<<(std::ostream& out, const World& world);
};
