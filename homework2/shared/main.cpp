#include <SDL.h>
#include <SDL_version.h>
#include <iostream>

std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

int main(int /*argc*/, char* /*argv*/[])
{
    using namespace std;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    cout << "compiled: " << compiled << '\n';
    cout << "linked: " << linked << endl;
    auto is_sdl_init = (SDL_Init(SDL_INIT_EVERYTHING) >= 0);
    if (is_sdl_init)
    {
        std::cout << "SDL initialization succeeded!\n";
    }
    else
    {
        std::cout << "SDL initialization failed. SDL Error: " << SDL_GetError();
    }

    bool is_good_cout = cout.good();

    int result = (is_good_cout && is_sdl_init) ? EXIT_SUCCESS : EXIT_FAILURE;
    return result;
}
