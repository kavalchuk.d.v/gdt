#include <algorithm>
#include <array>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <string_view>
#include <thread>

#include <SDL.h>

#pragma pack(push, 4) // ? why is it used?
struct bind
{
    SDL_Keycode      key;
    std::string_view name;
};
#pragma pack(pop)

void check_input(const SDL_Event& event)
{
    static const std::array<bind, 4> keys{ {
        { SDLK_w, "up" },
        { SDLK_a, "left" },
        { SDLK_s, "down" },
        { SDLK_d, "right" },
    } };

    const auto key =
        std::find_if(keys.begin(), keys.end(), [event](const bind& b) {
            return b.key == event.key.keysym.sym;
        });

    if (key == keys.end())
    {
        return;
    }

    std::cout << key->name;
    if (event.type == SDL_KEYDOWN)
    {
        std::cerr << " has been pressed\n";
    }
    else if (event.type == SDL_KEYUP)
    {
        std::cerr << " has been released\n";
    }
}

int main()
{
    const auto init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        std::cerr << "Something bad has been occured. SDL_Init has returned: "
                  << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Window* const window =
        SDL_CreateWindow("myWindow!", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        std::cerr
            << "Something bad has been occured. SDL_CreateWindow has returned: "
            << SDL_GetError() << std::endl;
        SDL_Quit();
        return EXIT_FAILURE;
    }

    bool continue_loop = true;
    while (continue_loop)
    {
        SDL_Event sdl_event;

        while (SDL_PollEvent(&sdl_event))
        {
            switch (sdl_event.type)
            {
                case SDL_KEYDOWN:
                    check_input(sdl_event);
                    break;
                case SDL_KEYUP:
                    check_input(sdl_event);
                    break;
                case SDL_QUIT:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds{ 5 });
        }
    }

    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
