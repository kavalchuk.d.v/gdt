#pragma once
#include "iengine.hpp"
#include <iosfwd>
#include <string>
#include <string_view>

namespace om
{
class EngineSdl final : public IEngine
{
public:
    virtual std::string initialize(std::string_view /*config*/) override final;
    bool                read_input(event* const e) const override final;
    void                uninitialize() override final;
    ~EngineSdl() override final {}
};

} // end namespace om
