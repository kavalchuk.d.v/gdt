#include <chrono>
#include <cstdlib>
#include <engine_handler.hpp>
#include <iostream>
#include <memory>
#include <random>
#include <thread>
#include <unordered_set>

#include "timer.hpp"
#include "world.hpp"

bool isContiniousCommand(World::Events worldEvent)
{
    return (worldEvent == World::Events::userCommandShipUp);
}

bool input(const om::IEngine*                        engine,
           const std::unordered_set<World::Events>*& retWorldEvents)
{
    static std::unique_ptr<std::unordered_set<World::Events>> worldEvents{
        new std::unordered_set<World::Events>{}
    };
    retWorldEvents = worldEvents.get();

    for (auto it = worldEvents->begin(); it != worldEvents->end();)
    {
        if (!isContiniousCommand(*it))
            it = worldEvents->erase(it);
        else
            ++it;
    }
    om::event event;
    while (engine->read_input(&event))
    {
        std::cout << event << std::endl;
        switch (event)
        {
            case om::event::turn_off:
                return false;
                break;
            case om::event::up_pressed:
                worldEvents->insert(World::Events::userCommandShipUp);
                break;
            case om::event::up_released:
                worldEvents->erase(World::Events::userCommandShipUp);
                break;
            default:
                break;
        }
    }
    return true;
}

bool render(const World& world)
{
    using seconds_t        = std::chrono::duration<double, std::ratio<1>>;
    auto      randomTimeMs = getRandom(0, 6) / 1000;
    seconds_t durationSleep{ randomTimeMs };
    std::cout << world;
    std::this_thread::sleep_for(durationSleep);
    return true;
}

void normalizeLoopDuration(
    std::chrono::duration<double, std::ratio<1>> loopElapsedTime)
{
    using seconds_t      = std::chrono::duration<double, std::ratio<1>>;
    using milliseconds_t = std::chrono::duration<double, std::milli>;

    static constexpr int       refreshRate = 60;
    static constexpr seconds_t loopNormalizedDuration{
        (seconds_t{ 1 } / refreshRate) - milliseconds_t{ 1 }
    };
    auto timeToWaitDouble = (loopNormalizedDuration - loopElapsedTime);
    if (timeToWaitDouble < seconds_t::zero())
        timeToWaitDouble = seconds_t::zero();
    std::this_thread::sleep_for(timeToWaitDouble);
}

int main(int /*argc*/, char* /*argv*/[])
{
    using namespace om;
    constexpr auto             engineType = IEngine::EngineTypes::sdl;
    constexpr std::string_view config     = "";
    EngineHandler              engine(engineType, config);

    using clock_t = std::chrono::steady_clock;
    World world;

    bool continue_loop = true;
    while (continue_loop)
    {
        Timer                                    loopTimer;
        const std::unordered_set<World::Events>* worldEvents{};
        continue_loop  = input(engine.get(), worldEvents);
        auto isCommand = !worldEvents->empty();

        [[maybe_unused]] auto countWorldUpdating =
            world.update(clock_t::now(), isCommand);
        if (!render(world))
        {
            std::cerr << "Render error" << std::endl;
            continue;
        }
        normalizeLoopDuration(loopTimer.elapsed());
    }

    return EXIT_SUCCESS;
}
