#pragma once
#include <chrono>

class Timer
{
    using clock_t        = std::chrono::steady_clock;
    using seconds_t      = std::chrono::duration<double, std::ratio<1>>;
    using milliseconds_t = std::chrono::duration<double, std::milli>;

private:
    std::chrono::time_point<clock_t> m_beg;

public:
    Timer()
        : m_beg(clock_t::now())
    {
    }

    void reset() { m_beg = clock_t::now(); }

    seconds_t elapsed() const
    {
        return std::chrono::duration_cast<seconds_t>(clock_t::now() - m_beg);
    }
};
