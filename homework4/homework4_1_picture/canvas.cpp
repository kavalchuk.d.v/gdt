#include "canvas.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>

bool operator==(const Position& position1, const Position& position2) noexcept
{
    return (position1.x == position2.x) && (position2.y == position2.y);
}

bool operator!=(const Position& position1, const Position& position2) noexcept
{
    return !(position1 == position2);
}

std::ostream& operator<<(std::ostream& out, const Position& position)
{
    return out << position.x << ' ' << position.y;
}

std::istream& operator>>(std::istream& in, Position& position)
{
    return in >> position.x >> position.y;
}

bool Canvas::setPixel(const Position& position, const Color& color) noexcept
{

    if (!isPositionMatchSize(position))
    {
        return false;
    }
    const auto i = getNumberInBufferFromPosition(position);
    m_pixels[i]  = color;
    return true;
}

void Canvas::fillAll(const Color& color) noexcept
{
    m_pixels.fill(color);
}

bool Canvas::savePictureToPpm(const std::string& fileName)
{
    std::ofstream file{ fileName, std::ios_base::binary };

    if (!static_cast<bool>(file))
    {
        std::cerr << "Cannot open to write file: " << fileName << std::endl;
        return false;
    }
    file << "P" << static_cast<uint_least16_t>(m_mode) << '\n';
    file << m_width << ' ' << m_height << ' '
         << static_cast<uint_least16_t>(m_colorWidth) << '\n';

    const auto bufLen =
        static_cast<std::streamsize>(m_pixels.size() * sizeof(Color));

    // optimization to do not copy elements from m_pixels array to temporary
    // char[] reinterpreted should be used because file.read/write functions
    // support only parameter char*
    auto writeBufferPtr = reinterpret_cast<const char*>(m_pixels.data());
    file.write(writeBufferPtr, bufLen);

    return static_cast<bool>(file);
}

bool Canvas::loadPictureFromPpm(const std::string& fileName)
{
    static const std::string controlHeader = { 'P', m_mode + 0x30 };
    std::ifstream            file{ fileName, std::ios_base::binary };

    if (!static_cast<bool>(file))
    {
        std::cerr << "Cannot open to read file: " << fileName << std::endl;
        return false;
    }
    std::string headerFromFile;
    file >> headerFromFile;
    if (headerFromFile != controlHeader)
    {
        std::cerr << "Mode doesnt match." << std::endl;
        return false;
    }

    size_t         widthFromFile;
    size_t         heightFromFile;
    uint_least16_t colorWidthFromFile;
    file >> widthFromFile >> heightFromFile >> colorWidthFromFile;
    if (m_width != widthFromFile || m_height != heightFromFile)
    {
        std::cerr << "Size of picture doesnt match" << std::endl;
        return false;
    }
    if (colorWidthFromFile != m_colorWidth)
    {
        std::cerr << "Color width doesnt match" << std::endl;
        return false;
    }

    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    if (file.peek() == '#')
    {
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    const auto bufLen =
        static_cast<std::streamsize>(m_pixels.size() * sizeof(Color));

    // optimization to do not copy elements from temporary char[] to m_pixels
    // array reinterpreted should be used because file.read/write functions
    // support only parameter char*
    auto readBufferPtr = reinterpret_cast<char*>(m_pixels.data());

    file.read(readBufferPtr, bufLen);
    if (!static_cast<bool>(file))
    {
        std::cerr << "File has unexpectably finished" << std::endl;
    }
    return static_cast<bool>(file);
}

bool operator==(const Canvas& canvas1, const Canvas& canvas2) noexcept
{
    return canvas1.m_pixels == canvas2.m_pixels;
}

bool Canvas::isPositionMatchSize(const Position& position) const noexcept
{
    const auto currentPositionInBuffer =
        getNumberInBufferFromPosition(position);

    const auto isPositionOk = (currentPositionInBuffer < m_bufferSize);
    if (!isPositionOk)
    {
        std::cerr
            << "Canvas::isPositionMatchSize(): Error position to setPixel "
            << position << std::endl;
    }
    return isPositionOk;
}

Color Canvas::getColorAtPosition(const Position& position) const
{
    const auto numberInBuffer  = getNumberInBufferFromPosition(position);
    const auto colorAtPosition = m_pixels.at(numberInBuffer);
    return colorAtPosition;
}

positionType Canvas::getNumberInBufferFromPosition(
    const Position& position) const
{
    const auto currentPositionInBuffer =
        static_cast<positionType>(position.y) * m_width +
        static_cast<positionType>(position.x);
    return currentPositionInBuffer;
}
