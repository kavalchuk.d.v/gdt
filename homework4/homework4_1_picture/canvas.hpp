#pragma once
#include <array>

using colorType    = uint_least8_t;
using positionType = size_t;

#pragma pack(push, 1)
struct Color
{
    colorType   r = 0;
    colorType   g = 0;
    colorType   b = 0;
    friend bool operator==(const Color& color1, const Color& color2)
    {
        return (color1.r == color2.r) && (color1.g == color2.g) &&
               (color1.b == color2.b);
    }
};
#pragma pack(pop)
#include <iosfwd>
struct Position
{
    /// supposed to be width (horizontal) coordinate
    positionType x = 0;

    /// supposed to be height (vertical) coordinate
    positionType y = 0;

    friend bool operator==(const Position& position1,
                           const Position& position2) noexcept;

    friend bool operator!=(const Position& position1,
                           const Position& position2) noexcept;

    friend std::ostream& operator<<(std::ostream&   out,
                                    const Position& position);

    friend std::istream& operator>>(std::istream& in, Position& position);
};

class Canvas
{
public:
    bool        setPixel(const Position& position, const Color& color) noexcept;
    void        fillAll(const Color& color) noexcept;
    bool        savePictureToPpm(const std::string& fileName);
    bool        loadPictureFromPpm(const std::string& fileName);
    friend bool operator==(const Canvas& canvas1,
                           const Canvas& canvas2) noexcept;
    bool        isPositionMatchSize(const Position& position) const noexcept;
    // static constexpr Position                          m_canvasSize{ 240, 320
    // };

    Color getColorAtPosition(const Position& position) const;

    positionType getNumberInBufferFromPosition(const Position& position) const;
    /// supposed to describe max x coordinate
    static constexpr positionType m_width{ 320 };

    /// supposed to describe max y coordinate
    static constexpr positionType m_height{ 240 };

    /// describes buffer size for picture
    static constexpr positionType  m_bufferSize{ m_width * m_height };
    Color*                         getPixelPtr() { return m_pixels.data(); }
    static constexpr uint_least8_t m_mode{ 6 };
    static constexpr colorType     m_colorWidth{ 255 };

private:
    std::array<Color, m_bufferSize> m_pixels{};
};
