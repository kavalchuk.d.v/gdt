#pragma once
#include "canvas.hpp"
#include <cmath>

using vertexType = double;
struct Vertex
{
    vertexType f0 = 0; /// x
    vertexType f1 = 0; /// y
    vertexType f2 = 0; /// r
    vertexType f3 = 0; /// g
    vertexType f4 = 0; /// b
    vertexType f5 = 0; /// u (texture coordinate)
    vertexType f6 = 0; /// v (texture coordinate)
    vertexType f7 = 0; /// ?
};

struct Uniforms
{
    vertexType f0 = 0;
    vertexType f1 = 0;
    vertexType f2 = 0;
    vertexType f3 = 0;
    vertexType f4 = 0;
    vertexType f5 = 0;
    vertexType f6 = 0;
    vertexType f7 = 0;
};

struct GfxProgram
{
    virtual ~GfxProgram()                              = default;
    virtual void   set_uniforms(const Uniforms&)       = 0;
    virtual Vertex vertex_shader(const Vertex& v_in)   = 0;
    virtual Color  fragment_shader(const Vertex& v_in) = 0;
};

struct Program4 : public GfxProgram
{
    // f6 uniform - width
    // f7 uniform - height
    positionType width;
    positionType height;

    void set_uniforms(const Uniforms& uniform) override
    {
        width  = static_cast<positionType>(uniform.f6);
        height = static_cast<positionType>(uniform.f7);
    }

    Vertex vertex_shader(const Vertex& v_in) override
    {
        Vertex out = v_in;

        const vertexType rotationAngle = 30;
        const Vertex     rotationCenter{ 0, 0 };
        calculateRotation(out, rotationAngle, rotationCenter);

        // scale into 2 times
        out.f0 *= 0.25;
        out.f1 *= 0.25;

        // shift to 10 pix
        out.f0 += 50;
        out.f1 += 50;

        // mirror screen at x
        out.f0 = width - 1 - out.f0;

        return out;
    }

    Color fragment_shader(const Vertex& v_in) override
    {
        Color out;
        out.r = static_cast<uint8_t>(v_in.f2 * 255);
        out.g = static_cast<uint8_t>(v_in.f3 * 255);
        out.b = static_cast<uint8_t>(v_in.f4 * 255);
        return out;
    }

private:
    void calculateRotation(Vertex&          r_vertex,
                           const vertexType rotationAngleDegrees,
                           const Vertex&    rotationCenter = { 0, 0 })
    {
        r_vertex.f0 -= rotationCenter.f0;
        r_vertex.f1 -= rotationCenter.f1;

        // we just turn axes to -angle
        const auto rotationAngleRadian = degreeToRadian(rotationAngleDegrees);

        imposeAxes(r_vertex, rotationAngleRadian);

        r_vertex.f0 += rotationCenter.f0;
        r_vertex.f1 += rotationCenter.f1;
    }

    void imposeAxes(Vertex&          r_vertexToImpose,
                    const vertexType rotationAngleRadian)
    {
        const auto cosAngleAxesToR = std::cos(rotationAngleRadian);
        const auto sinAngleAxesToR = sinFromCos(cosAngleAxesToR);
        const auto inputX          = r_vertexToImpose.f0;
        const auto inputY          = r_vertexToImpose.f1;

        r_vertexToImpose.f0 =
            inputX * cosAngleAxesToR - inputY * sinAngleAxesToR;

        r_vertexToImpose.f1 =
            inputY * cosAngleAxesToR + inputX * sinAngleAxesToR;
    }

    std::array<vertexType, 2> calculateRotationShiftWithoutAxes(
        const vertexType rotationAngle, const vertexType distance)
    {
        std::array<vertexType, 2> outDxDy;
        outDxDy[0] = distance * std::sin(rotationAngle);
        outDxDy[1] = distance * (1 - std::cos(rotationAngle));
        return outDxDy;
    }

    vertexType calculateDistance(const Vertex& vertex,
                                 const Vertex& rotationCenter = { 0, 0 })
    {
        vertexType dX       = (vertex.f0 - rotationCenter.f0);
        vertexType dY       = (vertex.f1 - rotationCenter.f1);
        vertexType distance = std::sqrt(dX * dX + dY * dY);
        return distance;
    }

    vertexType degreeToRadian(const vertexType degreeAngle)
    {
        vertexType outRadianAngle =
            static_cast<vertexType>(M_PI * degreeAngle / 180.0);
        return outRadianAngle;
    }

    vertexType calcCos(const vertexType x, const vertexType distance)
    {
        if (distance < std::numeric_limits<vertexType>::epsilon())
            return 0;
        return x / distance;
    }

    vertexType sinFromCos(const vertexType cosAngle)
    {
        return std::sqrt(1.0 - cosAngle * cosAngle);
    }
};

struct Program4Tex : public GfxProgram
{
    const Canvas* texture;
    void          set_uniforms(const Uniforms&) override {}
    Vertex        vertex_shader(const Vertex& v_in) override
    {
        Vertex out = v_in;
        return out;
    }
    Color fragment_shader(const Vertex& v_in) override
    {
        Color out;

        out.r = static_cast<uint8_t>(v_in.f2 * 255);
        out.g = static_cast<uint8_t>(v_in.f3 * 255);
        out.b = static_cast<uint8_t>(v_in.f4 * 255);

        Color from_texture = sample2d(v_in.f5, v_in.f6);
        //
        out.r = from_texture.r * 0.5;
        out.g = from_texture.g * 0.5;
        out.b = from_texture.b * 0.5;
        return out;
    }

    void setTextureCanvas(const Canvas& tex) { texture = &tex; }

private:
    Color sample2d(double u_, double v_)
    {
        // x in texture
        auto u = static_cast<positionType>(std::round(u_));
        // y in texture
        auto  v = static_cast<positionType>(std::round(v_));
        Color c = texture->getColorAtPosition({ u, v });

        auto c1 = texture->getColorAtPosition({ 269, 50 });
        auto c2 = texture->getColorAtPosition({ 269, 90 });

        return c;
    }
};
