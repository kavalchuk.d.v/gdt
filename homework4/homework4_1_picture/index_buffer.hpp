#pragma once
#include <cstdint>
#include <vector>

using indexBufferType = uint_least16_t;

struct IndexBuffer // index begins from 0
{
    IndexBuffer(uint_least8_t polygonSize)
        : m_polygonSize{ polygonSize }
    {
    }
    IndexBuffer& operator=(IndexBuffer&& indexBuffer)
    {
        if (&indexBuffer == this)
            return *this;
        m_indexes     = std::move(indexBuffer.m_indexes);
        m_polygonSize = indexBuffer.m_polygonSize;
        return *this;
    }
    IndexBuffer(IndexBuffer&& indexBuffer)
        : m_indexes{ std::move(indexBuffer.m_indexes) }
        , m_polygonSize{ indexBuffer.m_polygonSize }
    {
    }

    bool safeAdd(const std::vector<indexBufferType>& additionalIndexes)
    {
        if (additionalIndexes.size() != m_polygonSize)
        {
            return false;
        }
        m_indexes.insert(m_indexes.end(), additionalIndexes.begin(),
                         additionalIndexes.end());
        return true;
    }
    std::vector<indexBufferType> m_indexes;
    uint_least8_t                getPolygonSize() { return m_polygonSize; }

private:
    uint_least8_t m_polygonSize;
};
