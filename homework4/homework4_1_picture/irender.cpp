#include "irender.hpp"
#include <algorithm>
#include <iostream>

using Colors = std::vector<Color>;

IRender::IRender(Canvas& canvas) noexcept
    : m_canvas{ canvas }
{
}
void IRender::clear(const Color& color) noexcept
{
    m_canvas.fillAll(color);
}

bool IRender::draw(const Positions& vertexPositions,
                   const Color&     color) noexcept
{
    if (!vertexPositionsCheck(vertexPositions))
        return false;
    auto pixelsPositions{ this->getPixelsPositions(vertexPositions) };
    for (const auto& currentPixelPosition : pixelsPositions)
    {
        if (!m_canvas.setPixel(currentPixelPosition, color))
        {
            return false;
        }
    }
    return true;
}

bool IRender::draw(const Positions&   allVertexPositions,
                   const IndexBuffer& indexBufferForOneTriangle,
                   const Color&       color) noexcept
{
    Positions polygonVertexesPositions;
    for (const auto& currentIndex : indexBufferForOneTriangle.m_indexes)
    {
        const auto& currentVertex = allVertexPositions[currentIndex];
        polygonVertexesPositions.push_back(currentVertex);
    }

    return draw(polygonVertexesPositions, color);
}

bool IRender::drawMany(const Positions&   allVertexPositions,
                       const IndexBuffer& allIndexBuffer,
                       const Color&       color) noexcept
{
    for (indexBufferType currentIndexOfIndex = 0;
         currentIndexOfIndex < allIndexBuffer.m_indexes.size();
         currentIndexOfIndex += getVertexNumber())
    {

        std::vector<indexBufferType> tempIndexBuffer;
        tempIndexBuffer.push_back(
            allIndexBuffer.m_indexes[currentIndexOfIndex]);
        tempIndexBuffer.push_back(
            allIndexBuffer.m_indexes[currentIndexOfIndex + 1]);
        tempIndexBuffer.push_back(
            allIndexBuffer.m_indexes[currentIndexOfIndex + 2]);

        IndexBuffer indexBufferForTrangle(getVertexNumber());
        indexBufferForTrangle.safeAdd(tempIndexBuffer);
        if (!draw(allVertexPositions, indexBufferForTrangle, color))
            return false;
    }
    return true;
}

bool IRender::vertexPositionsCheck(const Positions& vertexPositions) const
    noexcept
{
    if (vertexPositions.size() != this->getVertexNumber())
    {
        std::cerr << "Incorrect number of paramaters" << std::endl;
        return false;
    }
    for (const auto& currentVertexPosition : vertexPositions)
    {
        if (!m_canvas.isPositionMatchSize(currentVertexPosition))
        {
            std::cerr << "Vertex position doesnt match buffer size"
                      << std::endl;
            return false;
        }
    }
    return true;
}
