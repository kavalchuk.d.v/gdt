#pragma once
#include "canvas.hpp"
#include "index_buffer.hpp"
#include <vector>

using Positions = std::vector<Position>;

class IRender
{
public:
    using Colors = std::vector<Color>;

    IRender(Canvas& canvas) noexcept;

    virtual ~IRender() noexcept {}

    void clear(const Color& color) noexcept;

    bool draw(const Positions& vertexPositions, const Color& color) noexcept;

    bool draw(const Positions&   allVertexPositions,
              const IndexBuffer& indexBufferForOneTriangle,
              const Color&       color) noexcept;

    bool drawMany(const Positions&   allVertexPositions,
                  const IndexBuffer& allIndexBuffer,
                  const Color&       color) noexcept;

    virtual size_t getVertexNumber() const noexcept = 0;

protected:
    virtual bool vertexPositionsCheck(const Positions& vertexPositions) const
        noexcept;

    virtual Positions getPixelsPositions(const Positions& vertexPositions) const
        noexcept = 0;
    Canvas& m_canvas;
};
