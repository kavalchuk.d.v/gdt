#include "linerender.hpp"
#include <cmath>
#include <iostream>

using Colors = std::vector<Color>;

size_t LineRender::getVertexNumber() const noexcept
{
    return 2;
}

Positions LineRender::getPixelsPositions(const Positions& vertexPositions) const
    noexcept
{
    // algorithm Brezenhaime implementation
    const auto&  begin = vertexPositions[0];
    const auto&  end   = vertexPositions[1];
    const size_t distX =
        std::abs(static_cast<int>(end.x) - static_cast<int>(begin.x));
    const size_t distY =
        std::abs(static_cast<int>(end.y) - static_cast<int>(begin.y));

    auto currentPosition = begin;

    auto isLongSideX = (distX >= distY);

    Positions outPositions;

    int stepX = (end.x >= begin.x) ? 1 : -1;
    int stepY = (end.y >= begin.y) ? 1 : -1;

    size_t error = 0UL;

    if (isLongSideX)
    {
        outPositions.reserve(distX + 1);
        while (currentPosition.x != end.x)
        {
            outPositions.push_back(currentPosition);
            error += distY + 1;
            currentPosition.x += stepX;
            if (error >= (distX + 1))
            {
                currentPosition.y += stepY;
                error -= distX + 1;
            }
        }
    }
    else
    {
        outPositions.reserve(distY + 1);
        while (currentPosition.y != end.y)
        {
            outPositions.push_back(currentPosition);
            error += distX + 1;
            currentPosition.y += stepY;
            if (error >= (distY + 1))
            {
                currentPosition.x += stepX;
                error -= distY + 1;
            }
        }
    }
    outPositions.push_back(currentPosition);
    return outPositions;
}
