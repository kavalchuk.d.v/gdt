#pragma once
#include "irender.hpp"

class LineRender : public IRender
{
public:
    LineRender(Canvas& canvas) noexcept
        : IRender{ canvas }
    {
    }
    size_t getVertexNumber() const noexcept override;

protected:
    Positions getPixelsPositions(const Positions& vertexPositions) const
        noexcept override;
};
