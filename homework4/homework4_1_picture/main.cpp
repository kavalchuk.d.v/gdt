#include "canvas.hpp"
#include <cstdlib>
#include <iostream>
#include <string>

int main()
{
    const std::string fileName{ "mypic.ppm" };
    Canvas            canvas{};
    constexpr Color   green{ 0, 255, 0 };
    constexpr Color   black{ 0, 0, 0 };

    canvas.fillAll(green);
    for (Position currentPosition{ 10, 100 }; currentPosition.x < 50;
         ++currentPosition.x)
        canvas.setPixel({ currentPosition }, black);
    if (canvas.savePictureToPpm(fileName))
    {
        std::cerr << "Save ok" << std::endl;
        return EXIT_FAILURE;
    }
    Canvas canvas2{};
    if (canvas2.loadPictureFromPpm(fileName))
    {
        std::cerr << "Load ok" << std::endl;
        return EXIT_FAILURE;
    }

    if (canvas == canvas2)
    {
        std::cerr << "Picture are same" << std::endl;
    }
    else
    {
        std::cerr << "Picture arent same" << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
