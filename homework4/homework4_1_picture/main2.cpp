#include <cstdlib>
#include <iostream>
#include <string>

#include "canvas.hpp"
#include "linerender.hpp"
#include <random>

int getRandom(int lowRange, int highRange)
{
    static std::random_device rd;
    static std::mt19937       mersenne(rd());
    return ((mersenne()) % (highRange - lowRange + 1) + lowRange);
}

int main()
{
    using Positions = std::vector<Position>;

    const std::string fileName{ "mypicLine.ppm" };
    Canvas            canvas{};
    constexpr Color   green{ 0, 255, 0 };
    constexpr Color   black{ 0, 0, 0 };
    LineRender        lineRender{ canvas };
    IRender&          render{ lineRender };
    render.clear(black);

    Positions lineVertexes{ Position{ 0, 0 }, Position{ canvas.m_width - 1,
                                                        canvas.m_height - 1 } };
    render.draw(lineVertexes, green);

    for (auto i = 0; i < 20; ++i)
    {
        Position position0;
        position0.x = getRandom(0, canvas.m_width - 1);
        position0.y = getRandom(0, canvas.m_height - 1);

        Position position1;
        position1.x        = getRandom(0, canvas.m_width - 1);
        position1.y        = getRandom(0, canvas.m_height - 1);
        auto  maxColorType = canvas.m_colorWidth;
        Color color{ static_cast<colorType>(getRandom(0, maxColorType)),
                     static_cast<colorType>(getRandom(0, maxColorType)),
                     static_cast<colorType>(getRandom(0, maxColorType)) };
        render.draw(Positions{ position0, position1 }, color);
    }

    canvas.savePictureToPpm(fileName);
}
