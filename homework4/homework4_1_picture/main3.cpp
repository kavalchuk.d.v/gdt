#include "index_buffer.hpp"
#include "polygonrender.hpp"
#include <cstdlib>
#include <iostream>
#include <random>
#include <string>

int getRandom(int lowRange, int highRange)
{
    static std::random_device rd;
    static std::mt19937       mersenne(rd());
    return ((mersenne()) % (highRange - lowRange + 1) + lowRange);
}

Positions formRectangleGridVertexBuffer(const Position& sizeGrid,
                                        const Position& step,
                                        const Position& initialPos)
{
    Positions vertexBuffer{};
    for (Position currentPos = initialPos; currentPos.x < sizeGrid.x;
         currentPos.x += step.x)
    {
        for (currentPos.y = initialPos.y; currentPos.y < sizeGrid.y;
             currentPos.y += step.y)
        {
            vertexBuffer.push_back(currentPos);
        }
    }
    return vertexBuffer;
}

IndexBuffer formRectangleGridIndexBuffer(indexBufferType numberRowOfVertexes,
                                         indexBufferType numberVertexesInRow)
{
    IndexBuffer indexBuffer(3);
    for (indexBufferType rowNumber = 0; rowNumber < numberRowOfVertexes - 1;
         ++rowNumber)
    {
        indexBufferType beginOfThisRow = rowNumber * numberVertexesInRow;
        indexBufferType beginOfNextRow = (rowNumber + 1) * numberVertexesInRow;
        for (indexBufferType positionInRow = 0;
             positionInRow < numberVertexesInRow - 1; ++positionInRow)
        {
            {
                std::vector<indexBufferType> directTriangleIndexes;
                directTriangleIndexes.push_back(static_cast<indexBufferType>(
                    beginOfThisRow + positionInRow));
                directTriangleIndexes.push_back(directTriangleIndexes[0] + 1);
                directTriangleIndexes.push_back(static_cast<indexBufferType>(
                    beginOfNextRow + positionInRow));
                indexBuffer.safeAdd(directTriangleIndexes);
            }
            {
                std::vector<indexBufferType> backTriangleIndexes;
                backTriangleIndexes.push_back(static_cast<indexBufferType>(
                    beginOfThisRow + positionInRow + 1));
                backTriangleIndexes.push_back(static_cast<indexBufferType>(
                    beginOfNextRow + positionInRow));
                backTriangleIndexes.push_back(backTriangleIndexes[1] + 1);
                indexBuffer.safeAdd(backTriangleIndexes);
            }
        }
    }
    return indexBuffer;
}

bool drawRectangleGrid(Canvas& canvas, const Position& sizeGrid, Color color,
                       const Position& step       = Position{ 10, 10 },
                       const Position& initialPos = Position{})
{
    const auto& vertexBuffer =
        formRectangleGridVertexBuffer(sizeGrid, step, initialPos);

    const indexBufferType numberRowOfVertexes = static_cast<indexBufferType>(
        std::ceil(static_cast<double>(sizeGrid.x) / step.x));
    const indexBufferType numberVertexesInRow = static_cast<indexBufferType>(
        std::ceil(static_cast<double>(sizeGrid.y) / step.y));
    const auto& indexBuffer =
        formRectangleGridIndexBuffer(numberRowOfVertexes, numberVertexesInRow);

    TriangleRender triangleRender{ canvas };
    IRender&       render{ triangleRender };
    return render.drawMany(vertexBuffer, indexBuffer, color);
}

/// Draw skeleton of polygones with using IndexBuffer
int main()
{
    const std::string fileName{ "myPicIndexedTriangle.ppm" };
    Canvas            canvas{};
    constexpr Color   green{ 0, 255, 0 };
    constexpr Color   black{ 0, 0, 0 };
    TriangleRender    triangleRender{ canvas };
    IRender&          render{ triangleRender };
    render.clear(black);
    Positions triangleVertexes{ Position{ 0, 0 },
                                Position{ canvas.m_width - 1, 0 },
                                Position{ 0, canvas.m_height - 1 } };
    if (!render.draw(triangleVertexes, green))
        std::cerr << "When drawing first triangle some pixels cannot be set. "
                     "Please check "
                     "your algorithm."
                  << std::endl;

    if (!drawRectangleGrid(canvas, { canvas.m_width, canvas.m_height }, green))
    {
        std::cerr << "Error when draw grid has occured" << std::endl;
    }
    canvas.savePictureToPpm(fileName);
}
