#include "gfx_program.hpp"
#include "index_buffer.hpp"
#include "triangle_interpolated.hpp"
#include <cstdlib>
#include <random>
#include <string>

int getRandom(int lowRange, int highRange)
{
    static std::random_device rd;
    static std::mt19937       mersenne(rd());
    return ((mersenne()) % (highRange - lowRange + 1) + lowRange);
}

int main()
{
    const std::string fileName{ "myPicInterpolTriangle.ppm" };
    Canvas            canvas{};
    const Color       black{ 0, 0, 0 };

    Program4 program01;
    program01.set_uniforms(
        { 0, 0, 0, 0, 0, 0, canvas.m_width, canvas.m_height });
    TriangleInterpolatedRender interpolatedRender{ canvas, program01 };

    interpolatedRender.clear(black);

    Vertexes triangle_v{ { 0, 0, 1, 0, 0, 296, 98, 0 },
                         { 0, canvas.m_height - 1, 0, 1, 0, 269, 53, 0 },
                         { canvas.m_width - 1, 0, 0, 0, 1, 202, 90, 0 } };

    std::vector<indexBufferType> indexes_v{ 0, 1, 2 };
    IndexBuffer                  indexBuffer{ 3 };
    indexBuffer.safeAdd(indexes_v);

    interpolatedRender.drawInterpolatedMany(triangle_v, indexBuffer);

    canvas.savePictureToPpm(fileName);

    // texture example
    Program4Tex program02;

    program02.setTextureCanvas(canvas);
    Canvas                     canvas2{};
    TriangleInterpolatedRender interpolatedRender2{ canvas2, program01 };

    interpolatedRender2.SetGfxProgram(program02);

    interpolatedRender2.clear(black);

    interpolatedRender2.drawInterpolatedMany(triangle_v, indexBuffer);
    const std::string fileName2{ "myPicInterpolTriangleTexture.ppm" };
    canvas2.savePictureToPpm(fileName2);
}
