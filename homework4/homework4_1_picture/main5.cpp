#include "gfx_program.hpp"
#include "index_buffer.hpp"
#include "triangle_interpolated.hpp"
#include <SDL.h>
#include <cstdlib>
#include <iostream>

int main(int, char**)
{
    using namespace std;

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow(
        "runtime soft render", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        Canvas::m_width, Canvas::m_height, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    const Color black = { 0, 0, 0 };

    Canvas image;

    struct program : public GfxProgram
    {
        double mouse_x{};
        double mouse_y{};
        double radius{};

        void set_uniforms(const Uniforms& a_uniforms) override
        {
            mouse_x = a_uniforms.f0;
            mouse_y = a_uniforms.f1;
            radius  = a_uniforms.f2;
        }
        Vertex vertex_shader(const Vertex& v_in) override
        {
            Vertex out = v_in;

            return out;
        }

        Color fragment_shader(const Vertex& v_in) override
        {
            Color out;
            out.r = static_cast<uint8_t>(v_in.f2 * 255);
            out.g = static_cast<uint8_t>(v_in.f3 * 255);
            out.b = static_cast<uint8_t>(v_in.f4 * 255);

            double x  = v_in.f0;
            double y  = v_in.f1;
            double dx = mouse_x - x;
            double dy = mouse_y - y;
            if (dx * dx + dy * dy < radius * radius)
            {
                // make pixel gray if mouse cursor around current pixel with
                // radius
                // gray scale with formula: 0.21 R + 0.72 G + 0.07 B.
                double gray = 0.21 * out.r + 0.72 * out.g + 0.07 * out.b;
                out.r       = gray;
                out.g       = gray;
                out.b       = gray;
            }

            return out;
        }
    } program01;

    TriangleInterpolatedRender interpolated_render(image, program01);

    Vertexes triangle_v{ { 0, 0, 1, 0, 0, 0, 0, 0 },
                         { 0, Canvas::m_height - 1, 0, 1, 0, 0, 239, 0 },
                         { Canvas::m_width - 1, 0, 0, 0, 1, 319, 239, 0 } };

    std::vector<indexBufferType> indexes_v{ 0, 1, 2 };
    IndexBuffer                  indexBuffer{ 3 };
    indexBuffer.safeAdd(indexes_v);

    void*     pixels = image.getPixelPtr();
    const int depth  = sizeof(Color) * 8;
    const int pitch  = Canvas::m_width * sizeof(Color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    double mouse_x{};
    double mouse_y{};
    double radius{ 20.0 }; // 20 pixels radius

    bool continue_loop = true;

    while (continue_loop)
    {
        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                continue_loop = false;
                break;
            }
            else if (e.type == SDL_MOUSEMOTION)
            {
                mouse_x = e.motion.x;
                mouse_y = e.motion.y;
            }
            else if (e.type == SDL_MOUSEWHEEL)
            {
                radius *= e.wheel.y;
            }
        }

        interpolated_render.clear(black);
        program01.set_uniforms(Uniforms{ mouse_x, mouse_y, radius });

        interpolated_render.drawInterpolatedMany(triangle_v, indexBuffer);

        SDL_Surface* bitmapSurface =
            SDL_CreateRGBSurfaceFrom(pixels, Canvas::m_width, Canvas::m_height,
                                     depth, pitch, rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_Texture* bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }
        SDL_FreeSurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
