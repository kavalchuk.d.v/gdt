#pragma once
#include "linerender.hpp"

template <size_t vertexNumber>
class PolygonRender : public LineRender
{
public:
    PolygonRender(Canvas& canvas) noexcept
        : LineRender(canvas)
    {
    }

    size_t getVertexNumber() const noexcept override;

protected:
    Positions getPixelsPositions(const Positions& vertexPositions) const
        noexcept override;

    const Positions::const_iterator findNextVertexPolygon(
        const Positions&                vertexPositions,
        const Positions::const_iterator vertexPositionIt) const noexcept;
};

using TriangleRender = PolygonRender<3>;

#include "polygonrender.inl"
