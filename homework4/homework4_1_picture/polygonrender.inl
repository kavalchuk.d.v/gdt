#include "polygonrender.hpp"

template <size_t vertexNumber>
size_t PolygonRender<vertexNumber>::getVertexNumber() const noexcept
{
    return vertexNumber;
}

template <size_t vertexNumber>
Positions PolygonRender<vertexNumber>::getPixelsPositions(
    const Positions& vertexPositions) const noexcept
{
    Positions pixels{};

    for (auto vertexPositionIt = vertexPositions.begin();
         vertexPositionIt < vertexPositions.end(); ++vertexPositionIt)
    {
        auto nextVertexIt{ findNextVertexPolygon(vertexPositions,
                                                 vertexPositionIt) };

        Positions  currentLineVertexes{ *vertexPositionIt, *nextVertexIt };
        const auto currentLinePixels{ LineRender::getPixelsPositions(
            currentLineVertexes) };
        pixels.insert(pixels.end(), currentLinePixels.begin(),
                      currentLinePixels.end());
    }
    return pixels;
}

template <size_t vertexNumber>
const Positions::const_iterator
PolygonRender<vertexNumber>::findNextVertexPolygon(
    const Positions&                vertexPositions,
    const Positions::const_iterator vertexPositionIt) const noexcept
{
    if (vertexPositionIt + 1 < vertexPositions.end())
    {
        return vertexPositionIt + 1;
    }
    else
    {
        return vertexPositions.begin();
    }
}
