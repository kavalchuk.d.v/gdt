#include "triangle_interpolated.hpp"
#include <algorithm>
#include <cmath>
#include <iostream>

bool TriangleInterpolatedRender::drawInterpolatedMany(
    const Vertexes& bufferAllVertexes, const IndexBuffer& bufferAllIndexes)
{
    for (indexBufferType currentIndexOfIndex = 0;
         currentIndexOfIndex < bufferAllIndexes.m_indexes.size();
         currentIndexOfIndex += getVertexNumber())
    {

        std::vector<indexBufferType> tempIndexBuffer;
        tempIndexBuffer.push_back(
            bufferAllIndexes.m_indexes[currentIndexOfIndex]);
        tempIndexBuffer.push_back(
            bufferAllIndexes.m_indexes[currentIndexOfIndex + 1]);
        tempIndexBuffer.push_back(
            bufferAllIndexes.m_indexes[currentIndexOfIndex + 2]);

        IndexBuffer indexBufferForTrangle(getVertexNumber());
        indexBufferForTrangle.safeAdd(tempIndexBuffer);
        if (!drawInterpolated(bufferAllVertexes, indexBufferForTrangle))
            return false;
    }
    return false;
}

bool TriangleInterpolatedRender::drawInterpolated(
    const Vertexes&    bufferAllVertexes,
    const IndexBuffer& indexBufferFor1Triangle)
{
    Vertexes triangleNodeVertexes;
    auto     indexNode0 = indexBufferFor1Triangle.m_indexes[0];
    auto     indexNode1 = indexBufferFor1Triangle.m_indexes[1];
    auto     indexNode2 = indexBufferFor1Triangle.m_indexes[2];

    const auto& vertex1AfterShader =
        m_program->vertex_shader(bufferAllVertexes[indexNode0]);
    const auto& vertex2AfterShader =
        m_program->vertex_shader(bufferAllVertexes[indexNode1]);
    const auto& vertex3AfterShader =
        m_program->vertex_shader(bufferAllVertexes[indexNode2]);

    triangleNodeVertexes.push_back(vertex1AfterShader);
    triangleNodeVertexes.push_back(vertex2AfterShader);
    triangleNodeVertexes.push_back(vertex3AfterShader);

    const auto& interpolatedTriangleVertexes =
        getRasterizedVertexes(triangleNodeVertexes);

    for (const auto& currentVertex : interpolatedTriangleVertexes)
    {

        const auto& colorAfterShader =
            m_program->fragment_shader(currentVertex);

        auto finalCoordinateX =
            static_cast<size_t>(std::round(currentVertex.f0));
        auto finalCoordinateY =
            static_cast<size_t>(std::round(currentVertex.f1));
        const Position& finalPosition{ finalCoordinateX, finalCoordinateY };

        if (!m_canvas.setPixel(finalPosition, colorAfterShader))
        {
            return false;
        }
    }
    return true;
}

Vertexes TriangleInterpolatedRender::getRasterizedVertexes(
    const Vertexes& nodesRectangularVertexes) const noexcept
{
    std::vector<const Vertex*> vectorPtrVertexes;

    if (nodesRectangularVertexes.size() != 3)
    {
        std::cerr << "Number of nodes mismatch number of nodes for triangle."
                  << std::endl;
        return Vertexes{};
    }

    vectorPtrVertexes.push_back(&(nodesRectangularVertexes[0]));
    vectorPtrVertexes.push_back(&(nodesRectangularVertexes[1]));
    vectorPtrVertexes.push_back(&(nodesRectangularVertexes[2]));

    auto sortFun = [](const Vertex* vertex0, const Vertex* vertex1) {
        return vertex0->f1 < vertex1->f1;
    };

    std::sort(vectorPtrVertexes.begin(), vectorPtrVertexes.end(), sortFun);

    const auto& lowerVertex = *(vectorPtrVertexes[0]);
    const auto& midVertex   = *(vectorPtrVertexes[1]);
    const auto& upperVertex = *(vectorPtrVertexes[2]);

    const auto& additionalMidVertex =
        findAdditionalMidVertex(lowerVertex, upperVertex, midVertex.f1);

    const auto& horyzontalLowerTriangle = rasterizeHoryzontalTriangle(
        lowerVertex, midVertex, additionalMidVertex);

    const auto& horyzontalUpperTriangle = rasterizeHoryzontalTriangle(
        upperVertex, midVertex, additionalMidVertex);

    if ((horyzontalLowerTriangle.size() == 0) ||
        (horyzontalUpperTriangle.size() == 0))
    {
        std::cerr << "An error occured when raster triangle." << std::endl;
        return Vertexes{};
    }

    std::vector<Vertexes> triangleVector;
    triangleVector.push_back(std::move(horyzontalUpperTriangle));
    triangleVector.push_back(std::move(horyzontalLowerTriangle));

    Vertexes ourVertexesForTriangle = joinInterpolatedTriangles(triangleVector);

    return ourVertexesForTriangle;
}

Vertexes TriangleInterpolatedRender::joinInterpolatedTriangles(
    const std::vector<Vertexes>& triangles) const noexcept
{
    Vertexes ourVertexesForTriangle;
    size_t   fullNeededCapacity{};

    for (const auto triangle : triangles)
    {
        fullNeededCapacity += triangle.size();
    }

    ourVertexesForTriangle.reserve(fullNeededCapacity);

    for (const auto triangle : triangles)
    {
        ourVertexesForTriangle.insert(ourVertexesForTriangle.end(),
                                      triangle.begin(), triangle.end());
    }
    return ourVertexesForTriangle;
}

Vertex TriangleInterpolatedRender::findAdditionalMidVertex(
    const Vertex& lowerVertex, const Vertex& upperVertex,
    vertexType middleF1) const noexcept
{
    auto addtionalMidVertexKoef =
        (middleF1 - lowerVertex.f1) / (upperVertex.f1 - lowerVertex.f1);
    return interpolate(lowerVertex, upperVertex, addtionalMidVertexKoef);
}

Vertexes TriangleInterpolatedRender::rasterizeHoryzontalTriangle(
    const Vertex& single, const Vertex& horizontSide0,
    const Vertex& horizontSide1) const noexcept
{
    Vertexes interpolatedTriangleVertexes{};

    auto distance =
        static_cast<size_t>(std::round(std::abs(single.f1 - horizontSide0.f1)));

    if (distance == 0)
    {
        return rasterizeHoryzontalLine(horizontSide0, horizontSide1);
    }

    distance += 0;

    const auto longSideLength =
        static_cast<size_t>(
            std::round(std::abs(horizontSide1.f0 - horizontSide0.f0))) +
        1U;
    const auto vertexNumberPredict = (distance * longSideLength) / 2 + 1;
    interpolatedTriangleVertexes.reserve(vertexNumberPredict);

    for (size_t currentStep = 0; currentStep <= distance; ++currentStep)
    {
        auto interpolatingKoef = static_cast<double>(currentStep) / distance;
        auto currentLineVertexSide0 =
            interpolate(horizontSide0, single, interpolatingKoef);
        auto currentLineVertexSide1 =
            interpolate(horizontSide1, single, interpolatingKoef);

        const auto& currentLineRasteredVertexes = rasterizeHoryzontalLine(
            currentLineVertexSide0, currentLineVertexSide1);

        interpolatedTriangleVertexes.insert(interpolatedTriangleVertexes.end(),
                                            currentLineRasteredVertexes.begin(),
                                            currentLineRasteredVertexes.end());
    }
    return interpolatedTriangleVertexes;
}

Vertexes TriangleInterpolatedRender::rasterizeHoryzontalLine(
    const Vertex& vertex0, const Vertex& vertex1) const noexcept
{
    auto distance =
        static_cast<size_t>(std::round(std::abs(vertex1.f0 - vertex0.f0)));
    if (distance == 0)
    {
        return Vertexes{ vertex0 };
    }

    distance += 1;

    Vertexes interpolatedLineVertexes;
    interpolatedLineVertexes.reserve(distance);
    for (size_t currentStep = 0; currentStep <= distance; ++currentStep)
    {
        auto interpolatingKoef = static_cast<double>(currentStep) / distance;
        auto currentLineVertex =
            interpolate(vertex0, vertex1, interpolatingKoef);
        interpolatedLineVertexes.push_back(currentLineVertex);
    }
    return interpolatedLineVertexes;
}

Vertex TriangleInterpolatedRender::interpolate(const Vertex& vertex0,
                                               const Vertex& vertex1,
                                               vertexType    koef) noexcept
{
    return Vertex{ interpolate(vertex0.f0, vertex1.f0, koef),
                   interpolate(vertex0.f1, vertex1.f1, koef),
                   interpolate(vertex0.f2, vertex1.f2, koef),
                   interpolate(vertex0.f3, vertex1.f3, koef),
                   interpolate(vertex0.f4, vertex1.f4, koef),
                   interpolate(vertex0.f5, vertex1.f5, koef),
                   interpolate(vertex0.f6, vertex1.f6, koef),
                   interpolate(vertex0.f7, vertex1.f7, koef) };
}

vertexType TriangleInterpolatedRender::interpolate(
    const vertexType param_begin, const vertexType param_end,
    const vertexType koef) noexcept
{
    if (koef > 1 || koef < 0)
    {
        std::cerr << "Koef miss ranges: " << koef << std::endl;
    }
    return param_begin + (param_end - param_begin) * koef;
}
