#pragma once
#include "gfx_program.hpp"
#include "polygonrender.hpp"

using Vertexes = std::vector<Vertex>;

class TriangleInterpolatedRender : public TriangleRender
{
public:
    TriangleInterpolatedRender(Canvas& canvas, GfxProgram& program)
        : TriangleRender{ canvas }
        , m_program{ &program }
    {
    }
    void SetGfxProgram(GfxProgram& program) { m_program = &program; }

    bool drawInterpolatedMany(const Vertexes&    bufferAllVertexes,
                              const IndexBuffer& bufferAllIndexes);
    bool drawInterpolated(const Vertexes&    bufferAllVertexes,
                          const IndexBuffer& indexBufferFor1Triangle);

protected:
    Vertexes getRasterizedVertexes(
        const Vertexes& nodesRectangularVertexes) const noexcept;

    Vertexes useOptimization(Vertexes sortedNodesRectangularVertexes) const
        noexcept;

    Vertex findAdditionalMidVertex(const Vertex& lowerVertex,
                                   const Vertex& upperVertex,
                                   vertexType    middleF1) const noexcept;

    Vertexes joinInterpolatedTriangles(
        const std::vector<Vertexes>& triangles) const noexcept;

    Vertexes rasterizeHoryzontalTriangle(const Vertex& single,
                                         const Vertex& horizontSide0,
                                         const Vertex& horizontSide1) const
        noexcept;

    Vertexes rasterizeHoryzontalLine(const Vertex& leftVertex,
                                     const Vertex& rigthVertex) const noexcept;

    static Vertex interpolate(const Vertex& vertex0, const Vertex& vertex1,
                              vertexType koef) noexcept;

    static vertexType interpolate(const vertexType param_begin,
                                  const vertexType param_end,
                                  const vertexType koef) noexcept;

    GfxProgram* m_program = nullptr;
};
