cmake_minimum_required(VERSION 3.16)

project(homework5 LANGUAGES CXX C)

add_library(engine_lib_opengl SHARED
    engine_handler.cpp
    engine_sdl.cpp
    engine_sdl.hpp
    iengine.hpp
    engine_handler.hpp
    glad/include/glad/glad.h
    glad/include/KHR/khrplatform.h
    glad/src/glad.c
    vertex_shader_1.vert
    fragment_shader_1.frag
    )

target_compile_features(engine_lib_opengl PUBLIC cxx_std_17)

find_package(SDL2 REQUIRED)

target_include_directories (engine_lib_opengl
    PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}
    PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/glad/include/glad"}
    "${CMAKE_CURRENT_SOURCE_DIR}/glad/include/KHR")

#find_package(OpenGL REQUIRED)

target_link_libraries(
        engine_lib_opengl
        PRIVATE
        SDL2::SDL2
        SDL2::SDL2main
#        OpenGL::GL
)

# TODO windows implementation should be added
add_executable(homework5_1
    main.cpp
    world.cpp
    world.hpp
    engine_handler.hpp
    utilities.cpp
    utilities.hpp
    environement.cpp
    environement.hpp
    renderwrapper.cpp
    renderwrapper.hpp
    vertexes.txt
    )

target_compile_features(homework5_1 PUBLIC cxx_std_17)

target_link_libraries(
        homework5_1
        PRIVATE
        engine_lib_opengl
)






