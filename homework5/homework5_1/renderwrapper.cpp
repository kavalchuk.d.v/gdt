#include "renderwrapper.hpp"
#include <iostream>

const om::myGlfloat RenderWrapper::minStep{ 0.005 };
const om::myGlfloat RenderWrapper::gridZcoord{ 0.0 };

RenderWrapper::RenderWrapper(std::array<om::myGlfloat, 3> color,
                             om::myGlfloat                gridStep)
{
    if (!initGridBuffers(color, gridStep))
    {
        std::cerr << "Cannot create RenderWrapper(). Color or gridStep are "
                     "incorrrect."
                  << std::endl;
        throw std::runtime_error(
            "Cannot create RenderWrapper(). Color or gridStep are "
            "incorrrect.");
    }

    m_color    = color;
    m_gridStep = gridStep;
}

bool RenderWrapper::initGridBuffers(std::array<om::myGlfloat, 3> color,
                                    om::myGlfloat                step)
{
    if (!(checkColor(color) && checkStep(step)))
    {
        return false;
    }

    const auto numberOfRow =
        static_cast<size_t>(
            (om::IEngine::maxCoordinate - om::IEngine::minCoordinate) / step) +
        1;

    const auto numberOfVertecesInRow = numberOfRow;

    m_vertexBuffer = formRectangleGridVertexBuffer(color, step, numberOfRow,
                                                   numberOfVertecesInRow);
    m_indexBuffer =
        formRectangleGridIndexBuffer(numberOfRow, numberOfVertecesInRow);
    return true;
}

bool RenderWrapper::checkColor(std::array<om::myGlfloat, 3> color)
{
    for (const auto colorPart : color)
    {
        if (colorPart > 1.f && colorPart < 0.f)
        {
            return false;
        }
    }
    return true;
}

bool RenderWrapper::checkStep(om::myGlfloat step)
{
    if (step < minStep && step > 1)
    {
        return false;
    }
    return true;
}

void RenderWrapper::render(om::IEngine& engine, const World& world)
{
    renderRectangleGrid(engine);
    renderWorld(engine, world);
}

void RenderWrapper::renderWorld(om::IEngine& engine, const World& world)
{
    const om::myGlfloat worldShipSizeX{ 0.1 };
    const om::myGlfloat worldShipSizeY{ 0.1 };

    const om::myGlfloat worldShipZ =
        ((gridZcoord - 0.1) >= -1.0) ? gridZcoord - 0.1 : gridZcoord;

    const om::myGlfloat windowSize{ om::IEngine::maxCoordinate -
                                    om::IEngine::minCoordinate };

    const om::myGlfloat worldWidth = static_cast<om::myGlfloat>(world.sizeX);

    const om::myGlfloat currentShipX = static_cast<om::myGlfloat>(world.x);

    const om::myGlfloat worldShipXmiddle{
        currentShipX / worldWidth * windowSize - 1
    };

    const om::myGlfloat worldShipXLeft{ worldShipXmiddle - worldShipSizeX / 2 };

    const om::myGlfloat worldShipXRight{ worldShipXmiddle +
                                         worldShipSizeX / 2 };

    const om::myGlfloat worldHeight = static_cast<om::myGlfloat>(world.sizeY);

    const om::myGlfloat currentShipY = static_cast<om::myGlfloat>(world.y);

    const om::myGlfloat worldShipYdown{
        currentShipY / worldHeight * windowSize - 1
    };

    const om::myGlfloat worldShipYup{ worldShipYdown + worldShipSizeY };

    const om::Vertex vertex1{
        worldShipXLeft, worldShipYdown, worldShipZ, 0, 0, 0
    };
    const om::Vertex vertex2{
        worldShipXmiddle, worldShipYup, worldShipZ, 1, 0.3, 0.15
    };
    const om::Vertex vertex3{
        worldShipXRight, worldShipYdown, worldShipZ, 0, 0, 0
    };
    const om::Triangle worldShipPolygon{ vertex1, vertex2, vertex3 };
    engine.renderTriangle(worldShipPolygon);
}

void RenderWrapper::renderRectangleGrid(om::IEngine& engine)
{
    engine.render(m_vertexBuffer, m_indexBuffer, om::ShapeType::line);
}

std::vector<om::Vertex> RenderWrapper::formRectangleGridVertexBuffer(
    std::array<om::myGlfloat, 3> color, om::myGlfloat step,
    const size_t numberOfRow, const size_t numberOfVertecesInRow)
{
    std::vector<om::Vertex> vertexBuffer;

    const auto vertexNumber =
        static_cast<size_t>(numberOfRow * numberOfVertecesInRow);

    vertexBuffer.reserve(vertexNumber);
    for (size_t currentRow = 0; currentRow < numberOfRow; ++currentRow)
    {
        for (size_t currentVeretexInRow = 0;
             currentVeretexInRow < numberOfVertecesInRow; ++currentVeretexInRow)
        {
            const auto currentPosX =
                om::IEngine::minCoordinate + currentRow * step;
            const auto currentPosY =
                om::IEngine::minCoordinate + currentVeretexInRow * step;
            vertexBuffer.push_back({ currentPosX, currentPosY, gridZcoord,
                                     color[0], color[1], color[2] });
        }
    }
    return vertexBuffer;
}

std::vector<om::myUint> RenderWrapper::formRectangleGridIndexBuffer(
    om::myUint numberRowOfVertexes, om::myUint numberVertexesInRow)
{
    const size_t numberOfIndices =
        numberRowOfVertexes * (numberVertexesInRow - 1) * 4;

    std::vector<om::myUint> indexBuffer{};

    indexBuffer.reserve(numberOfIndices);
    // Horyzontal lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes; ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        for (om::myUint positionInRow = 0;
             positionInRow < numberVertexesInRow - 1; ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = thisIndex + 1;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    // Vertical lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes - 1;
         ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        const auto beginOfNextRow = (rowNumber + 1) * numberVertexesInRow;
        for (om::myUint positionInRow = 0; positionInRow < numberVertexesInRow;
             ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = beginOfNextRow + positionInRow;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    return indexBuffer;
}
