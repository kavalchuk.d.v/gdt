#include "utilities.hpp"
#include <thread>

Timer::Timer()
    : m_beg(clock_t::now())
{
}

void Timer::reset()
{
    m_beg = clock_t::now();
}

Timer::seconds_t Timer::elapsed() const
{
    return std::chrono::duration_cast<seconds_t>(clock_t::now() - m_beg);
}

void normalizeLoopDuration(
    std::chrono::duration<double, std::ratio<1>> loopElapsedTime)
{
    using seconds_t      = std::chrono::duration<double, std::ratio<1>>;
    using milliseconds_t = std::chrono::duration<double, std::milli>;

    static constexpr int       refreshRate = 60;
    static constexpr seconds_t loopNormalizedDuration{
        (seconds_t{ 1 } / refreshRate) - milliseconds_t{ 1 }
    };
    auto timeToWaitDouble = (loopNormalizedDuration - loopElapsedTime);
    if (timeToWaitDouble < seconds_t::zero())
        timeToWaitDouble = seconds_t::zero();
    std::this_thread::sleep_for(timeToWaitDouble);
}
