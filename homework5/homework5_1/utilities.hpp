#pragma once
#include <chrono>
#include <cmath>

class Timer
{
    using clock_t        = std::chrono::steady_clock;
    using seconds_t      = std::chrono::duration<double, std::ratio<1>>;
    using milliseconds_t = std::chrono::duration<double, std::milli>;

private:
    std::chrono::time_point<clock_t> m_beg;

public:
    Timer();

    void reset();

    seconds_t elapsed() const;
};

void normalizeLoopDuration(
    std::chrono::duration<double, std::ratio<1>> loopElapsedTime);

template <typename T>
bool almostEqual(T p1, T p2)
{
    return std::abs(p1 - p2) < std::numeric_limits<T>::epsilon();
}
