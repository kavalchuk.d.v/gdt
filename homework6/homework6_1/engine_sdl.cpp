#include "engine_sdl.hpp"

#include <algorithm>
#include <array>
#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace om
{
const om::myGlfloat IEngine::minCoordinate{ -1.0 };
const om::myGlfloat IEngine::maxCoordinate{ 1.0 };

template <typename T>
static void getGlFunctionPointer(const char* func_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(
            std::string("Can't get pointer to OpenGL function ") + func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

//////////////////////////////////////////////////////////////////////
std::ostream& operator<<(std::ostream& stream, const EventType e)
{
    std::uint32_t value   = static_cast<std::uint32_t>(e);
    std::uint32_t minimal = static_cast<std::uint32_t>(EventType::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(EventType::turn_off);
    const auto    numberOfEvents = static_cast<size_t>(EventType::max_type);
    static std::array<std::string_view, numberOfEvents> event_names = {
        { /// input events
          "left_pressed", "left_released", "right_pressed", "right_released",
          "up_pressed", "up_released", "down_pressed", "down_released",
          "select_pressed", "select_released", "start_pressed",
          "start_released", "button1_pressed", "button1_released",
          "button2_pressed", "button2_released", "cursor_motion",
          "wheel_rolled",
          /// virtual console events
          "turn_off" }
    };

    if (value >= minimal && value <= maximal)
    {
        stream << event_names[value];
        return stream;
    }
    else
    {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream& operator>>(std::istream& in, Vertex& vertex)
{
    in >> vertex.x;
    in >> vertex.y;
    in >> vertex.z;
    in >> vertex.r;
    in >> vertex.g;
    in >> vertex.b;
    return in;
}

std::istream& operator>>(std::istream& in, Triangle& triangle)
{
    in >> triangle.v[0];
    in >> triangle.v[1];
    in >> triangle.v[2];
    return in;
}

//////////////////////////////////////////////////////////////////////
struct bind
{
    bind(SDL_Keycode k, std::string_view s, EventType pressed,
         EventType released)
        : key(k)
        , name(s)
        , event_pressed(pressed)
        , event_released(released)
    {
    }

    SDL_Keycode      key;
    std::string_view name;
    EventType        event_pressed;
    EventType        event_released;
};

enum class MouseButtons : Uint8
{
    button_left = 1,
    button_middle,
    button_right
};

struct bindMouse
{
    bindMouse(MouseButtons b, std::string_view s, EventType pressed,
              EventType released)
        : button(b)
        , name(s)
        , event_pressed(pressed)
        , event_released(released)
    {
    }

    MouseButtons     button;
    std::string_view name;
    EventType        event_pressed;
    EventType        event_released;
};

const std::array<bind, 6 /*8*/> keys{
    { { SDLK_w, "up", EventType::up_pressed, EventType::up_released },
      { SDLK_a, "left", EventType::left_pressed, EventType::left_released },
      { SDLK_s, "down", EventType::down_pressed, EventType::down_released },
      { SDLK_d, "right", EventType::right_pressed, EventType::right_released },
      //      { SDLK_LCTRL, "button1", event::button1_pressed,
      //        event::button1_released },
      //      { SDLK_SPACE, "button2", event::button2_pressed,
      //        event::button2_released },
      { SDLK_ESCAPE, "select", EventType::select_pressed,
        EventType::select_released },
      { SDLK_RETURN, "start", EventType::start_pressed,
        EventType::start_released } }
};

const std::array<bindMouse, 2> mouseButtons{
    { { MouseButtons::button_left, "button1", EventType::button1_pressed,
        EventType::button1_released },
      { MouseButtons::button_right, "button2", EventType::button2_pressed,
        EventType::button2_released } }
};

static bool check_input(const SDL_Event& e, const bind*& result)
{
    using namespace std;
    const auto& searchingCollection = keys;
    const auto  it =
        find_if(begin(searchingCollection), end(searchingCollection),
                [e](const bind& b) { return b.key == e.key.keysym.sym; });

    if (it != end(searchingCollection))
    {
        result = &(*it);
        return true;
    }
    return false;
}

static bool check_input(const SDL_Event& e, const bindMouse*& result)
{
    using namespace std;
    const auto& searchingCollection = mouseButtons;
    const auto  it =
        find_if(begin(searchingCollection), end(searchingCollection),
                [e](const bindMouse& b) {
                    return static_cast<Uint8>(b.button) == e.button.button;
                });

    if (it != end(searchingCollection))
    {
        result = &(*it);
        return true;
    }
    return false;
}

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar* message, [[maybe_unused]] const void* userParam);
static bool isGlResultOk();
/// create main window
/// on success return empty string
std::string EngineSdl::initialize(std::string_view /*config*/)
{
    std::stringstream serr{};
    auto              isInitSdl = initSdl(serr);
    if (!isInitSdl)
    {
        return serr.str();
    }

    auto isInitSdlWindow = initSdlWindow(serr);
    if (!isInitSdlWindow)
    {
        return serr.str();
    }

    auto isInitOpenGlContext = initOpenGl(serr);
    if (!isInitOpenGlContext)
    {
        return serr.str();
    }

    // Now load and ES and Core functions
    auto isLoadGLFunctionsPointers = loadGLFunctionsPointers(serr);
    if (!isLoadGLFunctionsPointers)
    {
        return serr.str();
    }

    // Debugging
    setDebugOpenGl();

    // enabling VAO. Renderdoc only can work with VAO enabled
    auto isInitVertexBufferAndVAO = initBuffersAndVAO(serr);
    if (!isInitVertexBufferAndVAO)
    {
        return serr.str();
    }

    std::string vertexShaderFileName{ "vertex_shader_1.vert" };
    GLuint      vertexShader;
    auto        vertexShaderInitResult =
        initShader(vertexShaderFileName, GL_VERTEX_SHADER, vertexShader, serr);

    std::string vertexShader2FileName{ "vertex_shader_2.vert" };
    GLuint      vertexShader2;
    auto        vertexShader2InitResult = initShader(
        vertexShader2FileName, GL_VERTEX_SHADER, vertexShader2, serr);

    GLuint      fragmentShader;
    std::string fragmentShaderFileName{ "fragment_shader_1.frag" };
    auto        fragmentShaderInitResult = initShader(
        fragmentShaderFileName, GL_FRAGMENT_SHADER, fragmentShader, serr);

    GLuint      fragmentShader2;
    std::string fragmentShader2FileName{ "fragment_shader_2.frag" };
    auto        fragmentShader2InitResult = initShader(
        fragmentShader2FileName, GL_FRAGMENT_SHADER, fragmentShader2, serr);

    if (!(vertexShaderInitResult && vertexShader2InitResult &&
          fragmentShaderInitResult && fragmentShader2InitResult))
    {
        return serr.str();
    }

    auto isInitProgram =
        initProgram(vertexShader, fragmentShader, m_programId, serr);

    auto isInitProgram2 =
        initProgram(vertexShader2, fragmentShader2, m_programId2, serr);

    if (!(isInitProgram && isInitProgram2))
    {
        return serr.str();
    }

    // turn on rendering with just created shader program
    glUseProgram(m_programId);
    isGlResultOk();

    // Enabling Z-buffer
    glEnable(GL_DEPTH_TEST);

    return serr.str();
}

bool EngineSdl::initSdl(std::stringstream& serr)
{
    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        std::cerr << "warning: SDL2 compiled and linked version mismatch: "
                  << compiled << " " << linked << std::endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_Init: " << err_message << std::endl;
        return false;
    }
    return true;
}

bool EngineSdl::initSdlWindow(std::stringstream& serr)
{
    SDL_Window* const window =
        SDL_CreateWindow("title", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_CreateWindow: " << err_message
             << std::endl;
        SDL_Quit();
        return false;
    }

    m_window = window;
    return true;
}

bool EngineSdl::initOpenGl(std::stringstream& serr)
{
    // set debug
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    // version and profile for openGL ES 3.2. This wont work because Core and ES
    // shaders arent compatible
    int gl_major_ver       = 3;
    int gl_minor_ver       = 2;
    int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

    // checking for supporting platform
    std::string_view platform = SDL_GetPlatform();
    using namespace std::string_view_literals;

    auto list = { "Linux"sv, "Windows"sv, "Mac OS X"sv };
    auto it   = std::find(std::begin(list), std::end(list), platform);
    if (it != std::end(list))
    {
        // We can use RenderDoc + text debug if can init OpenGl Core 4.3.
        // OpenGL ES cant work with RenderDoc so use CORE if possible
        gl_major_ver       = 4;
        gl_minor_ver       = 3;
        gl_context_profile = SDL_GL_CONTEXT_PROFILE_CORE;
    }

    // set version and profile for openGl. If current profile ES (Android, IOS)
    // - we cant use renderdoc
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

    // Try set profile finded profile
    SDL_GLContext gl_context = SDL_GL_CreateContext(m_window);

    // Vertical sync
    SDL_GL_SetSwapInterval(1);

    // try set core 3.3 if gl_context havent init yet
    if (gl_context == nullptr)
    {
        // try to create Core context with lower version. Only Renderdoc is
        // available if Core 3.3 Version
        gl_major_ver       = 3;
        gl_minor_ver       = 3;
        gl_context_profile = SDL_GL_CONTEXT_PROFILE_CORE;
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);
        gl_context = SDL_GL_CreateContext(m_window);
    }

    if (!gl_context)
    {
        serr << "Cannot create context. Maybe current platform is not "
                "supported. Critical error.";
        return false;
    }

    // check created version
    int gl_major_ver_get;
    int gl_minor_ver_get;
    int gl_context_ver_get;

    int resultMajor =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver_get);

    int resultMinor =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver_get);

    int resultProfile =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &gl_context_ver_get);

    if (resultMinor != 0 || resultMajor != 0 || resultProfile != 0)
    {
        serr << "Cannot get version profile. Critical error.";
        return false;
    }

    if (gl_context_ver_get != gl_context_profile)
    {
        serr << "current context profile: " << gl_context_ver_get
             << "demand context profile: " << gl_context_profile << std::endl
             << "(Core - 1, ES - 4, Compatibility - 2)" << std::flush;
        return false;
    }

    if ((gl_major_ver_get < gl_major_ver) || (gl_minor_ver_get < gl_minor_ver))
    {
        serr << "current context opengl version: " << gl_major_ver_get << '.'
             << gl_minor_ver_get << '\n'
             << "need opengl version at least: " << gl_major_ver << '.'
             << gl_minor_ver_get << '.' << std::flush;
        return false;
    }

    m_glContext = gl_context;

    return true;
}

bool EngineSdl::loadGLFunctionsPointers(std::stringstream& serr)
{
    // This code to simplify adding pointers
    //    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress))
    //    {
    //    }
    // get pointers to opengl functions
    try
    {
        getGlFunctionPointer("glGetError", glad_glGetError);
        getGlFunctionPointer("glCreateShader", glad_glCreateShader);
        getGlFunctionPointer("glShaderSource", glad_glShaderSource);
        getGlFunctionPointer("glCompileShader", glad_glCompileShader);
        getGlFunctionPointer("glGetShaderiv", glad_glGetShaderiv);
        getGlFunctionPointer("glGetShaderInfoLog", glad_glGetShaderInfoLog);
        getGlFunctionPointer("glDeleteShader", glad_glDeleteShader);
        getGlFunctionPointer("glCreateProgram", glad_glCreateProgram);
        getGlFunctionPointer("glAttachShader", glad_glAttachShader);
        getGlFunctionPointer("glBindAttribLocation", glad_glBindAttribLocation);
        getGlFunctionPointer("glLinkProgram", glad_glLinkProgram);
        getGlFunctionPointer("glGetProgramiv", glad_glGetProgramiv);
        getGlFunctionPointer("glGetProgramInfoLog", glad_glGetProgramInfoLog);
        getGlFunctionPointer("glDeleteProgram", glad_glDeleteProgram);
        getGlFunctionPointer("glUseProgram", glad_glUseProgram);
        getGlFunctionPointer("glVertexAttribPointer",
                             glad_glVertexAttribPointer);
        getGlFunctionPointer("glEnableVertexAttribArray",
                             glad_glEnableVertexAttribArray);
        getGlFunctionPointer("glDisableVertexAttribArray",
                             glad_glDisableVertexAttribArray);
        getGlFunctionPointer("glValidateProgram", glad_glValidateProgram);
        getGlFunctionPointer("glBindBuffer", glad_glBindBuffer); // for VAO

        getGlFunctionPointer("glGenBuffers", glad_glGenBuffers); // for VAO

        getGlFunctionPointer("glGenVertexArrays",
                             glad_glGenVertexArrays); // for VAO

        getGlFunctionPointer("glBindVertexArray",
                             glad_glBindVertexArray); // for VAO

        getGlFunctionPointer("glBufferData", glad_glBufferData); // for VAO

        getGlFunctionPointer("glDrawArrays", glad_glDrawArrays);
        getGlFunctionPointer("glDrawElements", glad_glDrawElements);
        getGlFunctionPointer("glClear", glad_glClear);
        getGlFunctionPointer("glClearColor", glad_glClearColor);
        getGlFunctionPointer("glGetUniformLocation", glad_glGetUniformLocation);
        getGlFunctionPointer("glUniform3fv", glad_glUniform3fv);
        getGlFunctionPointer("glUniform1f", glad_glUniform1f);

        getGlFunctionPointer("glLineWidth", glad_glLineWidth);

        getGlFunctionPointer("glLineWidth", glad_glLineWidth);

        getGlFunctionPointer("glEnable", glad_glEnable);

        getGlFunctionPointer("glHint", glad_glHint);

        getGlFunctionPointer("glBlendFunc", glad_glBlendFunc);
    }
    catch (std::runtime_error ex)
    {
        serr << "Critical exception: " << ex.what() << std::endl;
        return false;
    }
    // For debug
    try
    {
        getGlFunctionPointer("glDebugMessageCallback",
                             glad_glDebugMessageCallback);
        getGlFunctionPointer("glDebugMessageControl",
                             glad_glDebugMessageControl);
    }
    catch (std::runtime_error ex)
    {
        std::cerr << "Noncritical exception: " << ex.what() << std::endl
                  << "text debug is unable" << std::endl;
    }

    return true;
}

bool EngineSdl::initBuffersAndVAO(std::stringstream& serr)
{

    // init internal Vertex Buffer
    GLuint vertex_buffer = 0;
    glGenBuffers(1, &vertex_buffer);
    auto isGenVertexBuffer = isGlResultOk();
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    auto isBindVertexBuffer = isGlResultOk();

    // init VAO (vertex array object)
    GLuint vertex_array_object = 0;
    glGenVertexArrays(1, &vertex_array_object);
    auto isGenVertexArrays = isGlResultOk();
    glBindVertexArray(vertex_array_object);
    auto isBindVertexArrays = isGlResultOk();

    auto isInitIndexBuffer = initIndexBuffer(serr);

    if (!(isGenVertexBuffer && isBindVertexBuffer && isInitIndexBuffer &&
          isGenVertexArrays && isBindVertexArrays))
    {
        serr << "Cannot init buffers or VAO." << std::endl;
    }

    return true;
}

bool EngineSdl::initIndexBuffer(std::stringstream& serr)
{
    // init internal Vertex Buffer
    GLuint index_buffer = 0;
    glGenBuffers(1, &index_buffer);
    auto isGenBuffers = isGlResultOk();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    auto isBindBuffers = isGlResultOk();
    if (!(isGenBuffers && isBindBuffers))
    {
        serr << "Cannot create index buffer." << std::endl;
    }
    return true;
}

void EngineSdl::setDebugOpenGl()
{
    if (glEnable && glDebugMessageCallback && glDebugMessageControl)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(callback_opengl_debug, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                              nullptr, GL_TRUE);
    }
    else
    {
        std::cerr << "Warning. Debug functions pointers arent init.";
    }
}

bool EngineSdl::initShader(const std::string& shaderFileName, GLenum type,
                           GLuint& r_shader, std::stringstream& serr)
{
    r_shader = glCreateShader(type);
    isGlResultOk();
    std::ifstream shaderFile{ shaderFileName };
    if (!shaderFile.is_open())
    {
        serr << "Unable open file shader: " << shaderFileName << std::endl;
        return false;
    }
    std::string shaderSrc;
    std::getline(shaderFile, shaderSrc, static_cast<char>(shaderFile.eof()));

    const auto srcShaderSrcPtr{ shaderSrc.data() };
    glShaderSource(r_shader, 1, &srcShaderSrcPtr, nullptr);
    isGlResultOk();

    glCompileShader(r_shader);
    isGlResultOk();

    GLint compiled_status = 0;
    glGetShaderiv(r_shader, GL_COMPILE_STATUS, &compiled_status);
    isGlResultOk();

    if (compiled_status != GL_TRUE)
    {
        GLint info_len = 0;
        glGetShaderiv(r_shader, GL_INFO_LOG_LENGTH, &info_len);
        isGlResultOk();
        std::string info_chars(static_cast<size_t>(info_len), ' ');
        glGetShaderInfoLog(r_shader, info_len, nullptr, info_chars.data());
        isGlResultOk();
        glDeleteShader(r_shader);
        isGlResultOk();

        std::string shader_type_name =
            (type == GL_VERTEX_SHADER) ? "vertex" : "fragment";

        serr << "Error compiling shader(" << shader_type_name << ")\n"
             << shaderSrc << "\n"
             << info_chars.data();
        return false;
    }
    return true;
}

bool EngineSdl::initProgram(GLuint vertexShader, GLuint fragmentShader,
                            GLuint& programId, std::stringstream& serr)
{
    auto isCreateProgram = createProgramAndAttachShaders(
        vertexShader, fragmentShader, programId, serr);
    auto isBindAttributes =
        bindAttributesLocationAndLinkProgram(programId, serr);
    return (isCreateProgram && isBindAttributes);
}

bool EngineSdl::createProgramAndAttachShaders(GLuint             vertexShader,
                                              GLuint             fragmentShader,
                                              GLuint&            programId,
                                              std::stringstream& serr)
{
    programId = glCreateProgram();
    isGlResultOk();

    if (m_programId == 0)
    {
        serr << "failed to create gl program";
        return false;
    }

    glAttachShader(programId, vertexShader);
    isGlResultOk();
    glAttachShader(programId, fragmentShader);
    isGlResultOk();
    return true;
}

bool EngineSdl::bindAttributesLocationAndLinkProgram(const GLuint programId,
                                                     std::stringstream& serr)
{
    // commented for provide posibility to create any program through this
    // function

    //    // bind attribute a_position to 0 location in callstack for shader
    //    glBindAttribLocation(programID, m_position1AttributeNumber,
    //    "a_position"); isGlResultOk(); glBindAttribLocation(programID,
    //    m_colorAttributeNumber, "a_color"); isGlResultOk();
    // link program after binding attribute locations
    glLinkProgram(programId);
    isGlResultOk();
    // Check the link status
    GLint linked_status = 0;
    glGetProgramiv(programId, GL_LINK_STATUS, &linked_status);
    isGlResultOk();
    if (linked_status == 0)
    {
        GLint infoLen = 0;
        glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLen);
        isGlResultOk();
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(programId, infoLen, nullptr, infoLog.data());
        isGlResultOk();
        serr << "Error linking program:\n" << infoLog.data();
        glDeleteProgram(programId);
        isGlResultOk();
        return false;
    }
    return true;
}

/// pool event from input queue
/// return true if more events in queue
bool EngineSdl::read_input(Event& e) const
{
    using namespace std;
    // collect all events from SDL
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event))
    {
        if (sdl_event.type == SDL_QUIT)
        {
            e.type = EventType::turn_off;
            return true;
        }
        else if ((sdl_event.type == SDL_KEYDOWN) && (sdl_event.key.repeat == 0))
        {
            const bind* binding = nullptr;
            if (check_input(sdl_event, binding))
            {
                e.type = binding->event_pressed;
                return true;
            }
        }
        else if (sdl_event.type == SDL_KEYUP)
        {
            const bind* binding = nullptr;
            if (check_input(sdl_event, binding))
            {
                e.type = binding->event_released;
                return true;
            }
        }
        // for mouse text mode is not used so we can not check repeat
        else if (sdl_event.type == SDL_MOUSEBUTTONDOWN)
        {
            const bindMouse* binding = nullptr;
            if (check_input(sdl_event, binding))
            {
                e.type = binding->event_pressed;
                return true;
            }
        }
        else if (sdl_event.type == SDL_MOUSEBUTTONUP)
        {
            const bindMouse* binding = nullptr;
            if (check_input(sdl_event, binding))
            {
                e.type = binding->event_released;
                return true;
            }
        }
        else if (sdl_event.type == SDL_MOUSEMOTION)
        {
            int w = 0, h = 0;
            SDL_GetWindowSize(m_window, &w, &h);

            // SDL to OpenGL coordinates
            auto mouse_x =
                static_cast<myGlfloat>(sdl_event.motion.x / (w / 2.0) - 1);
            ;
            auto mouse_y =
                -static_cast<myGlfloat>(sdl_event.motion.y / (h / 2.0) - 1);

            e.type          = EventType::cursor_motion;
            e.parameters.p0 = mouse_x;
            e.parameters.p1 = mouse_y;
            return true;
        }
        else if (sdl_event.type == SDL_MOUSEWHEEL)
        {
            myGlfloat scaleCoef = 100.0;
            // TODO finish implementation
            auto wheel_shift_x =
                static_cast<myGlfloat>(sdl_event.wheel.x / scaleCoef);
            auto wheel_shift_y =
                static_cast<myGlfloat>(sdl_event.wheel.y / scaleCoef);
            e.type          = EventType::wheel_rolled;
            e.parameters.p0 = wheel_shift_x;
            e.parameters.p1 = wheel_shift_y;
            return true;
        }
    }
    return false;
}

void EngineSdl::uninitialize()
{
    SDL_GL_DeleteContext(m_glContext);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

bool EngineSdl::setUniform(std::string_view uniformName,
                           om::myGlfloat    parameters)
{
    GLuint programId = m_programId2;
    glUseProgram(programId);
    const int location = glGetUniformLocation(programId, uniformName.data());
    isGlResultOk();
    if (location < 0)
    {
        std::cerr << "Error. Can't get uniform location from shader\n";
        return false;
        // throw std::runtime_error("can't get uniform location");
    }

    glUniform1f(location, parameters);

    isGlResultOk();
    return true;
}

bool EngineSdl::setUniform(std::string_view uniformName,
                           mouseUniform     parameters)
{
    GLuint programId = m_programId;
    glUseProgram(programId);
    const int location = glGetUniformLocation(programId, uniformName.data());
    isGlResultOk();
    if (location < 0)
    {
        std::cerr << "Error. Can't get uniform location from shader\n";
        return false;
        // throw std::runtime_error("can't get uniform location");
    }
    glUniform3fv(location, 1, parameters.data());
    isGlResultOk();
    return true;
}

bool EngineSdl::renderClearWindow()
{
    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    auto clearColorResult = isGlResultOk();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    auto clearResult = isGlResultOk();
    return clearColorResult && clearResult;
}

void EngineSdl::render(const std::vector<VertexMorphed>& vertices,
                       const std::vector<myUint>& indices, ShapeType type)
{
    glUseProgram(m_programId2);
    isGlResultOk();

    const auto vertexNumber    = static_cast<int>(type);
    const auto numberOfIndeces = indices.size();

    if ((numberOfIndeces % vertexNumber) != 0)
    {
        std::cerr
            << "Error draw triangles. Number of indices should multiply 3."
            << std::endl;
        return;
    }

    // Fill internal OpenGL Vertices buffer with triangle
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexMorphed),
                 vertices.data(), GL_STATIC_DRAW);
    isGlResultOk();

    // Fill internal OpenGL indices buffer with triangle
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numberOfIndeces * sizeof(myUint),
                 indices.data(), GL_STATIC_DRAW);

    isGlResultOk();

    GLintptr    position1ByteOffset      = 0;
    GLintptr    position2ByteOffset      = sizeof(myGlfloat) * 6;
    GLintptr    colorByteOffset          = sizeof(myGlfloat) * 3;
    const GLint position1AttributeAmount = 3;
    const GLint position2AttributeAmount = 3;
    const GLint colorAttributeAmount     = 3;

    // Enabling vertexAttrib array 0
    glEnableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();

    glVertexAttribPointer(m_position1AttributeNumber, position1AttributeAmount,
                          GL_FLOAT, GL_FALSE, sizeof(VertexMorphed),
                          reinterpret_cast<const void*>(position1ByteOffset));
    isGlResultOk();

    // Enabling vertexAttrib array 1
    glEnableVertexAttribArray(m_colorAttributeNumber);
    isGlResultOk();

    // byte shift in struct Vertex to reach color fields

    glVertexAttribPointer(m_colorAttributeNumber, colorAttributeAmount,
                          GL_FLOAT, GL_FALSE, sizeof(VertexMorphed),
                          reinterpret_cast<const void*>(colorByteOffset));
    isGlResultOk();

    // Enabling vertexAttrib array 2
    glEnableVertexAttribArray(m_position2AttributeNumber);
    isGlResultOk();

    glVertexAttribPointer(m_position2AttributeNumber, position2AttributeAmount,
                          GL_FLOAT, GL_FALSE, sizeof(VertexMorphed),

                          reinterpret_cast<const void*>(position2ByteOffset));
    isGlResultOk();

    glValidateProgram(m_programId);
    isGlResultOk();
    // Check the validate status
    GLint validateStatus = 0;
    glGetProgramiv(m_programId, GL_VALIDATE_STATUS, &validateStatus);
    isGlResultOk();
    if (validateStatus != GL_TRUE)
    {
        GLint infoLen = 0;
        glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &infoLen);
        isGlResultOk();
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(m_programId, infoLen, nullptr, infoLog.data());
        isGlResultOk();
        std::cerr << "Error draw triangle program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }

    GLenum shapeGlType = 0;
    switch (type)
    {
        case ShapeType::line:
            shapeGlType = GL_LINES;
            glLineWidth(2);
            break;
        case ShapeType::triangle:
            shapeGlType = GL_TRIANGLES;
            glLineWidth(1);
            break;
        case ShapeType::triangle_strip:
            shapeGlType = GL_TRIANGLE_STRIP;
            glLineWidth(1);
            break;
    }
    const GLsizei indicesDataType = GL_UNSIGNED_INT;
    glDrawElements(shapeGlType, numberOfIndeces, indicesDataType, nullptr);
    isGlResultOk();
    glDisableVertexAttribArray(m_position2AttributeNumber);
    isGlResultOk();
    glDisableVertexAttribArray(m_colorAttributeNumber);
    isGlResultOk();
    glDisableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();
}

void EngineSdl::render(const std::vector<Vertex>& vertices,
                       const std::vector<myUint>& indices, ShapeType type)
{
    glUseProgram(m_programId);
    isGlResultOk();
    // Only for Core profile setting to draw only skeletons of polygones
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // That unefficient because we should save our all our vertexes once and
    // after only use indeces buffer

    const auto vertexNumber    = static_cast<int>(type);
    const auto numberOfIndeces = indices.size();
    if ((numberOfIndeces % vertexNumber) != 0)
    {
        std::cerr
            << "Error draw triangles. Number of indices should multiply 3."
            << std::endl;
    }

    // Fill internal OpenGL Vertices buffer with triangle
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex),
                 vertices.data(), GL_STATIC_DRAW);
    isGlResultOk();

    // Fill internal OpenGL indices buffer with triangle
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numberOfIndeces * sizeof(myUint),
                 indices.data(), GL_STATIC_DRAW);

    isGlResultOk();

    // Choosing vertexAttrib array
    glEnableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();

    GLintptr    positionByteOffset      = 0;
    GLintptr    colorByteOffset         = sizeof(myGlfloat) * 3;
    const GLint positionAttributeAmount = 3;
    const GLint colorAttributeAmount    = 3;

    glVertexAttribPointer(m_position1AttributeNumber, positionAttributeAmount,
                          GL_FLOAT, GL_FALSE, sizeof(Vertex),

                          reinterpret_cast<const void*>(positionByteOffset));
    isGlResultOk();

    glEnableVertexAttribArray(m_colorAttributeNumber);
    isGlResultOk();

    // byte shift in struct Vertex to reach color fields

    glVertexAttribPointer(m_colorAttributeNumber, colorAttributeAmount,
                          GL_FLOAT, GL_FALSE, sizeof(Vertex),

                          reinterpret_cast<const void*>(colorByteOffset));
    isGlResultOk();

    glValidateProgram(m_programId);
    isGlResultOk();
    // Check the validate status
    GLint validateStatus = 0;
    glGetProgramiv(m_programId, GL_VALIDATE_STATUS, &validateStatus);
    isGlResultOk();
    if (validateStatus != GL_TRUE)
    {
        GLint infoLen = 0;
        glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &infoLen);
        isGlResultOk();
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(m_programId, infoLen, nullptr, infoLog.data());
        isGlResultOk();
        std::cerr << "Error draw triangle program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }

    GLenum shapeGlType = 0;
    switch (type)
    {
        case ShapeType::line:
            shapeGlType = GL_LINES;
            glLineWidth(2);
            break;
        case ShapeType::triangle:
            shapeGlType = GL_TRIANGLES;
            glLineWidth(1);
            break;
        case ShapeType::triangle_strip:
            shapeGlType = GL_TRIANGLE_STRIP;
            glLineWidth(1);
            break;
    }
    const GLsizei indicesDataType = GL_UNSIGNED_INT;
    glDrawElements(shapeGlType, numberOfIndeces, indicesDataType, nullptr);
    isGlResultOk();
    glDisableVertexAttribArray(m_colorAttributeNumber);
    isGlResultOk();
    glDisableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();
}

void EngineSdl::renderTriangle(const Triangle& t)
{
    glUseProgram(m_programId);
    isGlResultOk();
    // Only for Core profile setting to draw only skeletons of polygones
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // That`s unefficient because we should save our all our vertexes once and
    // after only use indeces buffer

    // Fill internal OpenGL Vertex buffer with triangle
    glBufferData(GL_ARRAY_BUFFER, sizeof(t), &t, GL_STATIC_DRAW);
    isGlResultOk();

    // Choosing vertexAttrib array - paramete N0
    glEnableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();

    GLintptr    positionByteOffset      = 0;
    GLintptr    colorByteOffset         = sizeof(myGlfloat) * 3;
    const GLint positionAttributeAmount = 3;
    const GLint colorAttributeAmount    = 3;

    glVertexAttribPointer(
        /*index in attributes*/ m_position1AttributeNumber,
        /*number of used Vertex fields*/ positionAttributeAmount,
        /*type of every vertex field*/ GL_FLOAT,
        /*normalized -smth about fixed point parameters*/ GL_FALSE,
        /*byte offset between every Vertex in array*/ sizeof(Vertex),
        /*offset relate to begin of Vertex to reach Position fields*/
        reinterpret_cast<const void*>(positionByteOffset));
    isGlResultOk();

    glEnableVertexAttribArray(m_colorAttributeNumber);
    isGlResultOk();

    // byte shift in struct Vertex to reach color fields

    glVertexAttribPointer(
        /*index in attributes*/ m_colorAttributeNumber,
        /*number of used Vertex fields*/ colorAttributeAmount,
        /*type of every vertex field*/ GL_FLOAT,
        /*normalized -smth about fixed point parameters*/ GL_FALSE,
        /*byte offset between every Vertex in array*/ sizeof(Vertex),
        /*offset relate to begin of Vertex to reach Color fields*/
        reinterpret_cast<const void*>(colorByteOffset));
    isGlResultOk();

    // code below is example without using VAO

    //        glVertexAttribPointer(
    //            /*index in attributes*/ 0,
    //            /*number of used Vertex fields*/ 3,
    //            /*type of every vertex field*/ GL_FLOAT,
    //            /*normalized -smth about fixed point parameters*/
    //            GL_FALSE,
    //            /*byte offset between every Vertex in array*/
    //            sizeof(Vertex),
    //            /*offset relate to begin of Vertex - without using VAO -
    //            pointer to
    //               Vertex array*/
    //            &t.v[0]);

    //        glVertexAttribPointer(
    //            /*index in attributes*/ 1,
    //            /*number of used Vertex fields*/ 3,
    //            /*type of every vertex field*/ GL_FLOAT,
    //            /*normalized -smth about fixed point parameters*/
    //            GL_FALSE,
    //            /*byte offset between every Vertex in array*/
    //            sizeof(Vertex),
    //            /*offset relate to begin of Vertex - without using VAO -
    //            pointer to
    //               Vertex array*/
    //            reinterpret_cast<const void*>(&(t.v[0].x) + 3));

    glValidateProgram(m_programId);
    isGlResultOk();
    // Check the validate status
    GLint validateStatus = 0;
    glGetProgramiv(m_programId, GL_VALIDATE_STATUS, &validateStatus);
    isGlResultOk();
    if (validateStatus != GL_TRUE)
    {
        GLint infoLen = 0;
        glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &infoLen);
        isGlResultOk();
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(m_programId, infoLen, nullptr, infoLog.data());
        isGlResultOk();
        std::cerr << "Error draw triangle program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    isGlResultOk();
    glDisableVertexAttribArray(m_position1AttributeNumber);
    isGlResultOk();
}

bool EngineSdl::updateWindow()
{
    SDL_GL_SwapWindow(m_window);
    renderClearWindow();
    return isGlResultOk();
}

static bool isGlResultOk()
{
    const int err = static_cast<int>(glGetError());
    if (err != GL_NO_ERROR)
    {
        switch (err)
        {
            case GL_INVALID_ENUM:
                std::cerr << "GL_INVALID_ENUM" << std::endl;
                break;
            case GL_INVALID_VALUE:
                std::cerr << "GL_INVALID_VALUE" << std::endl;
                break;
            case GL_INVALID_OPERATION:
                std::cerr << "GL_INVALID_OPERATION" << std::endl;
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;
                break;
            case GL_OUT_OF_MEMORY:
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;
                break;
        }
        return false;
    }
    return true;
}

// enum GL_DEBUG printing
static const char* source_to_strv(GLenum source)
{
    switch (source)
    {
        case GL_DEBUG_SOURCE_API:
            return "API";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            return "SHADER_COMPILER";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            return "WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            return "THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:
            return "APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:
            return "OTHER";
    }
    return "unknown";
}
// enum GL_DEBUG printing
static const char* type_to_strv(GLenum type)
{
    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:
            return "ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            return "DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            return "UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PERFORMANCE:
            return "PERFORMANCE";
        case GL_DEBUG_TYPE_PORTABILITY:
            return "PORTABILITY";
        case GL_DEBUG_TYPE_MARKER:
            return "MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:
            return "PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:
            return "POP_GROUP";
        case GL_DEBUG_TYPE_OTHER:
            return "OTHER";
    }
    return "unknown";
}
// enum GL_DEBUG printing
static const char* severity_to_strv(GLenum severity)
{
    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:
            return "HIGH";
        case GL_DEBUG_SEVERITY_MEDIUM:
            return "MEDIUM";
        case GL_DEBUG_SEVERITY_LOW:
            return "LOW";
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            return "NOTIFICATION";
    }
    return "unknown";
}

// 30Kb on my system, too much for stack
static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

// When error openGl call this funtion and we get detailed messages
static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar* message, [[maybe_unused]] const void* userParam)
{
    // The memory formessageis owned and managed by the GL, and should
    // onlybe considered valid for the duration of the function call.The
    // behavior of calling any GL or window system function from within
    // thecallback function is undefined and may lead to program
    // termination.Care must also be taken in securing debug callbacks for
    // use with asynchronousdebug output by multi-threaded GL
    // implementations.  Section 18.8 describes thisin further detail.

    auto& buff{ local_log_buff };
    int   num_chars = std::snprintf(
        buff.data(), buff.size(), "%s %s %d %s %.*s\n", source_to_strv(source),
        type_to_strv(type), id, severity_to_strv(severity), length, message);

    if (num_chars > 0)
    {
        // TODO use https://en.cppreference.com/w/cpp/io/basic_osyncstream
        // to fix possible data races
        // now we use GL_DEBUG_OUTPUT_SYNCHRONOUS to garantie call in main
        // thread
        std::cerr.write(buff.data(), num_chars);
    }
}

} // namespace om
