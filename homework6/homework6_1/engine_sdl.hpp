#pragma once
#include "iengine.hpp"
#include <SDL.h>
#include <glad.h>
#include <iosfwd>
#include <string>
#include <string_view>

namespace om
{

class EngineSdl : public IEngine
{
public:
    std::string initialize(std::string_view /*config*/) override final;
    bool        read_input(Event& e) const override final;
    void        uninitialize() override final;
    ~EngineSdl() override final {}

    bool setUniform(std::string_view uniformName,
                    mouseUniform     parameters) override;

    bool setUniform(std::string_view uniformName,
                    om::myGlfloat    parameters) override;

    bool renderClearWindow() override;

    void render(const std::vector<VertexMorphed>& vertices,
                const std::vector<myUint>&        indices,
                ShapeType type = ShapeType::triangle) override;

    void render(const std::vector<Vertex>& vertices,
                const std::vector<myUint>& indices,
                ShapeType                  type = ShapeType::triangle) override;
    void renderTriangle(const Triangle& t) override;
    bool updateWindow() override;

private:
    bool initSdl(std::stringstream& serr);
    bool initSdlWindow(std::stringstream& serr);
    bool initOpenGl(std::stringstream& serr);

    bool loadGLFunctionsPointers(std::stringstream& serr);

    bool initBuffersAndVAO(std::stringstream& serr);
    bool initIndexBuffer(std::stringstream& serr);

    void setDebugOpenGl();

    bool initShader(const std::string& shaderFileName, GLuint type,
                    GLuint& r_shader, std::stringstream& serr);
    bool initProgram(GLuint vertexShader, GLuint fragmentShader,
                     GLuint& programId, std::stringstream& serr);
    bool createProgramAndAttachShaders(GLuint vertexShader,
                                       GLuint fragmentShader, GLuint& programId,
                                       std::stringstream& serr);
    bool bindAttributesLocationAndLinkProgram(const GLuint       programID,
                                              std::stringstream& serr);

    SDL_Window*   m_window     = nullptr;
    SDL_GLContext m_glContext  = nullptr;
    GLuint        m_programId  = 0;
    GLuint        m_programId2 = 0;

    const GLuint m_position1AttributeNumber = 0;
    const GLuint m_position2AttributeNumber = 2;
    const GLuint m_colorAttributeNumber     = 1;
};

} // end namespace om
