#version 330 core
// mouse x, y and current radius
uniform vec3 u_mouse;

in vec4 v_position;
in vec3 v_color;

out vec4 FragColor;

void main()
{
     float dx2 = (v_position.x - u_mouse.x)*(v_position.x - u_mouse.x);
     float dy2 = (v_position.y - u_mouse.y)*(v_position.y - u_mouse.y);
     float distance = sqrt(dx2 + dy2);
     float r = u_mouse.z;
     if (distance > r){
         FragColor = vec4(v_color, 1.0);
     }else{
         float relativeDistance = distance / r;
         float relativePower = 1 - relativeDistance;
         vec3 chandedColor = vec3(v_color.r, v_color.g * relativeDistance, v_color.b * relativeDistance);
         float newRed = chandedColor.r + relativePower;
         if (newRed > 1){
             newRed = 1;
         }
         chandedColor.r = newRed;
         FragColor = vec4(chandedColor, 1.0);
     }
}

/// For OpenGL ES 3.2
//precision mediump float;
//varying vec4 v_position;
//varying vec3 v_color;

//void main()
//{
//    gl_FragColor = vec4(v_color, 1.0);
////    if (v_position.z >= 0.0)
////    {
////        float light_green = 0.5 + v_position.z / 2.0;
////        gl_FragColor = vec4(0.0, light_green, 0.0, 1.0);
////    } else
////    {
////        float color = 0.5 - (v_position.z / -2.0);
////        gl_FragColor = vec4(color, 0.0, 0.0, 1.0);
////    }
//}
