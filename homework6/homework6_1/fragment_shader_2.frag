#version 330 core
// mouse x, y and current radius

in vec4 v_position;
in vec3 v_color;

out vec4 FragColor;

void main()
{
     FragColor = vec4(v_color, 1.0);
}

/// For OpenGL ES 3.2
//precision mediump float;
//varying vec4 v_position;
//varying vec3 v_color;

//void main()
//{
//    gl_FragColor = vec4(v_color, 1.0);
////    if (v_position.z >= 0.0)
////    {
////        float light_green = 0.5 + v_position.z / 2.0;
////        gl_FragColor = vec4(0.0, light_green, 0.0, 1.0);
////    } else
////    {
////        float color = 0.5 - (v_position.z / -2.0);
////        gl_FragColor = vec4(color, 0.0, 0.0, 1.0);
////    }
//}
