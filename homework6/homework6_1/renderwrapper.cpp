#include "renderwrapper.hpp"
#include <iostream>

const om::myGlfloat RenderWrapper::minStep{ 0.005 };
const om::myGlfloat RenderWrapper::gridZcoord{ 0.0 };

RenderWrapper::RenderWrapper(std::array<om::myGlfloat, 3> color,
                             om::myGlfloat                gridStep)
{
    if (!initGridBuffers(color, gridStep))
    {
        std::cerr << "Cannot create RenderWrapper(). Color or gridStep are "
                     "incorrrect."
                  << std::endl;
        throw std::runtime_error(
            "Cannot create RenderWrapper(). Color or gridStep are "
            "incorrrect.");
    }

    m_color    = color;
    m_gridStep = gridStep;
}

bool RenderWrapper::initGridBuffers(std::array<om::myGlfloat, 3> color,
                                    om::myGlfloat                step)
{
    if (!(checkColor(color) && checkStep(step)))
    {
        return false;
    }

    const auto numberOfRow =
        static_cast<size_t>(
            (om::IEngine::maxCoordinate - om::IEngine::minCoordinate) / step) +
        1;

    const auto numberOfVertecesInRow = numberOfRow;

    m_vertexBuffer = formRectangleGridVertexBuffer(color, step, numberOfRow,
                                                   numberOfVertecesInRow);
    m_indexBuffer =
        formRectangleGridIndexBuffer(numberOfRow, numberOfVertecesInRow);
    return true;
}

bool RenderWrapper::checkColor(std::array<om::myGlfloat, 3> color)
{
    for (const auto colorPart : color)
    {
        if (colorPart > 1.f && colorPart < 0.f)
        {
            return false;
        }
    }
    return true;
}

bool RenderWrapper::checkStep(om::myGlfloat step)
{
    if (step < minStep && step > 1)
    {
        return false;
    }
    return true;
}

void RenderWrapper::render(om::IEngine& engine, const World& world)
{
    renderRectangleGrid(engine);

    renderWorld(engine, world);
}

static std::vector<om::VertexMorphed> formShipVertexBuffer(
    const om::myGlfloat shipX, const om::myGlfloat shipY,
    const om::myGlfloat shipZ)
{
    const om::myGlfloat            sizeX{ 0.2 };
    const om::myGlfloat            sizeY{ 0.3 };
    std::vector<om::VertexMorphed> vertexes;
    vertexes.reserve(11);

    vertexes.push_back({ shipX, shipY + sizeY, shipZ, 0.3, 0.3, 0.3 }); // 0

    vertexes.push_back({ vertexes[0].x1 - sizeX / 9.0f,
                         vertexes[0].y1 - sizeY / 4, shipZ, 0.3, 0.3,
                         0.3 }); // 1

    vertexes.push_back({ vertexes[0].x1 + sizeX / 9.0f, vertexes[1].y1, shipZ,
                         0.3, 0.3, 0.3 }); // 2

    vertexes.push_back({ vertexes[1].x1, vertexes[1].y1 - sizeY * 3 / 8.0f,
                         shipZ, 0, 0, 0 }); // 3

    vertexes.push_back({ vertexes[2].x1, vertexes[1].y1 - sizeY * 3 / 8.0f,
                         shipZ, 0, 0, 0 }); // 4

    vertexes.push_back(
        { vertexes[3].x1, vertexes[3].y1 + sizeY / 8, shipZ, 0, 0, 0 }); // 5

    vertexes.push_back({ vertexes[3].x1 + sizeX / 6, vertexes[3].y1, shipZ,
                         vertexes[3].x1 - sizeX / 6, vertexes[3].y1, shipZ, 0,
                         0, 0 }); // 6

    vertexes.push_back(
        { vertexes[4].x1, vertexes[4].y1 + sizeY / 8.0f, shipZ, 0, 0, 0 }); // 7

    vertexes.push_back({ vertexes[4].x1 - sizeX / 6, vertexes[4].y1, shipZ,
                         vertexes[4].x1 + sizeX / 6, vertexes[4].y1, shipZ, 0,
                         0, 0 }); // 8

    vertexes.push_back({ vertexes[6].x1, vertexes[6].y1 - 3 * sizeY / 8.0f,
                         shipZ, vertexes[6].x2,
                         vertexes[6].y1 - 3 * sizeY / 8.0f, shipZ, 0, 0,
                         0 }); // 9

    vertexes.push_back({ vertexes[8].x1, vertexes[8].y1 - 3 * sizeY / 8.0f,
                         shipZ, vertexes[8].x2,
                         vertexes[8].y1 - 3 * sizeY / 8.0f, shipZ, 0, 0,
                         0 }); // 10

    return vertexes;
}

static std::vector<om::myUint> formShipIndexBuffer()
{
    return { 0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 5, 6, 4, 7, 8, 3, 6, 9, 4, 8, 10 };
}

void RenderWrapper::renderWorld(om::IEngine& engine, const World& world)
{
    const om::myGlfloat worldShipZ =
        ((gridZcoord - 0.1) >= -1.0) ? gridZcoord - 0.1 : gridZcoord;

    const auto sizeCoord =
        om::IEngine::maxCoordinate - om::IEngine::minCoordinate;
    const auto relatedXShip = world.x * sizeCoord / world.sizeX - 1;
    const auto relatedYShip = world.y * sizeCoord / world.sizeY - 1;
    const auto vertexes =
        formShipVertexBuffer(relatedXShip, relatedYShip, worldShipZ);
    const auto indexes = formShipIndexBuffer();
    auto       koef    = static_cast<om::myGlfloat>(world.y / world.sizeY);
    if (koef > 1.0)
    {
        koef = static_cast<om::myGlfloat>(1.0);
    }
    engine.setUniform("u_koef", koef);
    engine.render(vertexes, indexes, om::ShapeType::triangle);
}

void RenderWrapper::renderRectangleGrid(om::IEngine& engine)
{
    engine.render(m_vertexBuffer, m_indexBuffer, om::ShapeType::line);
}

std::vector<om::Vertex> RenderWrapper::formRectangleGridVertexBuffer(
    std::array<om::myGlfloat, 3> color, om::myGlfloat step,
    const size_t numberOfRow, const size_t numberOfVertecesInRow)
{
    std::vector<om::Vertex> vertexBuffer;

    const auto vertexNumber =
        static_cast<size_t>(numberOfRow * numberOfVertecesInRow);

    vertexBuffer.reserve(vertexNumber);
    for (size_t currentRow = 0; currentRow < numberOfRow; ++currentRow)
    {
        for (size_t currentVeretexInRow = 0;
             currentVeretexInRow < numberOfVertecesInRow; ++currentVeretexInRow)
        {
            const auto currentPosX =
                om::IEngine::minCoordinate + currentRow * step;
            const auto currentPosY =
                om::IEngine::minCoordinate + currentVeretexInRow * step;
            vertexBuffer.push_back({ currentPosX, currentPosY, gridZcoord,
                                     color[0], color[1], color[2] });
        }
    }
    return vertexBuffer;
}

std::vector<om::myUint> RenderWrapper::formRectangleGridIndexBuffer(
    om::myUint numberRowOfVertexes, om::myUint numberVertexesInRow)
{
    const size_t numberOfIndices =
        numberRowOfVertexes * (numberVertexesInRow - 1) * 4;

    std::vector<om::myUint> indexBuffer{};

    indexBuffer.reserve(numberOfIndices);
    // Horyzontal lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes; ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        for (om::myUint positionInRow = 0;
             positionInRow < numberVertexesInRow - 1; ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = thisIndex + 1;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    // Vertical lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes - 1;
         ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        const auto beginOfNextRow = (rowNumber + 1) * numberVertexesInRow;
        for (om::myUint positionInRow = 0; positionInRow < numberVertexesInRow;
             ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = beginOfNextRow + positionInRow;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    return indexBuffer;
}
