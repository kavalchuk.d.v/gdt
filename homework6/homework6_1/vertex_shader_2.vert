#version 330 core
// Location is set in bind method
// for having possibility to change order
// of attributes without changing shader
// we comment strings layout (location = n)

// mouse x, y and current radius
uniform float u_koef;

layout (location = 0) in vec3 a_position1;
layout (location = 1) in vec3 a_color;
layout (location = 2) in vec3 a_position2;

out vec4 v_position;
out vec3 v_color;

void main()
{
    v_position = vec4(a_position1 * u_koef + a_position2 * (1.0f - u_koef), 1.0);
    v_color = a_color;
    gl_Position = v_position;
}
