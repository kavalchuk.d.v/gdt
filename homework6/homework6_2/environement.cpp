#include "environement.hpp"
#include <iostream>
#include <memory>

static bool isContiniousCommand(World::Events worldEvent)
{
    return (worldEvent == World::Events::userCommandShipUp ||
            worldEvent == World::Events::userCommandShipDown);
}

const om::myGlfloat Environement::initialRadius{ 0.05 };

bool Environement::input(const om::IEngine&         engine,
                         const World::WorldEvents*& retWorldEvents,
                         eventSet&                  environementEvents)
{
    static auto worldEvents{ std::make_unique<World::WorldEvents>(
        static_cast<size_t>(World::Events::maxType)) };

    retWorldEvents = worldEvents.get();

    for (auto it = worldEvents->begin(); it != worldEvents->end();)
    {
        if (!isContiniousCommand(it->first))
            it = worldEvents->erase(it);
        else
            ++it;
    }
    om::Event event;
    while (engine.read_input(event))
    {
        std::cout << event.type << std::endl;
        switch (event.type)
        {
            case om::EventType::turn_off:
                return false;
            case om::EventType::up_pressed:
                worldEvents->insert({ World::Events::userCommandShipUp, {} });
                break;
            case om::EventType::up_released:
                worldEvents->erase(World::Events::userCommandShipUp);
                break;
            case om::EventType::down_pressed:
                worldEvents->insert({ World::Events::userCommandShipDown, {} });
                break;
            case om::EventType::down_released:
                worldEvents->erase(World::Events::userCommandShipDown);
                break;
            case om::EventType::button2_pressed:
            {
                const auto windowEngineSize =
                    om::IEngine::maxCoordinate - om::IEngine::minCoordinate;

                const double xWorld =
                    (mouseParams[0] + 1) / windowEngineSize * World::sizeX;

                const double yWorld =
                    (mouseParams[1] + 1) / windowEngineSize * World::sizeY;
                worldEvents->insert(
                    { World::Events::userCommandShipTeleporation,
                      { xWorld, yWorld } });

                std::cerr << "Oh, it`s imposible, man, you`ve just committed "
                             "a teleportation.\n";
                break;
            }

            case om::EventType::cursor_motion:
                environementEvents.insert(event);
                break;
            case om::EventType::wheel_rolled:
                environementEvents.insert(event);
                break;
            case om::EventType::button1_pressed:
                environementEvents.insert(event);
                break;
            default:
                break;
        }
    }
    return true;
}

bool Environement::handleEnvironementEvents(om::IEngine& engine,
                                            eventSet     envEvents)
{
    for (const auto& event : envEvents)
    {
        switch (event.type)
        {
            case om::EventType::cursor_motion:
                mouseParams[0] = event.parameters.p0;
                mouseParams[1] = event.parameters.p1;
                std::cerr << mouseParams[0] << " " << mouseParams[1] << "\n";
                break;
            case om::EventType::wheel_rolled:
                mouseParams[2] *= (1 + event.parameters.p1);
                mouseParams[2] += event.parameters.p1;
                std::cerr << " " << mouseParams[2] << "\n";
                break;
            case om::EventType::button1_pressed:
                mouseParams[2] = initialRadius;
                std::cerr << " " << mouseParams[2] << "\n";
                break;
            default:
                break;
        }
    }
    return true;
}
