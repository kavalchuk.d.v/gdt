#version 330 core
// mouse x, y and current radius

in vec4 v_position;
in vec3 v_color;
in vec2 v_tex_position;

uniform sampler2D s_texture;

out vec4 FragColor;

void main()
{
     gl_FragColor = texture2D(s_texture, v_tex_position);
}
