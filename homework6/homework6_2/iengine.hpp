#pragma once
#include <array>
#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

namespace om
{

using myGlfloat = float;
using myUint    = unsigned int;

using mouseUniform = std::array<myGlfloat, 3>;

enum class ShapeType
{
    triangle_strip = 1,
    line           = 2,
    triangle       = 3,

};

/// dendy gamepad emulation events
enum class OM_DECLSPEC EventType : size_t
{
    /// input events
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button1_pressed, // mouse left
    button1_released,
    button2_pressed, // mouse rigth
    button2_released,

    cursor_motion,
    wheel_rolled,
    /// virtual console events
    turn_off,
    max_type,
};

struct OM_DECLSPEC EventParam
{
    myGlfloat p0{};
    myGlfloat p1{};
    myGlfloat p2{};
};

struct OM_DECLSPEC Event
{
    EventType   type{ EventType::max_type };
    EventParam  parameters{};
    friend bool operator==(const Event& e1, const Event& e2)
    {
        return e1.type == e2.type;
    }
};

OM_DECLSPEC std::ostream& operator<<(std::ostream& stream, const EventType e);

struct OM_DECLSPEC Vertex
{
    myGlfloat x{ 0.0 };
    myGlfloat y{ 0.0 };
    myGlfloat z{ 0.0 };

    myGlfloat r{ 0.0 };
    myGlfloat g{ 0.0 };
    myGlfloat b{ 0.0 };
};

#pragma pack(push, 1)
struct OM_DECLSPEC VertexTextured
{
    VertexTextured(myGlfloat xin, myGlfloat yin, myGlfloat zin, myGlfloat rin,
                   myGlfloat gin, myGlfloat bin)
        : x1{ xin }
        , y1{ yin }
        , z1{ zin }

        , r{ rin }
        , g{ gin }
        , b{ bin }

        , texX{ xin }
        , texY{ yin }
    {
    }

    VertexTextured(myGlfloat xin1, myGlfloat yin1, myGlfloat zin1,
                   myGlfloat rin, myGlfloat gin, myGlfloat bin,
                   myGlfloat texXin, myGlfloat texYin)
        : x1{ xin1 }
        , y1{ yin1 }
        , z1{ zin1 }

        , r{ rin }
        , g{ gin }
        , b{ bin }

        , texX{ texXin }
        , texY{ texYin }
    {
    }
    myGlfloat x1{ 0.0 };
    myGlfloat y1{ 0.0 };
    myGlfloat z1{ 0.0 };

    myGlfloat r{ 0.0 };
    myGlfloat g{ 0.0 };
    myGlfloat b{ 0.0 };

    myGlfloat texX{ 0.0 };
    myGlfloat texY{ 0.0 };
};
#pragma pack(pop)

struct OM_DECLSPEC Triangle
{
    Vertex v[3]{};
};

OM_DECLSPEC std::istream& operator>>(std::istream& in, Vertex& vertex);

OM_DECLSPEC std::istream& operator>>(std::istream& in, Triangle& triangle);

class OM_DECLSPEC IEngine
{
public:
    enum class EngineTypes : size_t
    {
        sdl,
        max_types,
    };
    virtual ~IEngine() noexcept {}
    /// create main window
    /// on success return empty string
    virtual std::string initialize(std::string_view config) = 0;
    /// pool event from input queue
    /// return true if more events in queue
    virtual bool read_input(Event& e) const = 0;
    virtual void uninitialize()             = 0;

    virtual bool renderClearWindow() = 0;

    virtual void render(const std::vector<VertexTextured>& vertices,
                        const std::vector<myUint>&         indices,
                        ShapeType type = ShapeType::triangle) = 0;

    virtual void render(const std::vector<Vertex>& vertices,
                        const std::vector<myUint>& indices,
                        ShapeType type = ShapeType::triangle) = 0;

    virtual bool loadTexture(const std::string_view textureName) = 0;

    virtual void               renderTriangle(const Triangle& t) = 0;
    virtual bool               updateWindow()                    = 0;
    static const om::myGlfloat minCoordinate;
    static const om::myGlfloat maxCoordinate;
};

} // end namespace om
