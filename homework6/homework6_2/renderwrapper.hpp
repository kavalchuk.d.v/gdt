#pragma once
#include "world.hpp"
#include <array>
#include <iengine.hpp>
#include <vector>

class RenderWrapper
{
public:
    RenderWrapper(std::array<om::myGlfloat, 3> color    = { 0, 1, 0 },
                  om::myGlfloat                gridStep = 0.025);

    void render(om::IEngine& engine, const World& world);

private:
    bool initGridBuffers(std::array<om::myGlfloat, 3> color,
                         om::myGlfloat                step);

    void renderRectangleGrid(om::IEngine& engine);

    std::vector<om::Vertex> formRectangleGridVertexBuffer(
        std::array<om::myGlfloat, 3> color, om::myGlfloat step,
        const size_t numberOfRow, const size_t numberOfVertecesInRow);

    std::vector<om::myUint> formRectangleGridIndexBuffer(
        om::myUint numberRowOfVertexes, om::myUint numberVertexesInRow);

    bool checkColor(std::array<om::myGlfloat, 3> color);
    bool checkStep(om::myGlfloat step);

    void renderWorld(om::IEngine& engine, const World& world);

    void renderTexturedTriangle(om::IEngine& engine);

    om::myGlfloat                m_gridStep;
    std::array<om::myGlfloat, 3> m_color;

    std::vector<om::Vertex> m_vertexBuffer;
    std::vector<om::myUint> m_indexBuffer;

    static const om::myGlfloat minStep;
    static const om::myGlfloat gridZcoord;
};
