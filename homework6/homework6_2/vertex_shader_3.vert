#version 330 core
// Location is set in bind method
// for having possibility to change order
// of attributes without changing shader
// we comment strings layout (location = n)

layout (location = 0) in vec3 a_position1;
layout (location = 1) in vec3 a_color;
layout (location = 2) in vec2 a_tex_position;

out vec4 v_position;
out vec3 v_color;
out vec2 v_tex_position;

void main()
{
    v_position = vec4(a_position1, 1.0);
    v_tex_position = a_tex_position;
    v_color = a_color;
    gl_Position = v_position;
}
