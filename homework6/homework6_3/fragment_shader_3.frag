#version 330 core
//#version 320 es

#ifdef GL_ES
precision mediump float;
attribute vec4 v_position;
attribute vec4 v_color;
attribute vec4 v_tex_position;
#else
in vec4 v_position;
in vec4 v_color;
in vec2 v_tex_position;
#endif

uniform sampler2D s_texture;

void main()
{
     gl_FragColor = texture2D(s_texture, v_tex_position);
}
