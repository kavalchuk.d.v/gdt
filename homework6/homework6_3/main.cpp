#include "environement.hpp"
#include "renderwrapper.hpp"
#include "utilities.hpp"
#include "world.hpp"
#include <engine_handler.hpp>

#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <unordered_set>

int main(int /*argc*/, char* /*argv*/[])
{
    using namespace om;
    constexpr auto             engineType = IEngine::EngineTypes::sdl;
    constexpr std::string_view config     = "";
    EngineHandler              engine(engineType, config);

    Environement environement;

    RenderWrapper renderWrapper{ *engine, { 0, 1, 0 }, 0.05 };

    Timer gameTime;

    World world{ gameTime.timerNow() };

    bool continue_loop = true;

    bool isTank = true;
    while (continue_loop)
    {
        Timer                     loopTimer;
        const World::WorldEvents* worldEvents;
        Environement::eventSet    environementEvents;

        continue_loop =
            environement.input(*engine, worldEvents, environementEvents);

        environement.handleEnvironementEvents(*engine, environementEvents);
        if (environement.isPause())
        {
            gameTime.pause();
        }
        else
        {
            gameTime.proceed();
            [[maybe_unused]] auto countWorldUpdating =
                world.update(gameTime.timerNow(), worldEvents);
        }
        if (environementEvents.find({ om::EventType::start_pressed }) !=
            end(environementEvents))
        {
            isTank = !isTank;
        }
        if (isTank)
        {
            renderWrapper.render();
        }
        else
        {
            renderWrapper.render(world);
        }

        engine->updateWindow();
        normalizeLoopDuration(loopTimer.elapsed());
    }

    return EXIT_SUCCESS;
}

// example with drawing triangles from file

// std::ifstream file{ "vertexes.txt" };
// Triangle      triangle1, triangle2;
// file >> triangle1 >> triangle2;
// std::vector<Vertex> vertexVector{ 6 };
// std::copy_n(&(triangle1.v[0]), 3, vertexVector.begin());
// std::copy_n(&(triangle2.v[0]), 3, vertexVector.begin() + 3);
// engine->render(gridVertexBuffer, gridIndexBuffer, ShapeType::line);
// engine->render(vertexVector, { 0, 1, 2, 4 }, ShapeType::line);
// engine->renderTriangle(triangle2);
