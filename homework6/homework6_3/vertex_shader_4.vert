#version 330 core
//#version 320 es

#ifdef GL_ES
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec3 a_position2;
attribute vec2 a_tex_position;

varying vec4 v_position;
varying vec4 v_color;
varying vec2 v_tex_position;
#else
in vec3 a_position1;
in vec4 a_color;
in vec3 a_position2;
in vec2 a_tex_position;

out vec4 v_position;
out vec4 v_color;
out vec2 v_tex_position;
#endif

// koefficient to morph vertex
uniform float u_koef;

void main()
{
    v_position = vec4(a_position1 * u_koef + a_position2 * (1.0f - u_koef), 1.0);
    v_tex_position = a_tex_position;
    v_color = a_color;
    gl_Position = v_position;
}
