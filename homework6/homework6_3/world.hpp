#pragma once
#include <chrono>
#include <iosfwd>
#define _USE_MATH_DEFINES
#include <array>
#include <cmath>
#include <unordered_map>

using gravityCalcType = float;

gravityCalcType calcDs(const gravityCalcType v0, gravityCalcType dt,
                       const gravityCalcType a);

constexpr gravityCalcType calcNextV(const gravityCalcType v0,
                                    const gravityCalcType a,
                                    const gravityCalcType dt);

constexpr gravityCalcType calcG(const gravityCalcType G = 6.67384e-11,
                                const gravityCalcType M = 5.972e24,
                                const gravityCalcType R = 6'371e3);

constexpr gravityCalcType calcFgravity(const gravityCalcType m);

gravityCalcType calcFresist(const gravityCalcType v, const gravityCalcType k1,
                            const gravityCalcType k2);

gravityCalcType calcNextA(const gravityCalcType m, const gravityCalcType v,
                          const gravityCalcType k1, const gravityCalcType k2);

gravityCalcType calcK1(const gravityCalcType I,
                       const gravityCalcType mu = 0.0182);

gravityCalcType calcK2(const gravityCalcType s, const gravityCalcType c,
                       const gravityCalcType ro = 1.29);

constexpr gravityCalcType calcCommandA(const gravityCalcType F,
                                       const gravityCalcType m);

int getRandom(int lowRange, int highRange);

class World
{
public:
    using gravityCalcType = double;
    enum class Events : size_t
    {
        userCommandShipUp,
        userCommandShipDown,
        userCommandShipTeleporation,
        maxType,
    };

    using WorldEvents =
        std::unordered_map<World::Events, std::array<double, 2>>;

    using clock_t        = std::chrono::steady_clock;
    using seconds_t      = std::chrono::duration<double, std::ratio<1>>;
    using milliseconds_t = std::chrono::duration<double, std::milli>;

    World(std::chrono::time_point<clock_t> initialTime)
        : lastUpdateTime{ initialTime }
    {
    }

    int update(std::chrono::time_point<clock_t> nowTime,
               const WorldEvents*               events);

    const gravityCalcType mObject = static_cast<gravityCalcType>(100); // kg
    const gravityCalcType cObject = static_cast<gravityCalcType>(0.4); // o.e.
    const gravityCalcType rObject = static_cast<gravityCalcType>(0.1); // meters
    const gravityCalcType commandForce =
        static_cast<gravityCalcType>(10'000); // newtons
    const gravityCalcType iObject =
        static_cast<gravityCalcType>(rObject); // meter
    const gravityCalcType sObject =
        static_cast<gravityCalcType>(M_PI * rObject * rObject); // meter2

    const gravityCalcType k1 = calcK1(iObject);
    const gravityCalcType k2 = calcK2(sObject, cObject);

    static const gravityCalcType sizeY;

    static const gravityCalcType sizeX;

    gravityCalcType y{ sizeY };
    gravityCalcType x{ 250 };

    gravityCalcType v{ 0 };
    gravityCalcType a{ 0 };

    std::chrono::time_point<clock_t> lastUpdateTime;
    seconds_t                        dt{ milliseconds_t{ 5 } };

private:
    float getCommandForce(bool isCommand);
};

std::ostream& operator<<(std::ostream& out, const World& world);
