#pragma once
#include "gltexture.hpp"
#include "iengine.hpp"
#include <glad.h>
#include <string>
#include <vector>

namespace om
{
class GlProgram
{
public:
    explicit GlProgram(
        const std::string_view& vertexShaderFileName,
        const std::string_view& fragmentShaderFileName,
        const std::vector<std::pair<GLuint, std::string_view>>& attributes);

    GlProgram(const GlProgram&) = delete;

    GlProgram& operator=(const GlProgram&) = delete;

    explicit GlProgram(GlProgram&& srcProgram);

    ~GlProgram();

    GlProgram& operator=(GlProgram&& srcProgram);

    bool use();
    bool setUniform(std::string_view uniformName, GLint parameters);
    bool setUniform(std::string_view uniformName, GLfloat parameters);
    bool setUniform(std::string_view     uniformName,
                    std::vector<GLfloat> parameters);

    bool setTextures(const std::vector<std::string_view>& textureUniformName,
                     const std::vector<GlTexture*>&       texture);
    bool setTexture(std::string_view textureUniformName, GlTexture* texture);
    void resetTextures() { m_nextTextureUnit = 0; }

    bool validate();

private:
    bool initShader(const std::string_view& shaderFileName, GLenum type,
                    GLuint& r_shader, std::stringstream& serr);

    bool initProgram(
        GLuint vertexShader, GLuint fragmentShader, GLuint& programId,
        const std::vector<std::pair<GLuint, std::string_view>>& attributes,
        std::stringstream&                                      serr);

    bool createProgramAndAttachShaders(GLuint vertexShader,
                                       GLuint fragmentShader, GLuint& programId,
                                       std::stringstream& serr);

    bool bindAttributesLocationAndLinkProgram(
        const GLuint                                            programId,
        const std::vector<std::pair<GLuint, std::string_view>>& attributes,
        std::stringstream&                                      serr);

    bool preSetUniform(std::string_view uniformName, GLint& location);

    friend bool operator==(GlProgram program1, GlProgram program2)
    {
        return program1.m_programId == program2.m_programId;
    }

    friend bool operator<(GlProgram program1, GlProgram program2)
    {
        return program1.m_programId < program2.m_programId;
    }
    GLuint                  m_programId{};
    GLenum                  m_nextTextureUnit{};
    static constexpr GLenum maxNumberOfTexturesForProgram{ 16 };
};

} // namespace om
