#pragma once
#include "glprogram.hpp"
#include "gltexture.hpp"
#include "iengine.hpp"
#include <SDL.h>
#include <glad.h>
#include <iosfwd>
#include <string>
#include <string_view>
#include <unordered_map>

namespace om
{

class EngineSdl : public IEngine
{
public:
    std::string initialize(std::string_view /*config*/) override final;
    bool        read_input(Event& e) const override final;
    void        uninitialize() override final;
    ~EngineSdl() override final {}

    ProgramId addProgram(const std::string_view& vertexShaderFileName,
                         const std::string_view& fragmentShaderFileName,
                         const std::vector<std::pair<myUint, std::string_view>>&
                             attributes) override;

    bool eraseProgram(ProgramId programId) override;

    TextureId addTexture(const std::string_view& pathToTexture) override;

    bool eraseTexture(TextureId textureId) override;

    bool setCurrentDefaultProgram(ProgramId programId) override;

    bool setUniform(std::string_view       uniformName,
                    std::vector<myGlfloat> parameters,
                    ProgramId              programId = ProgramId()) override;

    bool setUniform(std::string_view uniformName, om::myGlfloat parameters,
                    ProgramId programId = ProgramId()) override;

    bool renderClearWindow(Color color = { 0, 0, 0, 0 }) override;

    void render(const std::vector<VertexTextured>&   vertices,
                const std::vector<myUint>&           indices,
                const std::vector<TextureId>&        textureIds,
                const std::vector<std::string_view>& textureAttributesNames,
                const Matrix<3, 3>&                  moveMatrix,
                const std::string_view moveUnifromName = "u_move_matrix",
                ProgramId              programId       = ProgramId(),
                ShapeType              type = ShapeType::triangle) override;

    void render(const std::vector<VertexTextured>&   vertices,
                const std::vector<myUint>&           indices,
                const std::vector<TextureId>&        textureIds,
                const std::vector<std::string_view>& textureAttributesNames,
                ProgramId                            programId = ProgramId(),
                ShapeType type = ShapeType::triangle) override;

    void render(const std::vector<VertexWide>&       vertices,
                const std::vector<myUint>&           indices,
                const std::vector<TextureId>&        textureIds,
                const std::vector<std::string_view>& textureAttributesNames,
                ProgramId                            programId = ProgramId(),
                ShapeType type = ShapeType::triangle) override;

    void render(const std::vector<VertexMorphed>& vertices,
                const std::vector<myUint>&        indices,
                ProgramId                         programId = ProgramId(),
                ShapeType type = ShapeType::triangle) override;

    void render(const std::vector<Vertex>& vertices,
                const std::vector<myUint>& indices,
                ProgramId                  programId = ProgramId(),
                ShapeType                  type = ShapeType::triangle) override;

    void renderTriangle(const Triangle<Vertex>& t,
                        ProgramId programId = ProgramId()) override;

    bool updateWindow(Color fillingColor = { 0, 0, 0, 0 }) override;

    std::array<myGlfloat, 2> getWindowInchesSize() override;

private:
    bool initSdl(std::stringstream& serr);
    bool initSdlWindow(std::stringstream& serr);
    bool initOpenGl(std::stringstream& serr);

    bool loadGLFunctionsPointers(std::stringstream& serr);

    bool initBuffersAndVAO(std::stringstream& serr);
    bool initIndexBuffer(std::stringstream& serr);

    void       setDebugOpenGl();
    void       setAdditionalGlParameters();
    GlProgram* findProgram(ProgramId programId);
    GlTexture* findTexture(TextureId textureId);

    template <typename T, typename = std::enable_if_t<is_vertex<T>>>
    void renderInternal(const std::vector<T>&      vertices,
                        const std::vector<myUint>& indices, ShapeType type,
                        ProgramId programId);

    template <typename T, typename = std::enable_if_t<is_texture_vertex<T>>>
    void renderTexturedInternal(
        const std::vector<T>& vertices, const std::vector<myUint>& indices,
        std::vector<TextureId>        textureIds,
        std::vector<std::string_view> textureAttributesNames, ShapeType type,
        ProgramId programId);

    template <typename T, typename = std::enable_if_t<is_vertex<T>>>
    void renderTriangleInternal(const Triangle<T>& t, ProgramId programId);

    SDL_Window*   m_window{};
    SDL_GLContext m_glContext{};

    GlProgram* m_currentProgram{};
    ProgramId  lastProgramId{ 0 };
    std::unordered_map<ProgramId, GlProgram, MyIdsHash<ProgramId>> m_programs;

    GlTexture* m_currentTexture{};
    TextureId  lastTextureId{ 0 };
    std::unordered_map<TextureId, GlTexture, MyIdsHash<TextureId>> m_textures;

    myGlfloat m_windowWidthDpi{};
    myGlfloat m_windowHeightpi{};

    static constexpr myGlfloat hardcodedWpixels{ 1024 };
    static constexpr myGlfloat hardcodedHpixels{ 768 };
};

} // end namespace om
