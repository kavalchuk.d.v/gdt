#pragma once
#include <array>
#include <iosfwd>

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

namespace om
{
using myGlfloat = float;
using myUint    = unsigned int;

struct OM_DECLSPEC Color
{
    myGlfloat r{};
    myGlfloat g{};
    myGlfloat b{};
    myGlfloat a{ 1 };
};

struct OM_DECLSPEC Position3
{
    myGlfloat x{};
    myGlfloat y{};
    myGlfloat z{};
};

struct OM_DECLSPEC Position2
{
    myGlfloat x{};
    myGlfloat y{};
};

#pragma pack(push, 1)
struct OM_DECLSPEC Vertex
{
    Vertex(myGlfloat xin, myGlfloat yin, myGlfloat zin, myGlfloat rin,
           myGlfloat gin, myGlfloat bin, myGlfloat ain = 1)
        : position{ xin, yin, zin }
        , color{ rin, gin, bin, ain }
    {
    }

    Vertex(Position3 pos, Color col)
        : position{ pos }
        , color{ col }
    {
    }

    Vertex()
        : position{}
        , color{}
    {
    }

    Position3 position;
    Color     color;

public:
    static constexpr myUint positionByteOffset = 0;
    static constexpr myUint colorByteOffset =
        positionByteOffset + sizeof(position);
    static constexpr myUint positionAttributeNumber = 0;
    static constexpr myUint colorAttributeNumber    = 1;
    static constexpr myUint positionAttributeAmount =
        sizeof(position) / sizeof(position.x);
    static constexpr myUint colorAttributeAmount =
        sizeof(color) / sizeof(color.a);

    static constexpr std::array<myUint, 2> attributesNumbers{
        positionAttributeNumber, colorAttributeNumber
    };
    static constexpr std::array<myUint, 2> attributesByteOffsets{
        positionByteOffset, colorByteOffset
    };
    static constexpr std::array<myUint, 2> attributesAmounts{
        positionAttributeAmount, colorAttributeAmount
    };
};

struct OM_DECLSPEC VertexMorphed : public Vertex
{
    VertexMorphed(myGlfloat xin1, myGlfloat yin1, myGlfloat zin1, myGlfloat rin,
                  myGlfloat gin, myGlfloat bin, myGlfloat ain, myGlfloat xin2,
                  myGlfloat yin2, myGlfloat zin2)
        : Vertex(xin1, yin1, zin1, rin, gin, bin, ain)
        , position2{ xin2, yin2, zin2 }
    {
    }

    VertexMorphed(myGlfloat xin, myGlfloat yin, myGlfloat zin, myGlfloat rin,
                  myGlfloat gin, myGlfloat bin, myGlfloat ain = 1)
        : VertexMorphed(xin, yin, zin, rin, gin, bin, ain, xin, yin, zin)
    {
    }

    VertexMorphed(Position3 posStart, Color col, Position3 posFinish)
        : Vertex(posStart, col)
        , position2{ posFinish }
    {
    }

    VertexMorphed()
        : position2{}
    {
    }

    Position3 position2;

    static constexpr myUint position2ByteOffset =
        colorByteOffset + sizeof(color);
    static constexpr myUint position2AttributeNumber = 2;

    static constexpr std::array<myUint, 3> attributesNumbers{
        positionAttributeNumber, colorAttributeNumber, position2AttributeNumber
    };
    static constexpr std::array<myUint, 3> attributesByteOffsets{
        positionByteOffset, colorByteOffset, position2ByteOffset
    };
    static constexpr std::array<myUint, 3> attributesAmounts{
        positionAttributeAmount,
        colorAttributeAmount,
        positionAttributeAmount,
    };
};

struct OM_DECLSPEC VertexTextured : public Vertex
{
    VertexTextured(myGlfloat xin, myGlfloat yin, myGlfloat zin, myGlfloat rin,
                   myGlfloat gin, myGlfloat bin, myGlfloat ain,
                   myGlfloat xin_tex, myGlfloat yin_tex)
        : Vertex(xin, yin, zin, rin, gin, bin, ain)
        , position_tex{ xin_tex, yin_tex }
    {
    }

    VertexTextured(Position3 pos, Color col, Position2 pos_tex)
        : Vertex(pos, col)
        , position_tex{ pos_tex }
    {
    }

    VertexTextured()
        : position_tex{}
    {
    }

    Position2 position_tex;

public:
    static constexpr myUint positionTexByteOffset =
        colorByteOffset + sizeof(color);
    static constexpr myUint positionTexAttributeNumber = 3;

    static constexpr myUint positionTexAttributeAmount =
        sizeof(position_tex) / sizeof(position_tex.x);

    static constexpr std::array<myUint, 3> attributesNumbers{
        positionAttributeNumber, colorAttributeNumber,
        positionTexAttributeNumber
    };
    static constexpr std::array<myUint, 3> attributesByteOffsets{
        positionByteOffset, colorByteOffset, positionTexByteOffset
    };
    static constexpr std::array<myUint, 3> attributesAmounts{
        positionAttributeAmount,
        colorAttributeAmount,
        positionTexAttributeAmount,
    };
};

struct OM_DECLSPEC VertexWide : public VertexMorphed
{
    VertexWide(myGlfloat xinStart, myGlfloat yinStart, myGlfloat zinStart,
               myGlfloat rin, myGlfloat gin, myGlfloat bin, myGlfloat ain,
               myGlfloat xinFinish, myGlfloat yinFinish, myGlfloat zinFinish,
               myGlfloat xin_tex, myGlfloat yin_tex)
        : VertexMorphed(xinStart, yinStart, zinStart, rin, gin, bin, ain,
                        xinFinish, yinFinish, zinFinish)
        , position_tex{ xin_tex, yin_tex }
    {
    }

    VertexWide(Position3 posStart, Color col, Position3 posFinish,
               Position2 positionTex)
        : VertexMorphed(posStart, col, posFinish)
        , position_tex{ positionTex }
    {
    }

    VertexWide() {}

    Position2 position_tex;

public:
    static constexpr myUint positionTexAttributeNumber = 3;
    static constexpr myUint positionTexByteOffset =
        position2ByteOffset + sizeof(position2);
    static constexpr myUint positionTexAttributeAmount =
        sizeof(position_tex) / sizeof(position_tex.x);

    static constexpr std::array<myUint, 4> attributesNumbers{
        positionAttributeNumber, colorAttributeNumber, position2AttributeNumber,
        positionTexAttributeNumber
    };
    static constexpr std::array<myUint, 4> attributesByteOffsets{
        positionByteOffset, colorByteOffset, position2ByteOffset,
        positionTexByteOffset
    };
    static constexpr std::array<myUint, 4> attributesAmounts{
        positionAttributeAmount,
        colorAttributeAmount,
        positionAttributeAmount,
        positionTexAttributeAmount,
    };
};
#pragma pack(pop)

template <typename T>
constexpr bool is_vertex = std::is_base_of<Vertex, T>::value;

template <typename T>
constexpr bool is_texture_vertex = std::is_base_of<VertexTextured, T>::value ||
                                   std::is_base_of<VertexWide, T>::value;

template <typename T, typename = std::enable_if_t<is_vertex<T>>>
struct OM_DECLSPEC Triangle
{
    T v[3]{};
};

OM_DECLSPEC std::istream& operator>>(std::istream& in, Vertex& vertex);

OM_DECLSPEC std::istream& operator>>(std::istream&     in,
                                     Triangle<Vertex>& triangle);
} // namespace om
