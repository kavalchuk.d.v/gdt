#version 330 core
//#version 320 es

#ifdef GL_ES
attribute vec3 a_position;
attribute vec4 a_color;
attribute vec2 a_tex_position;

varying vec4 v_position;
varying vec4 v_color;
varying vec2 v_tex_position;
#else
in vec3 a_position;
in vec4 a_color;
in vec2 a_tex_position;

out vec4 v_position;
out vec4 v_color;
out vec2 v_tex_position;
#endif

// move matrix
uniform mat3 u_move_matrix;

void main()
{
    vec3 moved_position = u_move_matrix * a_position;
    v_position = vec4(moved_position, 1.0);
    v_tex_position = a_tex_position;
    v_color = a_color;
    gl_Position = v_position;
}
