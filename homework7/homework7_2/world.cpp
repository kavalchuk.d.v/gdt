#include "world.hpp"

#include <iostream>

const World::gravityCalcType World::sizeY{ 1500 };

const World::gravityCalcType World::sizeX{ 1500 };

gravityCalcType calcDs(const gravityCalcType v0, gravityCalcType dt,
                       const gravityCalcType a)
{
    return v0 * dt + a * std::pow(dt, 2) / 2;
}

constexpr gravityCalcType calcNextV(const gravityCalcType v0,
                                    const gravityCalcType a,
                                    const gravityCalcType dt)
{
    return v0 + a * dt;
}

constexpr gravityCalcType calcG(const gravityCalcType G,
                                const gravityCalcType M,
                                const gravityCalcType R)
{
    return G * M / (R * R);
}

constexpr gravityCalcType calcFgravity(const gravityCalcType m)
{
    return m * calcG();
}

gravityCalcType calcFresist(const gravityCalcType v, const gravityCalcType k1,
                            const gravityCalcType k2)
{
    auto fresistK1 = -(k1 * v);
    auto fresistK2 = -(k2 * v * std::fabs(v));
    return fresistK1 + fresistK2;
}

gravityCalcType calcNextA(const gravityCalcType m, const gravityCalcType v,
                          const gravityCalcType k1, const gravityCalcType k2)
{
    // zero m should be checked before calling calcNextA
    return (calcFgravity(m) + calcFresist(v, k1, k2)) / m;
}

gravityCalcType calcK1(const gravityCalcType I, const gravityCalcType mu)
{
    return static_cast<gravityCalcType>(6) *
           static_cast<gravityCalcType>(M_PI) * mu * I;
}

gravityCalcType calcK2(const gravityCalcType s, const gravityCalcType c,
                       const gravityCalcType ro)
{
    return s * c * ro / 2;
}

constexpr gravityCalcType calcCommandA(const gravityCalcType F,
                                       const gravityCalcType m)
{
    return F / m;
}

// int getRandom(int lowRange, int highRange)
//{
//    static std::random_device rd;
//    static std::mt19937       mersenne(rd());
//    return ((mersenne()) % (highRange - lowRange + 1) + lowRange);
//}

int World::update(std::chrono::time_point<clock_t> nowTime,
                  const WorldEvents*               events)
{
    int             count         = 0;
    bool            isCommand     = false;
    gravityCalcType signOfCommand = 0.0;
    auto isContainUp = events->find(Events::userCommandShipUp) != events->end();
    auto isContainDown =
        events->find(Events::userCommandShipDown) != events->end();
    if (isContainUp && !isContainDown)
    {
        isCommand     = true;
        signOfCommand = static_cast<gravityCalcType>(+1.0);
    }
    else if (isContainDown && !isContainUp)
    {
        isCommand     = true;
        signOfCommand = static_cast<gravityCalcType>(-1.0);
    }

    auto teleportationEventIt =
        events->find(Events::userCommandShipTeleporation);

    if (teleportationEventIt != events->end())
    {
        const auto& newXY = teleportationEventIt->second;
        this->x           = newXY[0];
        this->y           = newXY[1];
    }

    while (lastUpdateTime + dt < nowTime)
    {
        auto allForces = (calcFgravity(mObject) +
                          signOfCommand * getCommandForce(isCommand) +
                          calcFresist(v, k1, k2));

        a         = allForces / mObject;
        auto newV = calcNextV(v, a, static_cast<gravityCalcType>(dt.count()));
        y -= calcDs(v, static_cast<gravityCalcType>(dt.count()), newV);
        if (y <= 0.0)
        {
            y = 0.0;
            if (v >= 0.3)
            {
                newV = -v / 2;
            }
        }
        v = newV;
        lastUpdateTime =
            std::chrono::time_point_cast<std::chrono::steady_clock::duration>(
                lastUpdateTime + dt);
        ++count;
    }
    return count;
}

gravityCalcType World::getCommandForce(bool isCommand)
{
    return (isCommand) ? -World::commandForce
                       : static_cast<gravityCalcType>(0.0);
}

std::ostream& operator<<(std::ostream& out, const World& world)
{
    out << "H: " << world.y << " V: " << world.v << " A: " << world.a
        << " T: " << world.lastUpdateTime.time_since_epoch().count() << '\n';
    return out;
}
