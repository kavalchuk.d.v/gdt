#version 330 core
//#version 320 es

#ifdef GL_ES
precision mediump float;
attribute vec4 v_position;
attribute vec4 v_color;
#else
in vec4 v_position;
in vec4 v_color;
#endif

void main()
{
     gl_FragColor = v_color;
}
