#include "environement.hpp"
#include "renderwrapper.hpp"
#include "utilities.hpp"
#include "world.hpp"
#include <engine_handler.hpp>

#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <unordered_set>

int main(int /*argc*/, char* /*argv*/[])
{
    using namespace om;
    constexpr auto             engineType = IEngine::EngineTypes::sdl;
    constexpr std::string_view gameTitle{ "Ultimate space simulator" };
    constexpr std::string_view config{};
    EngineHandler              engine(engineType, gameTitle, config);

    Environement environement;

    RenderWrapper renderWrapper{ *engine, { 0, 1, 0 }, 0.05 };

    Timer gameTime;

    World world{ gameTime.timerNow() };

    bool continue_loop = true;
    bool isTank        = true;
    while (continue_loop)
    {
        Timer                     loopTimer;
        const World::WorldEvents* worldEvents;
        Environement::eventSet    environementEvents;

        continue_loop =
            environement.input(*engine, worldEvents, environementEvents);

        environement.handleEnvironementEvents(*engine, environementEvents);
        if (environement.isPause())
        {
            gameTime.pause();
        }
        else
        {
            gameTime.proceed();
            [[maybe_unused]] auto countWorldUpdating =
                world.update(gameTime.timerNow(), worldEvents);
        }
        if (environementEvents.find({ om::EventType::start_pressed }) !=
            end(environementEvents))
        {
            isTank = !isTank;
        }
        if (isTank)
        {
            renderWrapper.render();
            engine->updateWindow();
        }
        else
        {
            renderWrapper.render(world);
            engine->updateWindow({ 0.2, 0.2, 1.0, 0.0 });
        }

        std::cout << world;

        normalizeLoopDuration(loopTimer.elapsed());
    }

    return EXIT_SUCCESS;
}
