#include "renderwrapper.hpp"
#include "utilities.hpp"

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexAttributePositions{
        { om::Vertex::positionAttributeNumber, "a_position" },
        { om::Vertex::colorAttributeNumber, "a_color" }
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexMorphedAttributePositions{
        { om::VertexMorphed::positionAttributeNumber, "a_position1" },
        { om::VertexMorphed::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexTexturedAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexWideAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position1" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::string_view> RenderWrapper::textureAttributeNames{
    "s_texture"
};

RenderWrapper::RenderWrapper(om::IEngine&                 engine,
                             std::array<om::myGlfloat, 3> color,
                             om::myGlfloat                gridStep)
    : m_engine{ engine }
{
    if (!initGridBuffers(color, gridStep))
    {
        std::cerr << "Cannot create RenderWrapper(). Color or gridStep are "
                     "incorrrect."
                  << std::endl;
        throw std::runtime_error(
            "Cannot create RenderWrapper(). Color or gridStep are "
            "incorrrect.");
    }

    m_programIdShaderGrid =
        m_engine.addProgram("vertex_shader_1.vert", "fragment_shader_1.frag",
                            vertexAttributePositions);

    m_programIdShaderMorph =
        m_engine.addProgram("vertex_shader_2.vert", "fragment_shader_2.frag",
                            vertexMorphedAttributePositions);

    m_programIdTexturedMorphed =
        m_engine.addProgram("vertex_shader_3.vert", "fragment_shader_3.frag",
                            vertexWideAttributePositions);

    m_programIdTexturedMoved =
        m_engine.addProgram("vertex_shader_5.vert", "fragment_shader_5.frag",
                            vertexTexturedAttributePositions);

    m_programIdMorphedMoved =
        m_engine.addProgram("vertex_shader_6.vert", "fragment_shader_6.frag",
                            vertexMorphedAttributePositions);

    m_textureIdTank = m_engine.addTexture("tank.png");

    m_engine.setCurrentDefaultProgram(m_programIdShaderGrid);

    m_color    = color;
    m_gridStep = gridStep;
}

bool RenderWrapper::initGridBuffers(std::array<om::myGlfloat, 3> color,
                                    om::myGlfloat                step)
{
    if (!(checkColor(color) && checkStep(step)))
    {
        return false;
    }

    const auto numberOfRow =
        static_cast<size_t>(
            (om::IEngine::maxCoordinate - om::IEngine::minCoordinate) / step) +
        1;

    const auto numberOfVertecesInRow = numberOfRow;

    m_vertexBuffer = formRectangleGridVertexBuffer(color, step, numberOfRow,
                                                   numberOfVertecesInRow);
    m_indexBuffer =
        formRectangleGridIndexBuffer(numberOfRow, numberOfVertecesInRow);

    return true;
}

bool RenderWrapper::checkColor(std::array<om::myGlfloat, 3> color)
{
    for (const auto colorPart : color)
    {
        if (colorPart > 1.f && colorPart < 0.f)
        {
            return false;
        }
    }
    return true;
}

bool RenderWrapper::checkStep(om::myGlfloat step)
{
    if (step < minStep && step > 1)
    {
        return false;
    }
    return true;
}

void RenderWrapper::renderRectangleTexturedTank(om::IEngine& engine)
{
    static std::vector<om::VertexTextured> vertexes{
        { -0.5, -0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
        { -0.5, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 },
        { 0.5, -0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0 },
        { 0.5, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0 }
    };

    std::vector<om::myUint> indexes{ 0, 1, 3, 0, 2, 3 };

    static Timer timer;
    float        elapsedSeconds = timer.elapsed().count();

    const om::myGlfloat rotatingPeriod{ 2 };
    auto                angle = elapsedSeconds / rotatingPeriod * M_PI - M_PI;

    om::Vector<3> rotatingCenter{
        (vertexes[2].position.x + vertexes[0].position.x) / 2,
        (vertexes[0].position.y + vertexes[1].position.y) / 2, 1.0
    };

    const auto rotateMatrix{ om::MatrixFunctor::getRotateMatrix(
        angle, rotatingCenter) };

    const auto windowSize = engine.getWindowInchesSize();

    const auto yToXRelate{ static_cast<om::myGlfloat>(windowSize[1]) /
                           static_cast<om::myGlfloat>(windowSize[0]) };

    const auto aspectMatrix{ om::MatrixFunctor::getScaleMatrix(
        { yToXRelate, 1.0 }, rotatingCenter) };

    const om::myGlfloat xShift = std::sin(angle);

    const om::myGlfloat yShift = -std::sin(angle) / 2;

    const auto shiftMatrix{ om::MatrixFunctor::getShiftMatrix(
        { xShift, yShift, 1.0 }) };

    const auto resultMatrix = aspectMatrix * shiftMatrix * rotateMatrix;

    engine.render(vertexes, indexes, { m_textureIdTank }, textureAttributeNames,
                  resultMatrix, moveMatrixUniformName,
                  m_programIdTexturedMoved);
}

void RenderWrapper::render()
{
    renderRectangleTexturedTank(m_engine);
}

void RenderWrapper::render(const World& world)
{
    renderRectangleGrid();

    renderWorld(world);
}

static std::vector<om::VertexMorphed> formShipVertexBuffer(
    const om::myGlfloat shipZ)
{
    const om::myGlfloat            sizeX{ 0.2 };
    const om::myGlfloat            sizeY{ 0.3 };
    std::vector<om::VertexMorphed> vertexes;
    vertexes.reserve(11);

    vertexes.push_back({ 0, 0 + sizeY, shipZ, 0.3, 0.3, 0.3 }); // 0

    vertexes.push_back({ vertexes[0].position.x - sizeX / 9.0f,
                         vertexes[0].position.y - sizeY / 4, shipZ, 0.3, 0.3,
                         0.3 }); // 1

    vertexes.push_back({ vertexes[0].position.x + sizeX / 9.0f,
                         vertexes[1].position.y, shipZ, 0.3, 0.3, 0.3 }); // 2

    vertexes.push_back({ vertexes[1].position.x,
                         vertexes[1].position.y - sizeY * 3 / 8.0f, shipZ, 0, 0,
                         0 }); // 3

    vertexes.push_back({ vertexes[2].position.x,
                         vertexes[1].position.y - sizeY * 3 / 8.0f, shipZ, 0, 0,
                         0 }); // 4

    vertexes.push_back({ vertexes[3].position.x,
                         vertexes[3].position.y + sizeY / 8, shipZ, 0, 0,
                         0 }); // 5

    vertexes.push_back({ vertexes[3].position.x + sizeX / 6,
                         vertexes[3].position.y, shipZ, 0, 0, 0, 1,
                         vertexes[3].position.x - sizeX / 6,
                         vertexes[3].position.y, shipZ }); // 6

    vertexes.push_back({ vertexes[4].position.x,
                         vertexes[4].position.y + sizeY / 8.0f, shipZ, 0, 0,
                         0 }); // 7

    vertexes.push_back({ vertexes[4].position.x - sizeX / 6,
                         vertexes[4].position.y, shipZ, 0, 0, 0, 1,
                         vertexes[4].position.x + sizeX / 6,
                         vertexes[4].position.y, shipZ }); // 8

    vertexes.push_back(
        { vertexes[6].position.x, vertexes[6].position.y - 3 * sizeY / 8.0f,
          shipZ, 0, 0, 0, 1, vertexes[6].position2.x,
          vertexes[6].position.y - 3 * sizeY / 8.0f, shipZ }); // 9

    vertexes.push_back(
        { vertexes[8].position.x, vertexes[8].position.y - 3 * sizeY / 8.0f,
          shipZ, 0, 0, 0, 1, vertexes[8].position2.x,
          vertexes[8].position.y - 3 * sizeY / 8.0f, shipZ }); // 10

    return vertexes;
}

static std::vector<om::myUint> formShipIndexBuffer()
{
    return { 0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 5, 6, 4, 7, 8, 3, 6, 9, 4, 8, 10 };
}

void RenderWrapper::renderWorld(const World& world)
{
    const om::myGlfloat worldShipZ =
        ((gridZcoord - 0.1) >= -1.0) ? gridZcoord - 0.1 : gridZcoord;

    const auto sizeCoord =
        om::IEngine::maxCoordinate - om::IEngine::minCoordinate;

    for (const auto& currentObject : world.objects)
    {
        const auto relatedXShip = currentObject.x * sizeCoord / world.sizeX - 1;
        const auto relatedYShip = currentObject.y * sizeCoord / world.sizeY - 1;
        const auto vertexes     = formShipVertexBuffer(worldShipZ);
        const auto indexes      = formShipIndexBuffer();
        auto koef = static_cast<om::myGlfloat>(currentObject.y / world.sizeY);
        if (koef > 1.0)
        {
            koef = static_cast<om::myGlfloat>(1.0);
        }

        om::Vector<3> rotatingCenter{
            (vertexes[9].position.x + vertexes[10].position.x) / 2,
            (vertexes[0].position.y + vertexes[10].position.y) / 2, 1.0
        };

        const auto rotateMatrix{ om::MatrixFunctor::getRotateMatrix(
            currentObject.angle, rotatingCenter) };

        const auto windowSize = m_engine.getWindowInchesSize();

        const auto yToXRelate{ static_cast<om::myGlfloat>(windowSize[1]) /
                               static_cast<om::myGlfloat>(windowSize[0]) };

        const auto aspectMatrix{ om::MatrixFunctor::getScaleMatrix(
            { yToXRelate, 1.0 }, { 0.0, 0.0, 1.0 }) };

        const auto shiftMatrix{ om::MatrixFunctor::getShiftMatrix(
            { static_cast<om::myGlfloat>(relatedXShip),
              static_cast<om::myGlfloat>(relatedYShip), 1.0 }) };

        const auto resultMatrix = aspectMatrix * shiftMatrix * rotateMatrix;

        m_engine.setUniform("u_koef", koef, m_programIdMorphedMoved);
        m_engine.render(vertexes, indexes, resultMatrix, moveMatrixUniformName,
                        m_programIdMorphedMoved, om::ShapeType::triangle);
    }
}

void RenderWrapper::renderRectangleGrid()
{
    m_engine.render(m_vertexBuffer, m_indexBuffer, m_programIdShaderGrid,
                    om::ShapeType::line);
}

std::vector<om::Vertex> RenderWrapper::formRectangleGridVertexBuffer(
    std::array<om::myGlfloat, 3> color, om::myGlfloat step,
    const size_t numberOfRow, const size_t numberOfVertecesInRow)
{
    std::vector<om::Vertex> vertexBuffer;

    const auto vertexNumber =
        static_cast<size_t>(numberOfRow * numberOfVertecesInRow);

    vertexBuffer.reserve(vertexNumber);
    for (size_t currentRow = 0; currentRow < numberOfRow; ++currentRow)
    {
        for (size_t currentVeretexInRow = 0;
             currentVeretexInRow < numberOfVertecesInRow; ++currentVeretexInRow)
        {
            const auto currentPosX =
                om::IEngine::minCoordinate + currentRow * step;
            const auto currentPosY =
                om::IEngine::minCoordinate + currentVeretexInRow * step;
            vertexBuffer.push_back({ currentPosX, currentPosY, gridZcoord,
                                     color[0], color[1], color[2] });
        }
    }
    return vertexBuffer;
}

std::vector<om::myUint> RenderWrapper::formRectangleGridIndexBuffer(
    om::myUint numberRowOfVertexes, om::myUint numberVertexesInRow)
{
    const size_t numberOfIndices =
        numberRowOfVertexes * (numberVertexesInRow - 1) * 4;

    std::vector<om::myUint> indexBuffer{};

    indexBuffer.reserve(numberOfIndices);
    // Horyzontal lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes; ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        for (om::myUint positionInRow = 0;
             positionInRow < numberVertexesInRow - 1; ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = thisIndex + 1;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    // Vertical lines
    for (om::myUint rowNumber = 0; rowNumber < numberRowOfVertexes - 1;
         ++rowNumber)
    {
        const auto beginOfThisRow = rowNumber * numberVertexesInRow;
        const auto beginOfNextRow = (rowNumber + 1) * numberVertexesInRow;
        for (om::myUint positionInRow = 0; positionInRow < numberVertexesInRow;
             ++positionInRow)
        {
            const auto thisIndex = beginOfThisRow + positionInRow;
            const auto nextIndex = beginOfNextRow + positionInRow;
            indexBuffer.push_back(thisIndex);
            indexBuffer.push_back(nextIndex);
        }
    }
    return indexBuffer;
}
