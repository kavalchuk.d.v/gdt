#version 330 core
//#version 320 es

#ifdef GL_ES
attribute vec3 a_position;
attribute vec4 a_color;

varying vec4 v_position;
varying vec4 v_color;
#else
in vec3 a_position;
in vec4 a_color;

out vec4 v_position;
out vec4 v_color;
#endif

uniform vec3 u_mouse;

void main()
{
    float dx = (a_position.x - u_mouse.x);
    float dx2 = dx*dx;
    float dy = (a_position.y - u_mouse.y);
    float dy2 = dy*dy;
    float distance = sqrt(dx2 + dy2);
    float r = u_mouse.z;

    const float distanceEpsilon = 0.0005;

    if (distance >= r){
        v_position = vec4(a_position, 1.0);
        gl_Position = v_position;
    }else{
        float zeroShift = 0.2;
        float shift = zeroShift*(r-distance);
        float cosa = 0.0;
        float sina = 1.0;

        if (distance > distanceEpsilon){
            cosa = dx / distance;
            sina = dy / distance;
        }

        float shiftX = shift * cosa;
        float shiftY = shift * sina;

        float newX = a_position.x + shiftX;
        float newY = a_position.y + shiftY;

        v_position = vec4(newX, newY, a_position.z, 1.0);
        gl_Position = v_position;
    }

    v_color = a_color;
}
