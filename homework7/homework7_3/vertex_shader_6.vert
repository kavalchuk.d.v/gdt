#version 330 core
//#version 320 es

#ifdef GL_ES
attribute vec3 a_position1;
attribute vec4 a_color;
attribute vec3 a_position2;

varying vec4 v_position;
varying vec4 v_color;
#else
in vec3 a_position1;
in vec4 a_color;
in vec3 a_position2;

out vec4 v_position;
out vec4 v_color;
#endif

uniform float u_koef;
// move matrix
uniform mat3 u_move_matrix;

void main()
{
    vec3 morphedPos = a_position1 * u_koef + a_position2 * (1.0f - u_koef);
    vec3 moved_position =   u_move_matrix * vec3(morphedPos.x, morphedPos.y, 1.0);
    v_position = vec4(moved_position.x,moved_position.y, morphedPos.z, 1.0);

    v_color = a_color;
    gl_Position = v_position;
}
