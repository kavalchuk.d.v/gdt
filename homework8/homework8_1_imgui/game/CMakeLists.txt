cmake_minimum_required(VERSION 3.16)

project(homework8_1_imgui LANGUAGES CXX C)

# TODO windows implementation should be added
add_executable(homework8_1
    main.cpp
    world.cpp
    world.hpp
    utilities.cpp
    utilities.hpp
    environement.cpp
    environement.hpp
    render_wrapper.cpp
    render_wrapper.hpp
    imgui_wrapper.cpp
    imgui_wrapper.hpp
    ../engine/matrix.hpp
    ../engine/engine_handler.hpp
    shaders/vertex_shader_1.vert
    shaders/vertex_shader_2.vert
    shaders/vertex_shader_3.vert
    shaders/vertex_shader_4.vert
    shaders/vertex_shader_5.vert
    shaders/vertex_shader_6.vert
    shaders/fragment_shader_1.frag
    shaders/fragment_shader_2.frag
    shaders/fragment_shader_3.frag
    shaders/fragment_shader_4.frag
    shaders/fragment_shader_5.frag
    shaders/fragment_shader_6.frag
    textures/tank.png
    
    )

target_compile_features(homework8_1 PUBLIC cxx_std_17)

if(CMAKE_BUILD_TYPE MATCHES Debug)
target_compile_definitions(homework8_1 PRIVATE "-DDEBUG_CONFIGURATION")
endif()

target_compile_options(homework8_1 PRIVATE
  $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX>
  $<$<NOT:$<CXX_COMPILER_ID:MSVC>>:-Wall -Wextra -pedantic -Werror -fsanitize=leak -pg>
)

target_link_libraries(
        homework8_1
        PRIVATE
        engine_lib
)

set (source_shader "${CMAKE_CURRENT_SOURCE_DIR}/shaders")
set (destination_shader "${CMAKE_CURRENT_BINARY_DIR}/shaders")
set (source_textures "${CMAKE_CURRENT_SOURCE_DIR}/textures")
set (destination_textures "${CMAKE_CURRENT_BINARY_DIR}/textures")

add_custom_command(
 TARGET homework8_1 POST_BUILD
 COMMAND ${CMAKE_COMMAND} -E copy_directory ${source_shader} ${destination_shader}
 COMMAND ${CMAKE_COMMAND} -E copy_directory ${source_textures} ${destination_textures}
 DEPENDS ${destination_shader}
 COMMENT "copy resources folder"
)






