#pragma once
#include "world.hpp"
#include <iengine.hpp>

class ImguiWrapper
{
public:
    void createImguiObjects(om::IEngine& engine, World& world);
};
