#include "environement.hpp"
#include "render_wrapper.hpp"
#include "utilities.hpp"
#include "world.hpp"
#include <engine_handler.hpp>

#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <unordered_set>

int main(int /*argc*/, char* /*argv*/[])
{
    using namespace om;
    constexpr auto             engineType = IEngine::EngineTypes::sdl;
    constexpr std::string_view gameTitle{ "Ultimate space simulator" };
    constexpr std::string_view config{};
    EngineHandler              engine(engineType, gameTitle, config);

    Environement  environement;
    RenderWrapper renderWrapper{ *engine, { 0, 1, 0 }, 0.05 };
    ImguiWrapper  imguiWrapper;

    Timer gameTime;

    World world{ gameTime.timerNow() };

    bool continue_loop = true;
    bool isTank        = true;

    int loopCount{};
    while (continue_loop)
    {
        Timer                     loopTimer;
        const World::WorldEvents* worldEvents;
        Environement::eventSet    environementEvents;

        Timer tempTimer;
        continue_loop =
            environement.input(*engine, worldEvents, environementEvents);
        [[maybe_unused]] const auto inputTime = tempTimer.elapsed().count();
        tempTimer.reset();

        environement.handleEnvironementEvents(*engine, environementEvents);
        [[maybe_unused]] const auto envEventsTime = tempTimer.elapsed().count();
        tempTimer.reset();

        if (environement.isPause())
        {
            gameTime.pause();
        }
        else
        {

            gameTime.proceed();
            [[maybe_unused]] auto countWorldUpdating =
                world.update(gameTime.timerNow(), worldEvents);
        }

        if (environementEvents.find({ om::EventType::start_pressed }) !=
            end(environementEvents))
        {
            isTank = !isTank;
        }

        [[maybe_unused]] const auto worldUpdateTime =
            tempTimer.elapsed().count();
        tempTimer.reset();

        if (isTank)
        {
            renderWrapper.render();
            engine->updateWindow();
        }
        else
        {
            renderWrapper.render(world);
            imguiWrapper.createImguiObjects(*engine, world);

            engine->updateWindow({ 0.2, 0.2, 1.0, 0.0 });
        }

        [[maybe_unused]] const auto renderTime = tempTimer.elapsed().count();
        tempTimer.reset();

#ifdef DEBUG_CONFIGURATION
        if (loopCount == 10)
        {
            std::clog << world;
            std::clog << " Full time: " << loopTimer.elapsed().count()
                      << " Input time: " << inputTime
                      << " EnviromentEventsTime: " << envEventsTime
                      << " WorldUpdateTime: " << worldUpdateTime
                      << " RenderTime : " << renderTime << '\n';
            loopCount = 0;
        }
#endif

        normalizeLoopDuration(loopTimer.elapsed(),
                              engine->getDisplayRefreshRate());

        ++loopCount;
    }

    return EXIT_SUCCESS;
}
