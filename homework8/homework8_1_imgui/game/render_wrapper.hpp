#pragma once
#include "world.hpp"
#include <iengine.hpp>

#include "imgui_wrapper.hpp"
#include <array>
#include <vector>

class RenderWrapper
{
public:
    RenderWrapper(om::IEngine&                 engine,
                  std::array<om::myGlfloat, 3> color    = { 0, 1, 0 },
                  om::myGlfloat                gridStep = 0.025);

    void render(const World& world);
    void render();

private:
    bool initGridBuffers(std::array<om::myGlfloat, 3> color,
                         om::myGlfloat                step);

    void                    renderRectangleGrid();
    std::vector<om::Vertex> formRectangleGridVertexBuffer(
        std::array<om::myGlfloat, 3> color, om::myGlfloat step,
        const size_t numberOfRow, const size_t numberOfVertecesInRow);

    std::vector<om::myUint> formRectangleGridIndexBuffer(
        om::myUint numberRowOfVertexes, om::myUint numberVertexesInRow);

    bool checkColor(std::array<om::myGlfloat, 3> color);
    bool checkStep(om::myGlfloat step);

    void renderWorld(const World& world);

    void renderTexturedTriangle();

    void renderRectangleTexturedTank(om::IEngine& engine);

    om::myGlfloat                m_gridStep;
    std::array<om::myGlfloat, 3> m_color;

    std::vector<om::Vertex> m_vertexBuffer;
    std::vector<om::myUint> m_indexBuffer;

    om::ProgramId m_programIdShaderGrid;
    om::ProgramId m_programIdShaderMorph;
    om::ProgramId m_programIdTexturedMorphed;
    om::ProgramId m_programIdTexturedMoved;
    om::ProgramId m_programIdMorphedMoved;

    om::TextureId m_textureIdTank;
    om::TextureId m_textureIdFlag;

    om::IEngine& m_engine;

    static constexpr om::myGlfloat minStep{ 0.005 };
    static constexpr om::myGlfloat gridZcoord{ 0.0 };

    static constexpr std::string_view moveMatrixUniformName{ "u_move_matrix" };

    static const std::vector<std::string_view> textureAttributeNames;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexMorphedAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexTexturedAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexWideAttributePositions;
};
