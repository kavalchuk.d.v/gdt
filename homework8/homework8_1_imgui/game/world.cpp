#include "world.hpp"

#include <algorithm>

worldCalcType PhysicalObject::getCommandDirectForce()
{
    return commandDirectForce * mainEnginesPercentThrust / 100;
}
worldCalcType PhysicalObject::getCommandBackwardForce()
{
    return -commandBackwardForce * sideEnginesPercentThrust / 100;
}
worldCalcType PhysicalObject::getCommandRotateForce()
{
    return CommandRotateForce * sideEnginesPercentThrust / 100;
}

World::World(std::chrono::time_point<clock_t> initialTime)
    : userShipIndex{ 0 }
    , lastUpdateTime{ initialTime }
{
    objects.push_back({ 200, sizeY });
}

int World::update(std::chrono::time_point<clock_t> nowTime,
                  const WorldEvents*               events)
{
    int  count       = 0;
    auto isContainUp = events->find(Events::userCommandShipUp) != events->end();
    auto isContainDown =
        events->find(Events::userCommandShipDown) != events->end();

    worldCalcType motionCommandForce{};

    if (isContainUp && !isContainDown)
    {
        motionCommandForce = objects[userShipIndex].getCommandDirectForce();
    }
    else if (isContainDown && !isContainUp)
    {
        motionCommandForce = objects[userShipIndex].getCommandBackwardForce();
    }

    auto isContainRotateLeft =
        events->find(Events::userCommandShipRotateLeft) != events->end();

    auto isContainRotateRight =
        events->find(Events::userCommandShipRotateRight) != events->end();

    worldCalcType rotateCommandForce{};
    if (isContainRotateLeft && !isContainRotateRight)
    {
        rotateCommandForce = objects[userShipIndex].getCommandRotateForce();
    }
    else if (isContainRotateRight && !isContainRotateLeft)
    {
        rotateCommandForce = -objects[userShipIndex].getCommandRotateForce();
    }

    auto isAnyCommandEnabled = isContainRotateLeft || isContainRotateRight;

    if (objects[userShipIndex].stabilizationLevel1Enabled)
    {
        if ((objects[userShipIndex].angleSpeed > 0 && isContainRotateRight) ||
            (objects[userShipIndex].angleSpeed < 0 && isContainRotateLeft))
        {
            rotateCommandForce *= 2;
        }
    }

    auto teleportationEventIt =
        events->find(Events::userCommandShipTeleporation);

    if (teleportationEventIt != events->end())
    {
        const auto& newXY        = teleportationEventIt->second;
        objects[userShipIndex].x = newXY[0];
        objects[userShipIndex].y = newXY[1];
    }

    while (lastUpdateTime + dt < nowTime)
    {
        if (!isAnyCommandEnabled &&
            objects[userShipIndex].stabilizationLevel2Enabled)
        {
            if (std::abs(objects[userShipIndex].angleSpeed) >
                std::numeric_limits<worldCalcType>::epsilon())
                rotateCommandForce =
                    -objects[userShipIndex].angleSpeed *
                    objects[userShipIndex].inertionMoment /
                    (dt.count() * objects[userShipIndex].r * 2);
            if (rotateCommandForce >
                objects[userShipIndex].getCommandRotateForce() * 10)
            {
                rotateCommandForce =
                    objects[userShipIndex].getCommandRotateForce() * 5;
            }
        }

        // change 0 direction to axe X
        auto allForcesX =
            (motionCommandForce * std::sin(-objects[userShipIndex].angle) +
             Resist::calcFresist(objects[userShipIndex].vx,
                                 objects[userShipIndex].k1,
                                 objects[userShipIndex].k2));

        auto allForcesY =
            (-Gravity::calcFgravity(objects[userShipIndex].m) +
             motionCommandForce * std::cos(-objects[userShipIndex].angle) +
             Resist::calcFresist(objects[userShipIndex].vy,
                                 objects[userShipIndex].k1,
                                 objects[userShipIndex].k2));

        objects[userShipIndex].ax =
            Motion::calcNextA(objects[userShipIndex].m, allForcesX);

        objects[userShipIndex].ay =
            Motion::calcNextA(objects[userShipIndex].m, allForcesY);

        auto newVX = Motion::calcNextV(objects[userShipIndex].vx,
                                       objects[userShipIndex].ax,
                                       static_cast<worldCalcType>(dt.count()));

        auto newVY = Motion::calcNextV(objects[userShipIndex].vy,
                                       objects[userShipIndex].ay,
                                       static_cast<worldCalcType>(dt.count()));

        objects[userShipIndex].x +=
            Motion::calcDs(objects[userShipIndex].vx, newVX,
                           static_cast<worldCalcType>(dt.count()));

        objects[userShipIndex].y +=
            Motion::calcDs(objects[userShipIndex].vy, newVY,
                           static_cast<worldCalcType>(dt.count()));

        if (objects[userShipIndex].y <= 0.0)
        {
            objects[userShipIndex].y = 0.0;
            newVY                    = -objects[userShipIndex].vy / 2;
        }

        objects[userShipIndex].vx = newVX;
        objects[userShipIndex].vy = newVY;

        auto linearRotatingSpeed = Rotation::calcLinearSpeed(
            objects[userShipIndex].angleSpeed, objects[userShipIndex].r);

        auto forceOfResistane =
            Resist::calcFresist(linearRotatingSpeed, objects[userShipIndex].k1,
                                objects[userShipIndex].k2);

        auto allForceMoments =
            Rotation::calcForceMoment(2 * forceOfResistane,
                                      objects[userShipIndex].r / 2) +
            Rotation::calcForceMoment(2 * rotateCommandForce,
                                      objects[userShipIndex].r);

        objects[userShipIndex].angleAcceleration =
            Rotation::calcNextAngleAcceleration(
                allForceMoments, objects[userShipIndex].inertionMoment);

        auto newAngleSpeed = Rotation::calcNextAngleSpeed(
            objects[userShipIndex].angleSpeed,
            objects[userShipIndex].angleAcceleration,
            static_cast<worldCalcType>(dt.count()));

        objects[userShipIndex].angle += Rotation::calcShiftAngle(
            objects[userShipIndex].angleSpeed, newAngleSpeed,
            static_cast<worldCalcType>(dt.count()));

        if (objects[userShipIndex].angle >= 2 * M_PI)
        {
            objects[userShipIndex].angle -= 2 * M_PI;
        }

        objects[userShipIndex].angleSpeed = newAngleSpeed;

        lastUpdateTime =
            std::chrono::time_point_cast<std::chrono::steady_clock::duration>(
                lastUpdateTime + dt);
        ++count;
    }
    return count;
}

std::ostream& operator<<(std::ostream& out, const PhysicalObject& object)
{
    out << "X: " << object.x << "Y: " << object.y << " Vx: " << object.vx
        << " Vy: " << object.vy << " Ax: " << object.ax << " Ay: " << object.ay
        << " angle: " << object.angle << " angleSpeed : " << object.angleSpeed
        << " angleAccel " << object.angleAcceleration
        << " linear rotating speed "
        << Rotation::calcLinearSpeed(object.angleSpeed, object.r) << '\n';
    return out;
}

std::ostream& operator<<(std::ostream& out, const World& world)
{
    out << "List of objects: " << '\n';
    std::for_each_n(world.objects.begin(), world.objects.size(),
                    [&out](const PhysicalObject& object) { out << object; });
    out << " T: " << world.lastUpdateTime.time_since_epoch().count() / 1.0e9
        << '\n';
    return out;
}
