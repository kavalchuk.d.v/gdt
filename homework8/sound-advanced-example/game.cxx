#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hxx"

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<om::engine, void (*)(om::engine*)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    om::texture* texture = engine->create_texture("tank.png");
    if (nullptr == texture)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    om::vertex_buffer* vertex_buf = nullptr;

    std::ifstream file("vert_tex_color.txt");
    if (!file)
    {
        std::cerr << "can't load vert_tex_color.txt\n";
        return EXIT_FAILURE;
    }
    else
    {
        std::array<om::tri2, 2> tr;
        file >> tr[0] >> tr[1];
        vertex_buf = engine->create_vertex_buffer(&tr[0], tr.size());
        if (vertex_buf == nullptr)
        {
            std::cerr << "can't create vertex buffer\n";
            return EXIT_FAILURE;
        }
    }

    auto soundTrack1 = engine->create_sound_track("t2_no_problemo.wav");

    auto soundTrack3 = engine->create_sound_track("rocket_sound_1.wav");

    auto soundTrack2 = engine->create_sound_track("highlands.wav");

    om::sound_buffer* s3 = engine->create_sound_buffer(soundTrack3);

    om::sound_buffer* s1 = engine->create_sound_buffer(soundTrack1);
    om::sound_buffer* s2 = engine->create_sound_buffer(soundTrack2);

    bool continue_loop = true;

    om::vec2    current_tank_pos(0.f, 0.f);
    float       current_tank_direction(0.f);
    const float pi = 3.1415926f;

    s2->play(om::sound_buffer::properties::once);

    s3->play(om::sound_buffer::properties::looped);

    while (continue_loop)
    {
        om::event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event.type)
            {
                case om::event_type::hardware:
                    if (std::get<om::hardware_data>(event.info).is_reset)
                    {
                        continue_loop = false;
                    }
                    break;
                case om::event_type::input_key:
                {
                    const auto& key_data = std::get<om::input_data>(event.info);
                    if (key_data.is_down)
                    {
                        if (key_data.key == om::keys::button1)
                        {
                            // if you want to play same buffer uncomment string
                            // below and comment play_sound_buffer_once
                            // s1->play(om::sound_buffer::properties::once);
                            engine->play_sound_buffer_once(soundTrack1);
                        }
                        else if (key_data.key == om::keys::button2)
                        {
                            // s2->play(om::sound_buffer::properties::looped);
                        }
                    }
                }
                break;
            }
        }

        if (engine->is_key_down(om::keys::left))
        {
            current_tank_pos.x -= 0.01f;
            current_tank_direction = -pi / 2.f;
        }
        else if (engine->is_key_down(om::keys::right))
        {
            current_tank_pos.x += 0.01f;
            current_tank_direction = pi / 2.f;
        }
        else if (engine->is_key_down(om::keys::up))
        {
            current_tank_pos.y += 0.01f;
            current_tank_direction = 0.f;
        }
        else if (engine->is_key_down(om::keys::down))
        {
            current_tank_pos.y -= 0.01f;
            current_tank_direction = -pi;
        }

        auto volumeValue =
            1.f - std::min(current_tank_pos.x * current_tank_pos.x +
                               current_tank_pos.y * current_tank_pos.y,
                           4.f) /
                      4.f;

        auto distance = std::sqrt(current_tank_pos.x * current_tank_pos.x +
                                  current_tank_pos.y * current_tank_pos.y);

        auto sinA{ 1.f };

        if (distance > std::numeric_limits<float>::epsilon() * 2)
        {
            sinA = std::abs(current_tank_pos.x / distance);
        }

        const auto sign = (current_tank_pos.x > 0.0) ? -1.0 : 1.0;

        const auto maxLeftRightRegulation{ 0.15f };

        auto leftRightValue = sign * sinA * maxLeftRightRegulation;

        auto stereoValue = sign * sinA;

        if (std::abs(current_tank_pos.y) < 0.5f &&
            std::abs(current_tank_pos.x) < 0.5f)
        {
            leftRightValue *= std::abs(current_tank_pos.x) *
                              std::abs(current_tank_pos.y) * 1.f / 0.5 * 1.f /
                              0.5;

            stereoValue *= std::abs(current_tank_pos.x) *
                           std::abs(current_tank_pos.y) * 1.f / 0.5 * 1.f / 0.5;
        }

        s2->setVolume(volumeValue);

        s2->setLeftRightBalance(leftRightValue);

        s2->setStereo(stereoValue);

        s1->setVolume(volumeValue);

        s1->setLeftRightBalance(leftRightValue);

        s1->setStereo(stereoValue);

        om::mat2x3 move   = om::mat2x3::move(current_tank_pos);
        om::mat2x3 aspect = om::mat2x3::scale(1, 640.f / 480.f);
        om::mat2x3 rot    = om::mat2x3::rotation(current_tank_direction);
        om::mat2x3 m      = rot * aspect * move;

        engine->render(*vertex_buf, texture, m);

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
