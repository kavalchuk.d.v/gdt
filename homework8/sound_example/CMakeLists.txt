cmake_minimum_required(VERSION 3.16)
project(sound_example CXX)

add_executable(sound_example sound_test.cxx)
target_compile_features(sound_example PUBLIC cxx_std_20)

find_package(SDL2 REQUIRED)


target_link_libraries(sound_example PRIVATE SDL2::SDL2 SDL2::SDL2main)

