#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>

#include <SDL.h>

constexpr int32_t AUDIO_FORMAT = AUDIO_S16LSB;

#pragma pack(push, 1)
struct AudioBuffer
{
    uint_least8_t* start{};
    size_t         size        = 0;
    size_t         current_pos = 0;
    struct
    {
        size_t frequncy = 0;
        double time     = 0.0;
        bool   use_note = false;
    } note;
};
#pragma pack(pop)

std::ostream& operator<<(std::ostream& o, const SDL_AudioSpec& spec);

static void audio_callback(void* userdata, uint8_t* stream, int len);

int main(int /*argc*/, char* /*argv*/[])
{
    std::clog << "start sdl init" << std::endl;
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
        std::cerr << "error: can't init audio: " << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    const char* file_name = "highlands.wav";

    SDL_RWops* file = SDL_RWFromFile(file_name, "rb");
    if (file == nullptr)
    {
        std::cerr << "error: can't open file: " << file_name << "\n";
        return EXIT_FAILURE;
    }

    SDL_AudioSpec audio_spec_from_file{};
    const int32_t auto_delete_file            = 1;
    uint8_t*      sample_buffer_from_file     = nullptr;
    uint32_t      sample_buffer_len_from_file = 0;

    std::clog << "loading sample buffer from file: " << file_name << std::endl;

    SDL_AudioSpec* audio_spec =
        SDL_LoadWAV_RW(file, auto_delete_file, &audio_spec_from_file,
                       &sample_buffer_from_file, &sample_buffer_len_from_file);

    if (audio_spec == nullptr)
    {
        std::cerr << "error: can't parse and load audio samples from file\n";
        return EXIT_FAILURE;
    }

    std::clog << "loaded file audio spec:\n"
              << audio_spec_from_file << std::endl;

    // clang-format off
    AudioBuffer loaded_audio_buff
    {
        .start = sample_buffer_from_file,
        .size = sample_buffer_len_from_file,
        .current_pos = 0,
        .note = {
         .frequncy = 0,
         .time = 0,
         .use_note = false
        }
    };
    // clang-format on
    SDL_AudioSpec desired{ .freq     = 48000,
                           .format   = AUDIO_FORMAT,
                           .channels = 2, // stereo
                           .silence  = 0,
                           .samples  = 1024, // must be power of 2
                           .padding  = 0,
                           .size     = 0,
                           .callback = audio_callback,
                           .userdata = &loaded_audio_buff };

    std::clog << "prepare disired audio specs for output device:\n"
              << desired << std::endl;

    const char*   device_name       = nullptr; // device name or nullptr
    const int32_t is_capture_device = 0; // 0 - play device, 1 - microphone
    const uint_least32_t allow_changes{};
    SDL_AudioSpec        returned{};

    SDL_AudioDeviceID audio_device_id = SDL_OpenAudioDevice(
        device_name, is_capture_device, &desired, &returned, allow_changes);
    if (audio_device_id == 0)
    {
        std::cerr << "error: failed to open audio device: " << SDL_GetError()
                  << std::endl;
        return EXIT_FAILURE;
    }
    std::clog << "returned audio spec for output device:\n"
              << returned << std::endl;

    if (desired.format != returned.format ||
        desired.channels != returned.channels || desired.freq != returned.freq)
    {
        std::cerr << "error: disired != returned audio device settings!";
        return EXIT_FAILURE;
    }

    std::clog << "unpause audio device (start audio thread)" << std::endl;
    SDL_PauseAudioDevice(audio_device_id, SDL_FALSE);

    while (true)
    {
        bool a{ true };
        std::cin >> a;
        if (!a)
        {
            break;
        }
    }

    std::clog << "pause audio device (stop audio thread)" << std::endl;
    // stop audio device and stop thread call our callback function
    SDL_PauseAudioDevice(audio_device_id, SDL_TRUE);

    std::clog << "close audio device" << std::endl;

    SDL_CloseAudioDevice(audio_device_id);

    SDL_FreeWAV(loaded_audio_buff.start);

    SDL_Quit();

    return EXIT_SUCCESS;
}

static void audio_callback(void* userdata, uint8_t* stream, int len)
{
    size_t stream_len = static_cast<size_t>(len);
    // silence
    std::memset(stream, 0, static_cast<size_t>(len));

    AudioBuffer* audio_buff_data = reinterpret_cast<AudioBuffer*>(userdata);

    assert(audio_buff_data != nullptr);

    while (stream_len > 0)
    {
        // copy data from loaded buffer into output stream
        const uint8_t* current_sound_pos =
            audio_buff_data->start + audio_buff_data->current_pos;

        const size_t left_in_buffer =
            audio_buff_data->size - audio_buff_data->current_pos;

        const auto currentTime = std::chrono::duration_cast<
            std::chrono::duration<double, std::ratio<1>>>(
            std::chrono::steady_clock::now().time_since_epoch());

        const auto relLoudness = std::sin(currentTime.count() / 2) / 2;

        const auto soundPower =
            static_cast<int>(std::abs(SDL_MIX_MAXVOLUME * relLoudness)) +
            SDL_MIX_MAXVOLUME / 2;

        size_t num_samples = stream_len / 2 / 2;
        double dt          = 1.0 / 48000.0;

        int16_t* output = reinterpret_cast<int16_t*>(stream);

        for (size_t sample = 0; sample < num_samples; ++sample)
        {
            double omega_t = audio_buff_data->note.time * 2.0 * 3.1415926 *
                             audio_buff_data->note.frequncy;
            double curr_sample =
                std::numeric_limits<int16_t>::max() * sin(omega_t);
            int16_t curr_val = static_cast<int16_t>(curr_sample);

            *output = curr_val;
            output++;
            *output = curr_val;
            output++;

            audio_buff_data->note.time += dt;
        }

        std::clog << "Sound relative: " << relLoudness
                  << "SoundVolume: " << soundPower << std::endl;

        if (left_in_buffer > stream_len)
        {
            SDL_MixAudioFormat(stream, current_sound_pos, AUDIO_FORMAT, len,
                               soundPower);
            audio_buff_data->current_pos += stream_len;
            break;
        }
        else
        {

            // first copy rest from buffer and repeat sound from begining
            SDL_MixAudioFormat(stream, current_sound_pos, AUDIO_FORMAT,
                               left_in_buffer, soundPower);
            // start buffer from begining
            audio_buff_data->current_pos = 0;
            stream_len -= left_in_buffer;
        }
    }
}

std::ostream& operator<<(std::ostream& o, const SDL_AudioSpec& spec)
{
    o << "\tfreq: " << spec.freq << '\n'
      << "\tformat: " << std::hex << spec.format << '\n'
      << "\tchannels: " << std::dec << int(spec.channels) << '\n'
      << "\tsilence: " << int(spec.silence) << '\n'
      << "\tsamples: " << spec.samples << '\n'
      << "\tsize: " << spec.size << '\n'
      << "\tcallback: " << reinterpret_cast<const void*>(spec.callback) << '\n'
      << "\tuserdata: " << spec.userdata;
    return o;
}
