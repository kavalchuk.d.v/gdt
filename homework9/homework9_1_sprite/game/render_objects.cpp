#include "render_objects.hpp"
#include "global.hpp"
#include "utilities.hpp"

using namespace renderObjects;

RocketMainCorpus::RocketMainCorpus(const std::string_view textureAttribureName,
                                   const std::string_view moveMatrixUniformName,
                                   const om::ProgramId&   programId,
                                   const om::TextureId&   tex)
{
    // TODO! 16/9 screen should be expected
    const om::Vector<2> texturePixcelSize{
        457,
        1024,
    };

    const om::Vector<2> textureSpritePos{
        17 / texturePixcelSize.elements[0],
        28 / texturePixcelSize.elements[1],
    };

    const om::Vector<2> textureSpriteSize{
        1 - textureSpritePos.elements[0] * 2,
        1 - textureSpritePos.elements[1] - 24 / texturePixcelSize.elements[1]
    };

    const auto rocketSizeX = static_cast<float>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketSizeY = static_cast<float>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    m_sprites.push_back({ std::string{ "rocket_corpus" },
                          textureAttribureName,
                          moveMatrixUniformName,
                          programId,
                          tex,
                          { textureSpritePos, textureSpriteSize },
                          { { 0, 0 }, { rocketSizeX, rocketSizeY } },
                          0.f });
}

const om::Vector<2>& RocketMainCorpus::getPos() const
{
    return m_pos;
}
void RocketMainCorpus::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}
om::myGlfloat RocketMainCorpus::getAngle() const
{
    return m_angle;
}
void RocketMainCorpus::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void RocketMainCorpus::draw(om::IEngine& render, const PhysicalObject& rocket)
{
    const auto rocketNdcX =
        static_cast<om::myGlfloat>(rocket.x) * Global::getCurrentWorldScaleY();

    const auto rocketNdcY =
        static_cast<om::myGlfloat>(rocket.y) * Global::getCurrentWorldScaleY();

    const auto rocketSizeX = static_cast<float>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketSizeY = static_cast<float>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    m_sprites[0].setSpriteSize({ rocketSizeX, rocketSizeY });
    m_sprites[0].setSpritePos({ rocketNdcX, rocketNdcY });

    m_sprites[0].setAngle(rocket.angle);
    m_sprites[0].draw(render);
}

MainEngineFire::MainEngineFire(const std::string_view textureAttribureName,
                               const std::string_view moveMatrixUniformName,
                               const om::ProgramId&   programId,
                               const om::TextureId&   tex)
{
    // TODO! 16/9 screen should be expected

    const auto textureNumberOfLines   = 5;
    const auto textureNumberOfColumns = 6;

    const om::myGlfloat textureStepX   = 1.0 / textureNumberOfColumns;
    const om::myGlfloat textureStepY   = 1.0 / textureNumberOfLines;
    const om::myGlfloat renderBaseSize = 0.2;

    const om::Vector<2> textureSpriteSize{ textureStepX, textureStepY };

    std::vector<Sprite> allSprites;
    allSprites.reserve(30);
    {
        om::myGlfloat textureXCurent = 0.0;
        om::myGlfloat textureYCurent = 1.0 - textureStepY;

        for (int line = 0; line < textureNumberOfLines; ++line)
        {
            for (int column = 0; column < textureNumberOfColumns; ++column)
            {
                allSprites.push_back(
                    { std::string{ "main_eng_fire" } + std::to_string(line) +
                          " " + std::to_string(column),
                      textureAttribureName,
                      moveMatrixUniformName,
                      programId,
                      tex,
                      { { textureXCurent, textureYCurent }, textureSpriteSize },
                      { { 0, 0 }, { renderBaseSize, renderBaseSize } },
                      0.f });
                textureXCurent += textureStepX;
            }
            textureXCurent = 0.0;
            textureYCurent -= textureStepY;
        }
    }

    std::for_each_n(allSprites.begin(), allSprites.size(),
                    [](Sprite& sprite) { sprite.setBaseAngle(M_PI); });

    std::copy_n(allSprites.begin(), 9, std::back_inserter(m_spritesStart));
    std::copy_n(&allSprites[10], 10, std::back_inserter(m_spritesRun));
    std::copy_n(&allSprites[20], 10, std::back_inserter(m_spritesStop));

    m_animation.sprites(m_spritesStart, m_spritesRun, m_spritesStop);
    m_animation.setFps(20);
}

MainEngineFire& MainEngineFire::operator=(const MainEngineFire& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;

    this->m_spritesStart = mainEngineFire2.m_spritesStart;
    this->m_spritesRun   = mainEngineFire2.m_spritesRun;
    this->m_spritesStop  = mainEngineFire2.m_spritesStop;

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
    return *this;
}

MainEngineFire::MainEngineFire(const MainEngineFire& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;

    this->m_spritesStart = mainEngineFire2.m_spritesStart;
    this->m_spritesRun   = mainEngineFire2.m_spritesRun;
    this->m_spritesStop  = mainEngineFire2.m_spritesStop;

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
}

MainEngineFire& MainEngineFire::operator=(
    const MainEngineFire&& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;

    this->m_spritesStart = std::move(mainEngineFire2.m_spritesStart);
    this->m_spritesRun   = std::move(mainEngineFire2.m_spritesRun);
    this->m_spritesStop  = std::move(mainEngineFire2.m_spritesStop);

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
    return *this;
}

MainEngineFire::MainEngineFire(const MainEngineFire&& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;

    this->m_spritesStart = std::move(mainEngineFire2.m_spritesStart);
    this->m_spritesRun   = std::move(mainEngineFire2.m_spritesRun);
    this->m_spritesStop  = std::move(mainEngineFire2.m_spritesStop);

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
}

const om::Vector<2>& MainEngineFire::getPos() const
{
    return m_pos;
}
void MainEngineFire::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}

const om::Vector<2>& MainEngineFire::getSize() const
{
    return m_size;
}
void MainEngineFire::setSize(const om::Vector<2>& size)
{
    m_size = size;
}

om::myGlfloat MainEngineFire::getAngle() const
{
    return m_angle;
}

void MainEngineFire::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void MainEngineFire::draw(om::IEngine& render, Event event,
                          Timer::seconds_t eventTime, om::Vector<2> basePoint)
{
    auto currentSprite = m_animation.getCurrentSprite(event, eventTime);

    if (!currentSprite)
    {
        return;
    }
    currentSprite->setSpriteSize(m_size);
    currentSprite->setSpritePos(m_pos);
    currentSprite->setAngle(m_angle);
    currentSprite->draw(render, basePoint);
}

Rocket::Rocket(const std::string_view textureAttribureName,
               const std::string_view moveMatrixUniformName,
               const om::ProgramId&   programId,
               const om::TextureId&   texMainCorpus,
               const om::TextureId&   texMainEngineFire)
    : mainCorpus{ textureAttribureName, moveMatrixUniformName, programId,
                  texMainCorpus }
    , mainEngineFire{ textureAttribureName, moveMatrixUniformName, programId,
                      texMainEngineFire }
    , m_mainCorpusRelativePos{ 0.f, 0.f }
    , m_mainEngineFireRelativePos{ 0.f, 0.40f }
    , m_mainEngineFireRelativeSize{ 1.0f, 1.0f }
{
}

const om::Vector<2>& Rocket::getPos() const
{
    return m_pos;
}
void Rocket::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}
om::myGlfloat Rocket::getAngle() const
{
    return m_angle;
}
void Rocket::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void Rocket::draw(om::IEngine& render, const PhysicalObject& rocket)
{
    mainEngineFire.setAngle(rocket.angle);

    const auto ndcXShip =
        static_cast<om::myGlfloat>(rocket.x * Global::getCurrentWorldScaleY());
    const auto ndcYShip =
        static_cast<om::myGlfloat>(rocket.y * Global::getCurrentWorldScaleY());

    const om::Vector<2> ndcCoordShip{ ndcXShip, ndcYShip };

    const auto rocketNdcSizeX = static_cast<om::myGlfloat>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketNdcSizeY = static_cast<om::myGlfloat>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    const om::Vector<2> rocketNdcSize{ rocketNdcSizeX, rocketNdcSizeY };

    const om::Vector<2> mainEngineFireNdcSize =
        m_mainEngineFireRelativeSize * rocketNdcSize;

    const auto ndcMainEngineFireShift =
        m_mainEngineFireRelativePos * (mainEngineFireNdcSize + rocketNdcSize);

    const auto ndcMainEngineFire = ndcCoordShip - ndcMainEngineFireShift;

    mainEngineFire.setAngle(rocket.angle);

    mainEngineFire.setSize(mainEngineFireNdcSize);

    mainEngineFire.setPos(ndcMainEngineFire);

    MainEngineFire::Event event = (rocket.getMainEngineState().getState())
                                      ? MainEngineFire::Event::Run
                                      : MainEngineFire::Event::Stop;

    mainEngineFire.draw(render, event, rocket.getMainEngineState().getTime(),
                        ndcMainEngineFireShift);

    mainCorpus.draw(render, rocket);
}
