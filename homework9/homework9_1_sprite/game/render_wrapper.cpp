﻿#include "render_wrapper.hpp"
#include "global.hpp"
#include "utilities.hpp"

#include <algorithm>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexAttributePositions{
        { om::Vertex::positionAttributeNumber, "a_position" },
        { om::Vertex::colorAttributeNumber, "a_color" }
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexMorphedAttributePositions{
        { om::VertexMorphed::positionAttributeNumber, "a_position1" },
        { om::VertexMorphed::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexTexturedAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::pair<om::myUint, std::string_view>>
    RenderWrapper::vertexWideAttributePositions{
        { om::VertexTextured::positionAttributeNumber, "a_position1" },
        { om::VertexTextured::colorAttributeNumber, "a_color" },
        { om::VertexMorphed::position2AttributeNumber, "a_position2" },
        { om::VertexTextured::positionTexAttributeNumber, "a_tex_position" },
    };

const std::vector<std::string_view> RenderWrapper::textureAttributeNames{
    "s_texture"
};

using namespace renderObjects;

RenderWrapper::RenderWrapper(om::IEngine&                 engine,
                             std::array<om::myGlfloat, 3> color,
                             om::myGlfloat                gridStep)
    : m_engine{ engine }
{

    m_programIdShaderGrid = m_engine.addProgram(
        "shaders/vertex_shader_1.vert", "shaders/fragment_shader_1.frag",
        vertexAttributePositions);

    m_programIdShaderMorph = m_engine.addProgram(
        "shaders/vertex_shader_2.vert", "shaders/fragment_shader_2.frag",
        vertexMorphedAttributePositions);

    m_programIdTexturedMorphed = m_engine.addProgram(
        "shaders/vertex_shader_3.vert", "shaders/fragment_shader_3.frag",
        vertexWideAttributePositions);

    m_programIdTexturedMoved = m_engine.addProgram(
        "shaders/vertex_shader_5.vert", "shaders/fragment_shader_5.frag",
        vertexTexturedAttributePositions);

    m_programIdMorphedMoved = m_engine.addProgram(
        "shaders/vertex_shader_6.vert", "shaders/fragment_shader_6.frag",
        vertexMorphedAttributePositions);

    m_programIdMoved = m_engine.addProgram("shaders/vertex_shader_moved.vert",
                                           "shaders/fragment_shader_moved.frag",
                                           vertexAttributePositions);

    m_textureIdTank           = m_engine.addTexture("textures/flag.png");
    m_textureIdFireMainEngine = m_engine.addTexture("textures/flame_fire.png");
    m_textureIdRocketMainCorpus =
        m_engine.addTexture("textures/topdownfighter.png");

    mainEngineFire = renderObjects::MainEngineFire(
        textureAttributeNames[0], moveMatrixUniformName,
        m_programIdTexturedMoved, m_textureIdFireMainEngine);

    rocketMainCorpus = renderObjects::RocketMainCorpus(
        textureAttributeNames[0], moveMatrixUniformName,
        m_programIdTexturedMoved, m_textureIdRocketMainCorpus);

    rocket = renderObjects::Rocket(
        textureAttributeNames[0], moveMatrixUniformName,
        m_programIdTexturedMoved, m_textureIdRocketMainCorpus,
        m_textureIdFireMainEngine);

    m_engine.setCurrentDefaultProgram(m_programIdTexturedMoved);

    tankSprite = { "tank",
                   textureAttributeNames[0],
                   moveMatrixUniformName,
                   m_programIdTexturedMoved,
                   m_textureIdRocketMainCorpus,
                   { { 0.f, 0.f }, { 1.f, 1.f } },
                   { { 0.f, 0.f }, { 2.f, 2.f } },
                   0.f };

    m_color    = color;
    m_gridStep = gridStep;
}

bool RenderWrapper::checkColor(std::array<om::myGlfloat, 3> color)
{
    return std::none_of(
        color.begin(), color.end(), [](om::myGlfloat colorComponent) {
            return (colorComponent > 1.f || colorComponent < 0.f);
        });
}

void RenderWrapper::renderRectangleTexturedTank(om::IEngine& engine)
{
    static std::vector<om::VertexTextured> vertexes{
        { { -1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 0.0, 0.0 } },
        { { -1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 0.0, 1.0 } },
        { { 1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 1.0, 0.0 } },
        { { 1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f }, { 1.0, 1.0 } }
    };

    static std::vector<om::Vertex> verticesTest{
        { { -1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { -1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { 1.0, -1.0 }, { 0.f, 0.f, 1.f, 1.f } },
        { { 1.0, 1.0 }, { 0.f, 0.f, 1.f, 1.f } }
    };

    std::vector<om::myUint> indexes{ 0, 1, 3, 0, 2, 3 };

    static Timer timer;
    float        elapsedSeconds = timer.elapsed().count();

    const om::myGlfloat rotatingPeriod{ 2 };
    auto                angle = elapsedSeconds / rotatingPeriod * M_PI - M_PI;

    const om::myGlfloat xShift = std::sin(angle);

    const om::myGlfloat yShift = -std::sin(angle) / 2;

    // mainEngineFire.setAngle(angle);

    // mainEngineFire.setPos({ xShift, yShift });

    //    mainEngineFire.draw(engine, MainEngineFire::Event::Run,
    //                        fireTimer.elapsed());

    //    tankSprite.setSpritePos({ xShift, yShift });

    //    tankSprite.setAngle(angle);

    //    // engine.render(verticesTest, { 0, 1, 3, 0, 2, 3 },
    //    m_programIdShaderGrid);

    //    tankSprite.draw(engine);
}

void RenderWrapper::render(const World& world)
{
    renderRectangleTexturedTank(m_engine);

    renderWorld(world);
}

static std::vector<om::VertexMorphed> formShipVertexBuffer()
{
    const om::myGlfloat            sizeX{ 0.2 };
    const om::myGlfloat            sizeY{ 0.3 };
    std::vector<om::VertexMorphed> vertexes;
    vertexes.reserve(11);

    vertexes.push_back({ 0, 0 + sizeY, 0.3, 0.3, 0.3 }); // 0

    vertexes.push_back({ vertexes[0].position.x - sizeX / 9.0f,
                         vertexes[0].position.y - sizeY / 4, 0.3, 0.3,
                         0.3 }); // 1

    vertexes.push_back({ vertexes[0].position.x + sizeX / 9.0f,
                         vertexes[1].position.y, 0.3, 0.3, 0.3 }); // 2

    vertexes.push_back({ vertexes[1].position.x,
                         vertexes[1].position.y - sizeY * 3 / 8.0f, 0, 0,
                         0 }); // 3

    vertexes.push_back({ vertexes[2].position.x,
                         vertexes[1].position.y - sizeY * 3 / 8.0f, 0, 0,
                         0 }); // 4

    vertexes.push_back({ vertexes[3].position.x,
                         vertexes[3].position.y + sizeY / 8, 0, 0, 0 }); // 5

    vertexes.push_back({
        vertexes[3].position.x + sizeX / 6,
        vertexes[3].position.y,
        0,
        0,
        0,
        1,
        vertexes[3].position.x - sizeX / 6,
        vertexes[3].position.y,
    }); // 6

    vertexes.push_back({ vertexes[4].position.x,
                         vertexes[4].position.y + sizeY / 8.0f, 0, 0, 0 }); // 7

    vertexes.push_back(
        { vertexes[4].position.x - sizeX / 6, vertexes[4].position.y, 0, 0, 0,
          1, vertexes[4].position.x + sizeX / 6, vertexes[4].position.y }); // 8

    vertexes.push_back({ vertexes[6].position.x,
                         vertexes[6].position.y - 3 * sizeY / 8.0f, 0, 0, 0, 1,
                         vertexes[6].position2.x,
                         vertexes[6].position.y - 3 * sizeY / 8.0f }); // 9

    vertexes.push_back({ vertexes[8].position.x,
                         vertexes[8].position.y - 3 * sizeY / 8.0f, 0, 0, 0, 1,
                         vertexes[8].position2.x,
                         vertexes[8].position.y - 3 * sizeY / 8.0f }); // 10

    return vertexes;
}

static std::vector<om::myUint> formShipIndexBuffer()
{
    return { 0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 5, 6, 4, 7, 8, 3, 6, 9, 4, 8, 10 };
}

void RenderWrapper::renderWorld(const World& world)
{
    for (const auto& currentObject : world.objects)
    {
        const auto relatedXShip =
            currentObject.x * Global::getCurrentWorldScaleY();
        const auto relatedYShip =
            currentObject.y * Global::getCurrentWorldScaleY();

        m_currentShiftRelateToUser.elements[0] =
            -currentObject.x * Global::getCurrentWorldScaleY();
        m_currentShiftRelateToUser.elements[1] =
            -currentObject.y * Global::getCurrentWorldScaleY();

        const auto vertexes = formShipVertexBuffer();
        const auto indexes  = formShipIndexBuffer();

        om::Vector<2> rotatingCenter{
            (vertexes[9].position.x + vertexes[10].position.x) / 2,
            (vertexes[0].position.y + vertexes[10].position.y) / 2
        };

        const auto rotateMatrix{ om::MatrixFunctor::getRotateMatrix(
            currentObject.angle, rotatingCenter) };

        const auto windowSize = m_engine.getDrawableInchesSize();

        const auto yToXRelate{ static_cast<om::myGlfloat>(windowSize[1]) /
                               static_cast<om::myGlfloat>(windowSize[0]) };

        const auto aspectMatrix{ om::MatrixFunctor::getScaleMatrix(
            { yToXRelate, 1.0 }, { 0.0, 0.0 }) };

        const auto shiftMatrix{ om::MatrixFunctor::getShiftMatrix(
            { static_cast<om::myGlfloat>(relatedXShip),
              static_cast<om::myGlfloat>(relatedYShip) }) };

        const auto resultMatrix = aspectMatrix * shiftMatrix * rotateMatrix;

        MainEngineFire::Event event =
            (currentObject.getMainEngineState().getState())
                ? MainEngineFire::Event::Run
                : MainEngineFire::Event::Stop;

        //        mainEngineFire.setAngle(currentObject.angle);

        //        mainEngineFire.setPos({
        //        static_cast<om::myGlfloat>(relatedXShip),
        //                                static_cast<om::myGlfloat>(relatedYShip)
        //                                });

        //        mainEngineFire.draw(m_engine, event,
        //                            currentObject.getMainEngineState().getTime());

        // rocketMainCorpus.draw(m_engine, currentObject);
        rocket.draw(m_engine, currentObject);

        //        m_engine.setUniform("u_koef", koef, m_programIdMorphedMoved);
        //        m_engine.render(vertexes, indexes, resultMatrix,
        //        moveMatrixUniformName,
        //                        m_programIdMorphedMoved,
        //                        om::ShapeType::triangle);
    }
}
