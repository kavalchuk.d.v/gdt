#pragma once
#include "animation2d.hpp"
#include "imgui_wrapper.hpp"
#include "render_objects.hpp"
#include "sprite.hpp"
#include "world.hpp"
#include <iengine.hpp>

#include <array>
#include <vector>

class RenderWrapper
{
public:
    RenderWrapper(om::IEngine&                 engine,
                  std::array<om::myGlfloat, 3> color    = { 0, 1, 0 },
                  om::myGlfloat                gridStep = 0.025);

    void render(const World& world);
    void render();

private:
    bool checkColor(std::array<om::myGlfloat, 3> color);
    bool checkStep(om::myGlfloat step);

    void renderWorld(const World& world);

    void renderTexturedTriangle();

    void renderRectangleTexturedTank(om::IEngine& engine);

    om::myGlfloat                m_gridStep;
    std::array<om::myGlfloat, 3> m_color;

    om::Vector<2> m_currentShiftRelateToUser{};

    std::vector<om::Vertex> m_vertexBuffer;
    std::vector<om::myUint> m_indexBuffer;

    Sprite tankSprite;

    renderObjects::MainEngineFire   mainEngineFire;
    renderObjects::RocketMainCorpus rocketMainCorpus;
    renderObjects::Rocket           rocket;

    om::ProgramId m_programIdShaderGrid;
    om::ProgramId m_programIdShaderMorph;
    om::ProgramId m_programIdTexturedMorphed;
    om::ProgramId m_programIdTexturedMoved;
    om::ProgramId m_programIdMorphedMoved;
    om::ProgramId m_programIdMoved;

    om::TextureId m_textureIdTank;
    om::TextureId m_textureIdFireMainEngine;
    om::TextureId m_textureIdRocketMainCorpus;

    om::IEngine& m_engine;

    static constexpr std::string_view moveMatrixUniformName{ "u_move_matrix" };

    static const std::vector<std::string_view> textureAttributeNames;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexMorphedAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexTexturedAttributePositions;

    static const std::vector<std::pair<om::myUint, std::string_view>>
        vertexWideAttributePositions;
};
