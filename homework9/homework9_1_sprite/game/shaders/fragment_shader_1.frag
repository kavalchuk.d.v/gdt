#ifdef GL_ES
precision highp float;
#endif
in vec4 v_position;
in vec4 v_color;

layout(location = 0) out vec4 fragColor;

// mouse x, y and current radius
uniform vec3 u_mouse;

void main()
{
     float dx2 = (v_position.x - u_mouse.x)*(v_position.x - u_mouse.x);
     float dy2 = (v_position.y - u_mouse.y)*(v_position.y - u_mouse.y);
     float distance = sqrt(dx2 + dy2);
     float r = u_mouse.z;
     if (distance > r){
         fragColor = v_color;
     }else{
         float relativeDistance = distance / r;
         float relativePower = 1.0 - relativeDistance;
         vec3 chandedColor = vec3(v_color.r, v_color.g * relativeDistance, v_color.b * relativeDistance);
         float newRed = chandedColor.r + relativePower;
         if (newRed > 1.0){
             newRed = 1.0;
         }
         chandedColor.r = newRed;
         fragColor = vec4(chandedColor, v_color.a);
     }
}
