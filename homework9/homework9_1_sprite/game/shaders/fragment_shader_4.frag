#ifdef GL_ES
precision highp float;
#endif


in vec4 v_position;
in vec4 v_color;
in vec2 v_tex_position;

layout(location = 0) out vec4 fragColor;

uniform sampler2D s_texture;
uniform sampler2D s_texture2;

void main()
{

     fragColor = mix(texture2D(s_texture, v_tex_position),texture2D(s_texture2, v_tex_position),0.3);
}
