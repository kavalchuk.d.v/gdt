#ifdef GL_ES
precision highp float;
#endif


in vec4 v_position;
in vec4 v_color;
layout(location = 0) out vec4 fragColor;

uniform sampler2D s_texture;

void main()
{
     fragColor = v_color;
}
