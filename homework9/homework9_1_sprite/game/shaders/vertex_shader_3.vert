in vec2 a_position;
in vec4 a_color;
in vec2 a_tex_position;

out vec4 v_position;
out vec4 v_color;
out vec2 v_tex_position;

void main()
{
    v_position = vec4(a_position, 0.0, 1.0);
    v_tex_position = a_tex_position;
    v_color = a_color;
    gl_Position = v_position;
}
