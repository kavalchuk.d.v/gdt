#pragma once
#include "utilities.hpp"
#include <array>
#include <chrono>
#include <iosfwd>
#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
#include <unordered_map>
#include <vector>

using worldCalcType = double;

class Gravity
{
public:
    static constexpr worldCalcType calcG(const worldCalcType G = 6.67384e-11,
                                         const worldCalcType M = 5.972e24,
                                         const worldCalcType R = 6'371e3);

    static constexpr worldCalcType calcFgravity(const worldCalcType m);
};

class Motion
{
public:
    static constexpr worldCalcType calcDs(const worldCalcType v0,
                                          const worldCalcType v1,
                                          const worldCalcType dt);

    static constexpr worldCalcType calcNextV(const worldCalcType v0,
                                             const worldCalcType a,
                                             const worldCalcType dt);

    static constexpr worldCalcType calcNextA(const worldCalcType m,
                                             const worldCalcType superForce);

    static constexpr worldCalcType calcCommandA(const worldCalcType F,
                                                const worldCalcType m);
};

class Resist
{
public:
    static constexpr worldCalcType calcFresist(const worldCalcType v,
                                               const worldCalcType k1,
                                               const worldCalcType k2);

    static constexpr worldCalcType calcK1(const worldCalcType I,
                                          const worldCalcType mu = 0.0182);

    static constexpr worldCalcType calcK2(const worldCalcType s,
                                          const worldCalcType c,
                                          const worldCalcType ro = 1.29);
};

class Rotation
{
public:
    static constexpr worldCalcType calcShiftAngle(
        const worldCalcType angleSpeed0, const worldCalcType angleSpeed1,
        const worldCalcType dt);

    static constexpr worldCalcType calcNextAngleSpeed(
        const worldCalcType angleSpeed0, const worldCalcType angleAcceleration,
        const worldCalcType dt);

    static constexpr worldCalcType calcNextAngleAcceleration(
        const worldCalcType fullMomentOfForce,
        const worldCalcType momentOfInertion);

    static constexpr worldCalcType calcForceMoment(
        const worldCalcType force, const worldCalcType shoulder);

    static constexpr worldCalcType calcLinearSpeed(
        const worldCalcType angleSpeed, const worldCalcType r);

    static constexpr worldCalcType calcMomentInertion(const worldCalcType m,
                                                      const worldCalcType d);
};

class WorldObjectState
{
public:
    void updateTime(Timer::time_point_t currentTime)
    {
        m_currentTime = currentTime;
    }

    void set(bool state, Timer::time_point_t time)
    {
        if (m_state == state)
        {
            return;
        }
        m_state           = state;
        m_changeStatetime = time;
    }

    bool             getState() const { return m_state; }
    Timer::seconds_t getTime() const
    {
        return m_currentTime - m_changeStatetime;
    }

private:
    bool                m_state{};
    Timer::time_point_t m_changeStatetime{ Timer::clock_t::duration::max() };
    Timer::time_point_t m_currentTime{};
};

class PhysicalObject
{
public:
    using clock_t      = Timer::clock_t;
    using seconds_t    = Timer::seconds_t;
    using time_point_t = Timer::time_point_t;

    worldCalcType x{};
    worldCalcType y{};
    worldCalcType angle{};

    worldCalcType vx{};
    worldCalcType vy{};
    worldCalcType ax{};
    worldCalcType ay{};

    worldCalcType angleSpeed{};
    worldCalcType angleAcceleration{};

    const worldCalcType m = static_cast<worldCalcType>(100); // kg
    const worldCalcType r = static_cast<worldCalcType>(0.3); // meters

    const worldCalcType c = static_cast<worldCalcType>(0.4);          // o.e.
    const worldCalcType i = static_cast<worldCalcType>(r);            // meter
    const worldCalcType s = static_cast<worldCalcType>(M_PI * r * r); // meter2
    const worldCalcType k1{ Resist::calcK1(i) };
    const worldCalcType k2{ Resist::calcK2(s, c) };

    const worldCalcType inertionMoment = Rotation::calcMomentInertion(m, 2 * r);

    friend std::ostream& operator<<(std::ostream&         out,
                                    const PhysicalObject& object);

    worldCalcType getCommandDirectForce();
    worldCalcType getCommandBackwardForce();
    worldCalcType getCommandRotateForce();

    worldCalcType mainEnginesPercentThrust{ 100 };
    worldCalcType sideEnginesPercentThrust{ 100 };
    bool          stabilizationLevel1Enabled{ true };
    bool          stabilizationLevel2Enabled{ false };

    const WorldObjectState& getMainEngineState() const
    {
        return m_mainEngineState;
    }
    const WorldObjectState& getSideEngineState() const
    {
        return m_sideEngineState;
    }

    void updateTime(time_point_t newTime)
    {
        lastUpdateTime = newTime;
        m_mainEngineState.updateTime(lastUpdateTime);
    }

    void enableMainEngine() { m_mainEngineState.set(true, lastUpdateTime); }
    void disableMainEngine() { m_mainEngineState.set(false, lastUpdateTime); }

    void enableSideEngine() { m_sideEngineState.set(true, lastUpdateTime); }
    void disableSideEngine() { m_sideEngineState.set(false, lastUpdateTime); }

    static constexpr worldCalcType width{ 60 };
    static constexpr worldCalcType height{ width * 2 };

private:
    seconds_t getDurationSinceLastUpdate() const
    {
        return std::chrono::duration_cast<seconds_t>(
            lastUpdateTime.time_since_epoch());
    }

    time_point_t                   lastUpdateTime{};
    WorldObjectState               m_mainEngineState{};
    WorldObjectState               m_sideEngineState{};
    static constexpr worldCalcType commandDirectForce{ 5000 };
    static constexpr worldCalcType commandBackwardForce{ 500 };
    static constexpr worldCalcType CommandRotateForce{ 1 };
};

class World
{
public:
    enum class Events : size_t
    {
        userCommandShipUp,
        userCommandShipDown,
        userCommandShipTeleporation,
        userCommandShipRotateLeft,
        userCommandShipRotateRight,
        maxType,
    };

    using WorldEvents =
        std::unordered_map<World::Events, std::array<double, 2>>;

    using clock_t        = Timer::clock_t;
    using seconds_t      = Timer::seconds_t;
    using milliseconds_t = Timer::milliseconds_t;
    using time_point_t   = Timer::time_point_t;

    World(time_point_t initialTime);

    int update(time_point_t nowTime, const WorldEvents* events);

    /// TODO change to other container
    std::vector<PhysicalObject> objects;

    size_t userShipIndex;

    time_point_t lastUpdateTime;
    seconds_t    dt{ milliseconds_t{ 4 } };

    friend std::ostream& operator<<(std::ostream& out, const World& world);
};

constexpr worldCalcType Motion::calcDs(const worldCalcType v0,
                                       const worldCalcType v1,
                                       const worldCalcType dt)
{
    return (v0 + v1) * dt / 2;
}

constexpr worldCalcType Motion::calcNextV(const worldCalcType v0,
                                          const worldCalcType a,
                                          const worldCalcType dt)
{
    return v0 + a * dt;
}

constexpr worldCalcType Motion::calcNextA(const worldCalcType m,
                                          const worldCalcType superForce)
{
    // zero m should be checked before calling calcNextA
    if (m == 0)
    {
        return 0;
    }
    return (superForce) / m;
}

constexpr worldCalcType Motion::calcCommandA(const worldCalcType F,
                                             const worldCalcType m)
{
    if (m == 0)
    {
        std::cerr << "Command cannot be applied to 0 mass objects";
        return 0;
    }
    return F / m;
}

constexpr worldCalcType Gravity::calcG(const worldCalcType G,
                                       const worldCalcType M,
                                       const worldCalcType R)
{
    return G * M / (R * R);
}

constexpr worldCalcType Gravity::calcFgravity(const worldCalcType m)
{
    return m * calcG();
}

constexpr worldCalcType Resist::calcFresist(const worldCalcType v,
                                            const worldCalcType k1,
                                            const worldCalcType k2)
{
    auto fresistK1 = -(k1 * v);
    auto fresistK2 = -(k2 * v * std::abs(v));
    return fresistK1 + fresistK2;
}

constexpr worldCalcType Resist::calcK1(const worldCalcType I,
                                       const worldCalcType mu)
{
    return static_cast<worldCalcType>(6) * static_cast<worldCalcType>(M_PI) *
           mu * I;
}

constexpr worldCalcType Resist::calcK2(const worldCalcType s,
                                       const worldCalcType c,
                                       const worldCalcType ro)
{
    return s * c * ro / 2;
}

constexpr worldCalcType Rotation::calcShiftAngle(
    const worldCalcType angleSpeed0, const worldCalcType angleSpeed1,
    const worldCalcType dt)
{
    return (angleSpeed0 + angleSpeed1) * dt / 2;
}

constexpr worldCalcType Rotation::calcNextAngleSpeed(
    const worldCalcType angleSpeed0, const worldCalcType angleAcceleration,
    const worldCalcType dt)
{
    return angleSpeed0 + angleAcceleration * dt;
}

constexpr worldCalcType Rotation::calcNextAngleAcceleration(
    const worldCalcType fullMomentOfForce, const worldCalcType momentOfInertion)
{
    if (momentOfInertion == 0)
    {
        return 0;
    }
    return fullMomentOfForce / momentOfInertion;
}

constexpr worldCalcType Rotation::calcForceMoment(const worldCalcType force,
                                                  const worldCalcType shoulder)
{
    return force * shoulder;
}

constexpr worldCalcType Rotation::calcLinearSpeed(
    const worldCalcType angleSpeed, const worldCalcType r)
{
    return angleSpeed * r;
}

constexpr worldCalcType Rotation::calcMomentInertion(const worldCalcType m,
                                                     const worldCalcType d)
{
    return m * d * d / 12;
}
