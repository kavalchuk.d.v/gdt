#pragma once
#include "iengine.hpp"

class Environement;

class Global
{
public:
    static constexpr om::myGlfloat baseScaleXtoY = om::IEngine::baseScaleXtoY;
    static constexpr om::myGlfloat ndcSize =
        om::IEngine::maxCoordinate - om::IEngine::minCoordinate;
    static constexpr om::myGlfloat baseWorldWindowSizeX = 1200.0;
    static constexpr om::myGlfloat baseWorldWindowSizeY =
        baseWorldWindowSizeX / baseScaleXtoY;
    static constexpr om::myGlfloat baseWorldToNdcScaleX =
        ndcSize / baseWorldWindowSizeX;
    static constexpr om::myGlfloat baseWorldToNdcScaleY =
        ndcSize / baseWorldWindowSizeY;
    static constexpr om::myGlfloat initialScale = 1.0;

    static constexpr om::myGlfloat minimalScale = 0.33;

    static constexpr om::myGlfloat maximalScale = 3.0;

    static om::myGlfloat getCurrentWorldScaleX()
    {
        return currentWorldScale * baseWorldToNdcScaleX;
    };

    static om::myGlfloat getCurrentWorldScaleY()
    {
        return currentWorldScale * baseWorldToNdcScaleY;
    };

private:
    friend Environement;
    static om::myGlfloat currentWorldScale;
};
