#include "render_objects.hpp"
#include "global.hpp"
#include "utilities.hpp"
#include <stdexcept>

using namespace renderObjects;

RocketMainCorpus::RocketMainCorpus(const std::string_view textureAttribureName,
                                   const std::string_view moveMatrixUniformName,
                                   const om::ProgramId&   programId,
                                   const om::TextureId&   tex)
{
    // TODO! 16/9 screen should be expected
    const om::Vector<2> texturePixcelSize{
        457,
        1024,
    };

    const om::Vector<2> textureSpritePos{
        17 / texturePixcelSize.elements[0],
        28 / texturePixcelSize.elements[1],
    };

    const om::Vector<2> textureSpriteSize{
        1 - textureSpritePos.elements[0] * 2,
        1 - textureSpritePos.elements[1] - 24 / texturePixcelSize.elements[1]
    };

    const auto rocketSizeX = static_cast<float>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketSizeY = static_cast<float>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    m_sprites.push_back({ std::string{ "rocket_corpus" },
                          textureAttribureName,
                          moveMatrixUniformName,
                          programId,
                          tex,
                          { textureSpritePos, textureSpriteSize },
                          { { 0, 0 }, { rocketSizeX, rocketSizeY } },
                          0.f });
}

const om::Vector<2>& RocketMainCorpus::getPos() const
{
    return m_pos;
}
void RocketMainCorpus::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}
om::myGlfloat RocketMainCorpus::getAngle() const
{
    return m_angle;
}
void RocketMainCorpus::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void RocketMainCorpus::draw(om::IEngine& render, const PhysicalObject& rocket)
{
    const auto rocketNdcX =
        static_cast<om::myGlfloat>(rocket.x) * Global::getCurrentWorldScaleY();

    const auto rocketNdcY =
        static_cast<om::myGlfloat>(rocket.y) * Global::getCurrentWorldScaleY();

    const auto rocketSizeX = static_cast<float>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketSizeY = static_cast<float>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    m_sprites[0].setSpriteSize({ rocketSizeX, rocketSizeY });
    m_sprites[0].setSpritePos({ rocketNdcX, rocketNdcY });

    m_sprites[0].setAngle(rocket.angle);
    m_sprites[0].draw(render);
}

///////////////////////////////////////////////////////////////////////////////

EngineFire::EngineFire(const std::string_view textureAttribureName,
                       const std::string_view moveMatrixUniformName,
                       const om::ProgramId& programId, const om::TextureId& tex,
                       Type type)
    : m_type{ type }
{
    // TODO! 16/9 screen should be expected

    const auto textureNumberOfLines   = 5;
    const auto textureNumberOfColumns = 6;

    const om::myGlfloat textureStepX   = 1.0 / textureNumberOfColumns;
    const om::myGlfloat textureStepY   = 1.0 / textureNumberOfLines;
    const om::myGlfloat renderBaseSize = 0.2;

    const om::Vector<2> textureSpriteSize{ textureStepX, textureStepY };

    std::vector<Sprite> allSprites;
    allSprites.reserve(30);
    {
        om::myGlfloat textureXCurent = 0.0;
        om::myGlfloat textureYCurent = 1.0 - textureStepY;

        for (int line = 0; line < textureNumberOfLines; ++line)
        {
            for (int column = 0; column < textureNumberOfColumns; ++column)
            {
                allSprites.push_back(
                    { std::string{ "main_eng_fire" } + std::to_string(line) +
                          " " + std::to_string(column),
                      textureAttribureName,
                      moveMatrixUniformName,
                      programId,
                      tex,
                      { { textureXCurent, textureYCurent }, textureSpriteSize },
                      { { 0, 0 }, { renderBaseSize, renderBaseSize } },
                      0.f });
                textureXCurent += textureStepX;
            }
            textureXCurent = 0.0;
            textureYCurent -= textureStepY;
        }
    }

    om::myGlfloat angle;

    switch (m_type)
    {
        case Type::up:
            angle = 0;
            break;
        case Type::down:
            angle = M_PI;
            break;
        case Type::left:
            angle = M_PI_2;
            break;
        case Type::right:
            angle = -M_PI_2;
            break;
        default:
            throw std::runtime_error(
                "Incorrect type of renderObject engineFire");
    }

    std::for_each_n(allSprites.begin(), allSprites.size(),
                    [angle](Sprite& sprite) { sprite.setBaseAngle(angle); });

    std::copy_n(allSprites.begin(), 9, std::back_inserter(m_spritesStart));
    std::copy_n(&allSprites[10], 10, std::back_inserter(m_spritesRun));
    std::copy_n(&allSprites[20], 10, std::back_inserter(m_spritesStop));

    m_animation.sprites(m_spritesStart, m_spritesRun, m_spritesStop);
    m_animation.setFps(20);
}

EngineFire& EngineFire::operator=(const EngineFire& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;
    this->m_type  = mainEngineFire2.m_type;

    this->m_spritesStart = mainEngineFire2.m_spritesStart;
    this->m_spritesRun   = mainEngineFire2.m_spritesRun;
    this->m_spritesStop  = mainEngineFire2.m_spritesStop;

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
    return *this;
}

EngineFire::EngineFire(const EngineFire& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;
    this->m_type  = mainEngineFire2.m_type;

    this->m_spritesStart = mainEngineFire2.m_spritesStart;
    this->m_spritesRun   = mainEngineFire2.m_spritesRun;
    this->m_spritesStop  = mainEngineFire2.m_spritesStop;

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
}

EngineFire& EngineFire::operator=(const EngineFire&& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;
    this->m_type  = mainEngineFire2.m_type;

    this->m_spritesStart = std::move(mainEngineFire2.m_spritesStart);
    this->m_spritesRun   = std::move(mainEngineFire2.m_spritesRun);
    this->m_spritesStop  = std::move(mainEngineFire2.m_spritesStop);

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
    return *this;
}

EngineFire::EngineFire(const EngineFire&& mainEngineFire2)
{
    this->m_pos   = mainEngineFire2.m_pos;
    this->m_angle = mainEngineFire2.m_angle;
    this->m_type  = mainEngineFire2.m_type;

    this->m_spritesStart = std::move(mainEngineFire2.m_spritesStart);
    this->m_spritesRun   = std::move(mainEngineFire2.m_spritesRun);
    this->m_spritesStop  = std::move(mainEngineFire2.m_spritesStop);

    this->m_animation = mainEngineFire2.m_animation;
    this->m_animation.sprites(this->m_spritesStart, this->m_spritesRun,
                              this->m_spritesStop);
}

const om::Vector<2>& EngineFire::getPos() const
{
    return m_pos;
}
void EngineFire::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}

const om::Vector<2>& EngineFire::getSize() const
{
    return m_size;
}
void EngineFire::setSize(const om::Vector<2>& size)
{
    m_size = size;
}

om::myGlfloat EngineFire::getAngle() const
{
    return m_angle;
}

void EngineFire::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void EngineFire::draw(om::IEngine& render, Event event,
                      Timer::seconds_t eventTime, om::Vector<2> basePoint) const
{
    auto currentSprite = m_animation.getCurrentSprite(event, eventTime);

    if (!currentSprite)
    {
        return;
    }
    currentSprite->setSpriteSize(m_size);
    currentSprite->setSpritePos(m_pos);
    currentSprite->setAngle(m_angle);
    currentSprite->draw(render, basePoint);
}

///////////////////////////////////////////////////////////////////////////////

Rocket::Rocket(const std::string_view textureAttribureName,
               const std::string_view moveMatrixUniformName,
               const om::ProgramId&   programId,
               const om::TextureId&   texMainCorpus,
               const om::TextureId&   texMainEngineFire,
               const om::TextureId&   texSideEngineFire)
    : m_mainCorpusRelativePos{ 0.f, 0.f }

    , m_mainEngineFireRelativePos{ 0.f, 0.40f }
    , m_mainEngineFireRelativeSize{ 1.0f, 1.0f }

    // engine 1 - left side down, engine 2 - left side uppper, engine 3 - rigth
    // side down, engine 4 - rigth side upper

    , m_sideEnginesFireRelativePos{ { { 0.47f, 0.16f },
                                      { 0.5f, -0.1f },
                                      { -0.47f, 0.16f },
                                      { -0.5f, -0.1f } } }
    , m_sideEnginesFireRelativeSize{ { { 0.5f, 0.4f },
                                       { 0.5f, 0.4f },
                                       { 0.5f, 0.4f },
                                       { 0.5f, 0.4f } } }

    , mainCorpus{ textureAttribureName, moveMatrixUniformName, programId,
                  texMainCorpus }
    , mainEngineFire{ textureAttribureName, moveMatrixUniformName, programId,
                      texMainEngineFire, EngineFire::Type::down }
    , sideEnginesFire{
        { { textureAttribureName, moveMatrixUniformName, programId,
            texSideEngineFire, EngineFire::Type::left },
          { textureAttribureName, moveMatrixUniformName, programId,
            texSideEngineFire, EngineFire::Type::left },
          { textureAttribureName, moveMatrixUniformName, programId,
            texSideEngineFire, EngineFire::Type::right },
          { textureAttribureName, moveMatrixUniformName, programId,
            texSideEngineFire, EngineFire::Type::right } }
    }
{
}

const om::Vector<2>& Rocket::getPos() const
{
    return m_pos;
}
void Rocket::setPos(const om::Vector<2>& pos)
{
    m_pos = pos;
}
om::myGlfloat Rocket::getAngle() const
{
    return m_angle;
}
void Rocket::setAngle(om::myGlfloat angle)
{
    m_angle = angle;
}

void Rocket::drawEngineFire(om::IEngine& render, const PhysicalObject& rocket,
                            const WorldObjectState& engineState,
                            EngineFire&             engineFire,
                            const om::Vector<2>&    engineFireRelativePos,
                            const om::Vector<2>&    engineFireRelativeSize,
                            om::myGlfloat           thrustRelative)
{
    const auto ndcXShip =
        static_cast<om::myGlfloat>(rocket.x * Global::getCurrentWorldScaleY());
    const auto ndcYShip =
        static_cast<om::myGlfloat>(rocket.y * Global::getCurrentWorldScaleY());

    const om::Vector<2> ndcCoordShip{ ndcXShip, ndcYShip };

    const auto rocketNdcSizeX = static_cast<om::myGlfloat>(
        PhysicalObject::width * Global::getCurrentWorldScaleY());

    const auto rocketNdcSizeY = static_cast<om::myGlfloat>(
        PhysicalObject::height * Global::getCurrentWorldScaleY());

    const om::Vector<2> rocketNdcSize{ rocketNdcSizeX, rocketNdcSizeY };

    const om::Vector<2> engineFireNdcSize =
        engineFireRelativeSize * rocketNdcSize *
        static_cast<om::myGlfloat>(thrustRelative);

    const auto ndcEngineFireShift =
        engineFireRelativePos * (engineFireNdcSize + rocketNdcSize);

    const auto ndcEngineFire = ndcCoordShip - ndcEngineFireShift;

    engineFire.setAngle(rocket.angle);

    engineFire.setSize(engineFireNdcSize);

    engineFire.setPos(ndcEngineFire);

    EngineFire::Event eventEngine = (engineState.getState())
                                        ? EngineFire::Event::Run
                                        : EngineFire::Event::Stop;

    engineFire.draw(render, eventEngine, engineState.getTime(),
                    ndcEngineFireShift);
}

void Rocket::draw(om::IEngine& render, const PhysicalObject& rocket)
{

    drawEngineFire(render, rocket, rocket.getMainEngineState(), mainEngineFire,
                   m_mainEngineFireRelativePos, m_mainEngineFireRelativeSize,
                   rocket.mainEnginesPercentThrust / 100);

    for (size_t i = 0; i < sideEnginesFire.size(); ++i)
    {
        drawEngineFire(render, rocket, rocket.getSideEngineState(),
                       sideEnginesFire[i], m_sideEnginesFireRelativePos[i],
                       m_sideEnginesFireRelativeSize[i],
                       rocket.sideEnginesPercentThrust / 100);
    }

    mainCorpus.draw(render, rocket);
}
