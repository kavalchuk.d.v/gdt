#pragma once
#include "animation2d.hpp"
#include "iengine.hpp"
#include "sprite.hpp"
#include "utilities.hpp"
#include "world.hpp"

namespace renderObjects
{

class RocketMainCorpus
{
public:
    RocketMainCorpus() = default;

    RocketMainCorpus(const std::string_view textureAttribureName,
                     const std::string_view moveMatrixUniformName,
                     const om::ProgramId& programId, const om::TextureId& tex);

    const om::Vector<2>& getPos() const;
    void                 setPos(const om::Vector<2>& pos);
    om::myGlfloat        getAngle() const;
    void                 setAngle(om::myGlfloat angle);

    void draw(om::IEngine& render, const PhysicalObject& rocket);

private:
    om::Vector<2>       m_pos{};
    om::myGlfloat       m_angle{};
    std::vector<Sprite> m_sprites{};
};

class EngineFire
{
public:
    enum class Type
    {
        down,
        up,
        left,
        right
    };
    using Event  = Animation2D3Phase::Event;
    EngineFire() = default;

    EngineFire& operator=(const EngineFire& mainEngineFire2);

    EngineFire(const EngineFire& mainEngineFire2);

    EngineFire& operator=(const EngineFire&& mainEngineFire2);

    EngineFire(const EngineFire&& mainEngineFire2);

    EngineFire(const std::string_view textureAttribureName,
               const std::string_view moveMatrixUniformName,
               const om::ProgramId& programId, const om::TextureId& tex,
               Type type = Type::down);

    const om::Vector<2>& getPos() const;
    void                 setPos(const om::Vector<2>& pos);
    const om::Vector<2>& getSize() const;
    void                 setSize(const om::Vector<2>& size);
    om::myGlfloat        getAngle() const;
    void                 setAngle(om::myGlfloat angle);

    void draw(om::IEngine& render, Event event, Timer::seconds_t eventTime,
              om::Vector<2> basePoint = {}) const;

protected:
    om::Vector<2>       m_pos{};
    om::Vector<2>       m_size{};
    om::myGlfloat       m_angle{};
    std::vector<Sprite> m_spritesStart{};
    std::vector<Sprite> m_spritesRun{};
    std::vector<Sprite> m_spritesStop{};
    Animation2D3Phase   m_animation{};
    Type                m_type{};
};

class Rocket
{
public:
    Rocket() = default;
    Rocket(const std::string_view textureAttribureName,
           const std::string_view moveMatrixUniformName,
           const om::ProgramId& programId, const om::TextureId& texMainCorpus,
           const om::TextureId& texMainEngineFire,
           const om::TextureId& texSideEngineFire);

    void draw(om::IEngine& render, const PhysicalObject& rocket);

    const om::Vector<2>& getPos() const;
    void                 setPos(const om::Vector<2>& pos);
    om::myGlfloat        getAngle() const;
    void                 setAngle(om::myGlfloat angle);

private:
    void drawEngineFire(om::IEngine& render, const PhysicalObject& rocket,
                        const WorldObjectState& engineState,
                        EngineFire&             engineFire,
                        const om::Vector<2>&    engineFireRelativePos,
                        const om::Vector<2>&    engineFireRelativeSize,
                        om::myGlfloat           thrustRelative);

    om::Vector<2>                m_pos{};
    om::Vector<2>                m_mainCorpusRelativePos{};
    om::Vector<2>                m_mainEngineFireRelativePos{};
    om::Vector<2>                m_mainEngineFireRelativeSize{};
    std::array<om::Vector<2>, 4> m_sideEnginesFireRelativePos{};
    std::array<om::Vector<2>, 4> m_sideEnginesFireRelativeSize{};
    om::myGlfloat                m_angle{};
    RocketMainCorpus             mainCorpus{};
    EngineFire                   mainEngineFire{};
    std::array<EngineFire, 4>    sideEnginesFire{};
};

} // namespace renderObjects
