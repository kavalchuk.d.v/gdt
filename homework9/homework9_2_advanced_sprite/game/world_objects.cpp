#include "world_objects.hpp"

worldCalcType PhysicalObject::getCommandDirectForce()
{
    return commandDirectForce * mainEnginesPercentThrust / 100;
}
worldCalcType PhysicalObject::getCommandBackwardForce()
{
    return -commandBackwardForce * sideEnginesPercentThrust / 100;
}
worldCalcType PhysicalObject::getCommandRotateForce()
{
    return CommandRotateForce * sideEnginesPercentThrust / 100;
}
