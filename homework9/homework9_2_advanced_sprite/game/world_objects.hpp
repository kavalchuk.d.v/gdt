#pragma once
#include "utilities.hpp"
#include "world_physics.hpp"
#include <iosfwd>

class WorldObjectState
{
public:
    void updateTime(Timer::time_point_t currentTime)
    {
        m_currentTime = currentTime;
    }

    void set(bool state, Timer::time_point_t time)
    {
        if (m_state == state)
        {
            return;
        }
        m_state           = state;
        m_changeStatetime = time;
    }

    bool             getState() const { return m_state; }
    Timer::seconds_t getTime() const
    {
        return m_currentTime - m_changeStatetime;
    }

private:
    bool                m_state{};
    Timer::time_point_t m_changeStatetime{ Timer::clock_t::duration::max() };
    Timer::time_point_t m_currentTime{};
};

class PhysicalObject
{
public:
    using clock_t      = Timer::clock_t;
    using seconds_t    = Timer::seconds_t;
    using time_point_t = Timer::time_point_t;

    worldCalcType x{};
    worldCalcType y{};
    worldCalcType angle{};

    worldCalcType vx{};
    worldCalcType vy{};
    worldCalcType ax{};
    worldCalcType ay{};

    worldCalcType angleSpeed{};
    worldCalcType angleAcceleration{};

    const worldCalcType m = static_cast<worldCalcType>(100); // kg
    const worldCalcType r = static_cast<worldCalcType>(0.3); // meters

    const worldCalcType c = static_cast<worldCalcType>(0.4);          // o.e.
    const worldCalcType i = static_cast<worldCalcType>(r);            // meter
    const worldCalcType s = static_cast<worldCalcType>(M_PI * r * r); // meter2
    const worldCalcType k1{ Resist::calcK1(i) };
    const worldCalcType k2{ Resist::calcK2(s, c) };

    const worldCalcType inertionMoment = Rotation::calcMomentInertion(m, 2 * r);

    friend std::ostream& operator<<(std::ostream&         out,
                                    const PhysicalObject& object);

    worldCalcType getCommandDirectForce();
    worldCalcType getCommandBackwardForce();
    worldCalcType getCommandRotateForce();

    worldCalcType mainEnginesPercentThrust{ 100 };
    worldCalcType sideEnginesPercentThrust{ 100 };
    bool          stabilizationLevel1Enabled{ true };
    bool          stabilizationLevel2Enabled{ false };

    const WorldObjectState& getMainEngineState() const
    {
        return m_mainEngineState;
    }
    const WorldObjectState& getSideEngineState() const
    {
        return m_sideEngineState;
    }

    void updateTime(time_point_t newTime)
    {
        lastUpdateTime = newTime;
        m_mainEngineState.updateTime(lastUpdateTime);
        m_sideEngineState.updateTime(lastUpdateTime);
    }

    void enableMainEngine() { m_mainEngineState.set(true, lastUpdateTime); }
    void disableMainEngine() { m_mainEngineState.set(false, lastUpdateTime); }

    void enableSideEngine() { m_sideEngineState.set(true, lastUpdateTime); }
    void disableSideEngine() { m_sideEngineState.set(false, lastUpdateTime); }

    static constexpr worldCalcType width{ 60 };
    static constexpr worldCalcType height{ width * 2 };

private:
    seconds_t getDurationSinceLastUpdate() const
    {
        return std::chrono::duration_cast<seconds_t>(
            lastUpdateTime.time_since_epoch());
    }

    time_point_t                   lastUpdateTime{};
    WorldObjectState               m_mainEngineState{};
    WorldObjectState               m_sideEngineState{};
    static constexpr worldCalcType commandDirectForce{ 5000 };
    static constexpr worldCalcType commandBackwardForce{ 500 };
    static constexpr worldCalcType CommandRotateForce{ 1 };
};
