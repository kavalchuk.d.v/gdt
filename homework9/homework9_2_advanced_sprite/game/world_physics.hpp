#pragma once
#define _USE_MATH_DEFINES
#include <math.h>

using worldCalcType = double;

class Gravity
{
public:
    static constexpr worldCalcType calcG(const worldCalcType G = 6.67384e-11,
                                         const worldCalcType M = 5.972e24,
                                         const worldCalcType R = 6'371e3);

    static constexpr worldCalcType calcFgravity(const worldCalcType m);
};

class Motion
{
public:
    static constexpr worldCalcType calcDs(const worldCalcType v0,
                                          const worldCalcType v1,
                                          const worldCalcType dt);

    static constexpr worldCalcType calcNextV(const worldCalcType v0,
                                             const worldCalcType a,
                                             const worldCalcType dt);

    static constexpr worldCalcType calcNextA(const worldCalcType m,
                                             const worldCalcType superForce);

    static constexpr worldCalcType calcCommandA(const worldCalcType F,
                                                const worldCalcType m);
};

class Resist
{
public:
    static constexpr worldCalcType calcFresist(const worldCalcType v,
                                               const worldCalcType k1,
                                               const worldCalcType k2);

    static constexpr worldCalcType calcK1(const worldCalcType I,
                                          const worldCalcType mu = 0.0182);

    static constexpr worldCalcType calcK2(const worldCalcType s,
                                          const worldCalcType c,
                                          const worldCalcType ro = 1.29);
};

class Rotation
{
public:
    static constexpr worldCalcType calcShiftAngle(
        const worldCalcType angleSpeed0, const worldCalcType angleSpeed1,
        const worldCalcType dt);

    static constexpr worldCalcType calcNextAngleSpeed(
        const worldCalcType angleSpeed0, const worldCalcType angleAcceleration,
        const worldCalcType dt);

    static constexpr worldCalcType calcNextAngleAcceleration(
        const worldCalcType fullMomentOfForce,
        const worldCalcType momentOfInertion);

    static constexpr worldCalcType calcForceMoment(
        const worldCalcType force, const worldCalcType shoulder);

    static constexpr worldCalcType calcLinearSpeed(
        const worldCalcType angleSpeed, const worldCalcType r);

    static constexpr worldCalcType calcMomentInertion(const worldCalcType m,
                                                      const worldCalcType d);
};

constexpr worldCalcType Motion::calcDs(const worldCalcType v0,
                                       const worldCalcType v1,
                                       const worldCalcType dt)
{
    return (v0 + v1) * dt / 2;
}

constexpr worldCalcType Motion::calcNextV(const worldCalcType v0,
                                          const worldCalcType a,
                                          const worldCalcType dt)
{
    return v0 + a * dt;
}

constexpr worldCalcType Motion::calcNextA(const worldCalcType m,
                                          const worldCalcType superForce)
{
    // zero m should be checked before calling calcNextA
    if (m == 0)
    {
        return 0;
    }
    return (superForce) / m;
}

constexpr worldCalcType Motion::calcCommandA(const worldCalcType F,
                                             const worldCalcType m)
{
    if (m == 0)
    {
        return 0;
    }
    return F / m;
}

constexpr worldCalcType Gravity::calcG(const worldCalcType G,
                                       const worldCalcType M,
                                       const worldCalcType R)
{
    return G * M / (R * R);
}

constexpr worldCalcType Gravity::calcFgravity(const worldCalcType m)
{
    return m * calcG();
}

constexpr worldCalcType Resist::calcFresist(const worldCalcType v,
                                            const worldCalcType k1,
                                            const worldCalcType k2)
{
    auto fresistK1 = -(k1 * v);
    auto fresistK2 = -(k2 * v * std::abs(v));
    return fresistK1 + fresistK2;
}

constexpr worldCalcType Resist::calcK1(const worldCalcType I,
                                       const worldCalcType mu)
{
    return static_cast<worldCalcType>(6) * static_cast<worldCalcType>(M_PI) *
           mu * I;
}

constexpr worldCalcType Resist::calcK2(const worldCalcType s,
                                       const worldCalcType c,
                                       const worldCalcType ro)
{
    return s * c * ro / 2;
}

constexpr worldCalcType Rotation::calcShiftAngle(
    const worldCalcType angleSpeed0, const worldCalcType angleSpeed1,
    const worldCalcType dt)
{
    return (angleSpeed0 + angleSpeed1) * dt / 2;
}

constexpr worldCalcType Rotation::calcNextAngleSpeed(
    const worldCalcType angleSpeed0, const worldCalcType angleAcceleration,
    const worldCalcType dt)
{
    return angleSpeed0 + angleAcceleration * dt;
}

constexpr worldCalcType Rotation::calcNextAngleAcceleration(
    const worldCalcType fullMomentOfForce, const worldCalcType momentOfInertion)
{
    if (momentOfInertion == 0)
    {
        return 0;
    }
    return fullMomentOfForce / momentOfInertion;
}

constexpr worldCalcType Rotation::calcForceMoment(const worldCalcType force,
                                                  const worldCalcType shoulder)
{
    return force * shoulder;
}

constexpr worldCalcType Rotation::calcLinearSpeed(
    const worldCalcType angleSpeed, const worldCalcType r)
{
    return angleSpeed * r;
}

constexpr worldCalcType Rotation::calcMomentInertion(const worldCalcType m,
                                                     const worldCalcType d)
{
    return m * d * d / 12;
}
